��ty      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �block_quote���)��}�(hhh]��
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�h	�parent�hhh�source�N�line�Nubh �target���)��}�(h�.. _architecture:�h]�h}�(h]�h]�h]�h]�h]��refid��architecture�uhhhKhhhhh�8/home/whot/code/libinput/build/doc/user/architecture.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h� libinput's internal architecture�h]�h �Text����"libinput’s internal architecture�����}�(hh6hh4hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hh/hhhh,hKubh �	paragraph���)��}�(h��This page provides an outline of libinput's internal architecture. The goal
here is to get the high-level picture across and point out the components
and their interplay to new developers.�h]�h9��This page provides an outline of libinput’s internal architecture. The goal
here is to get the high-level picture across and point out the components
and their interplay to new developers.�����}�(hhHhhFhhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKhh/hhubhE)��}�(hX  The public facing API is in ``libinput.c``, this file is thus the entry point
for almost all API calls. General device handling is in ``evdev.c`` with the
device-type-specific implementations in ``evdev-<type>.c``. It is not
necessary to understand all of libinput to contribute a patch.�h]�(h9�The public facing API is in �����}�(h�The public facing API is in �hhThhhNhNubh �literal���)��}�(h�``libinput.c``�h]�h9�
libinput.c�����}�(h�
libinput.c�hh_ubah}�(h]�h]�h]�h]�h]�uhh]hhTubh9�\, this file is thus the entry point
for almost all API calls. General device handling is in �����}�(h�\, this file is thus the entry point
for almost all API calls. General device handling is in �hhThhhNhNubh^)��}�(h�``evdev.c``�h]�h9�evdev.c�����}�(h�evdev.c�hhsubah}�(h]�h]�h]�h]�h]�uhh]hhTubh9�2 with the
device-type-specific implementations in �����}�(h�2 with the
device-type-specific implementations in �hhThhhNhNubh^)��}�(h�``evdev-<type>.c``�h]�h9�evdev-<type>.c�����}�(h�evdev-<type>.c�hh�ubah}�(h]�h]�h]�h]�h]�uhh]hhTubh9�J. It is not
necessary to understand all of libinput to contribute a patch.�����}�(h�J. It is not
necessary to understand all of libinput to contribute a patch.�hhThhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKhh/hhubhE)��}�(h��:ref:`architecture-contexts` is the only user-visible implementation detail,
everything else is purely internal implementation and may change when
required.�h]�(�sphinx.addnodes��pending_xref���)��}�(h�:ref:`architecture-contexts`�h]�h �inline���)��}�(h�architecture-contexts�h]�h9�architecture-contexts�����}�(hhhh�ubah}�(h]�h]�(�xref��std��std-ref�eh]�h]�h]�uhh�hh�ubah}�(h]�h]�h]�h]�h]��refdoc��architecture��	refdomain�h��reftype��ref��refexplicit���refwarn���	reftarget��architecture-contexts�uhh�hh,hKhh�ubh9�� is the only user-visible implementation detail,
everything else is purely internal implementation and may change when
required.�����}�(h�� is the only user-visible implementation detail,
everything else is purely internal implementation and may change when
required.�hh�hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKhh/hhubh)��}�(h�.. _architecture-contexts:�h]�h}�(h]�h]�h]�h]�h]�h*�architecture-contexts�uhhhKhh/hhhh,ubh.)��}�(hhh]�(h3)��}�(h�The udev and path contexts�h]�h9�The udev and path contexts�����}�(hh�hh�hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hh�hhhh,hKubhE)��}�(hX1  The first building block is the "context" which can be one of
two types, "path" and "udev". See **libinput_path_create_context()** and
**libinput_udev_create_context()**. The path/udev specific bits are in
``path-seat.c`` and ``udev-seat.c``. This includes the functions that add new
devices to a context.�h]�(h9�lThe first building block is the “context” which can be one of
two types, “path” and “udev”. See �����}�(h�`The first building block is the "context" which can be one of
two types, "path" and "udev". See �hh�hhhNhNubh �strong���)��}�(h�"**libinput_path_create_context()**�h]�h9�libinput_path_create_context()�����}�(h�libinput_path_create_context()�hj   ubah}�(h]�h]�h]�h]�h]�uhh�hh�ubh9� and
�����}�(h� and
�hh�hhhNhNubh�)��}�(h�"**libinput_udev_create_context()**�h]�h9�libinput_udev_create_context()�����}�(h�libinput_udev_create_context()�hj  ubah}�(h]�h]�h]�h]�h]�uhh�hh�ubh9�%. The path/udev specific bits are in
�����}�(h�%. The path/udev specific bits are in
�hh�hhhNhNubh^)��}�(h�``path-seat.c``�h]�h9�path-seat.c�����}�(h�path-seat.c�hj(  ubah}�(h]�h]�h]�h]�h]�uhh]hh�ubh9� and �����}�(h� and �hh�hhhNhNubh^)��}�(h�``udev-seat.c``�h]�h9�udev-seat.c�����}�(h�udev-seat.c�hj<  ubah}�(h]�h]�h]�h]�h]�uhh]hh�ubh9�@. This includes the functions that add new
devices to a context.�����}�(h�@. This includes the functions that add new
devices to a context.�hh�hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKhh�hhub�sphinx.ext.graphviz��graphviz���)��}�(hhh]�h}�(h]�h]�h]�h]�h]��code�XS  digraph context
{
  compound=true;
  rankdir="LR";
  node [
    shape="box";
  ]

  libudev [label="libudev 'add' event"]
  udev [label="**libinput_udev_create_context()**"];
  udev_backend [label="udev-specific backend"];
  context [label="libinput context"]
  udev -> udev_backend;
  libudev -> udev_backend;
  udev_backend -> context;
}��options�}��docname�h�suhjW  hh�hhhh,hK5ubhE)��}�(h��The udev context provides automatic device hotplugging as udev's "add"
events are handled directly by libinput. The path context requires that the
caller adds devices.�h]�h9��The udev context provides automatic device hotplugging as udev’s “add”
events are handled directly by libinput. The path context requires that the
caller adds devices.�����}�(hji  hjg  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK6hh�hhubjX  )��}�(hhh]�h}�(h]�h]�h]�h]�h]�jb  X^  digraph context
{
  compound=true;
  rankdir="LR";
  node [
    shape="box";
  ]

  path [label="**libinput_path_create_context()**"];
  path_backend [label="path-specific backend"];
  xdriver [label="**libinput_path_add_device()**"]
  context [label="libinput context"]
  path -> path_backend;
  xdriver -> path_backend;
  path_backend -> context;
}�jd  }�jf  h�suhjW  hh�hhhh,hKOubhE)��}�(h�cAs a general rule: all Wayland compositors use a udev context, the X.org
stack uses a path context.�h]�h9�cAs a general rule: all Wayland compositors use a udev context, the X.org
stack uses a path context.�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKPhh�hhubhE)��}�(h��Which context was initialized only matters for creating/destroying a context
and adding devices. The device handling itself is the same for both types of
context.�h]�h9��Which context was initialized only matters for creating/destroying a context
and adding devices. The device handling itself is the same for both types of
context.�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKShh�hhubh)��}�(h�.. _architecture-device:�h]�h}�(h]�h]�h]�h]�h]�h*�architecture-device�uhhhK\hh�hhhh,ubeh}�(h]�(�the-udev-and-path-contexts�h�eh]�h]�(�the udev and path contexts��architecture-contexts�eh]�h]�uhh-hh/hhhh,hK�expect_referenced_by_name�}�j�  h�s�expect_referenced_by_id�}�h�h�subh.)��}�(hhh]�(h3)��}�(h�Device initialization�h]�h9�Device initialization�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj�  hhhh,hK[ubhE)��}�(h��libinput only supports evdev devices, all the device initialization is done
in ``evdev.c``. Much of the libinput public API is also a thin wrapper around
the matching implementation in the evdev device.�h]�(h9�Olibinput only supports evdev devices, all the device initialization is done
in �����}�(h�Olibinput only supports evdev devices, all the device initialization is done
in �hj�  hhhNhNubh^)��}�(h�``evdev.c``�h]�h9�evdev.c�����}�(h�evdev.c�hj�  ubah}�(h]�h]�h]�h]�h]�uhh]hj�  ubh9�p. Much of the libinput public API is also a thin wrapper around
the matching implementation in the evdev device.�����}�(h�p. Much of the libinput public API is also a thin wrapper around
the matching implementation in the evdev device.�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK]hj�  hhubhE)��}�(h�WThere is a 1:1 mapping between libinput devices and ``/dev/input/eventX``
device nodes.�h]�(h9�4There is a 1:1 mapping between libinput devices and �����}�(h�4There is a 1:1 mapping between libinput devices and �hj�  hhhNhNubh^)��}�(h�``/dev/input/eventX``�h]�h9�/dev/input/eventX�����}�(h�/dev/input/eventX�hj�  ubah}�(h]�h]�h]�h]�h]�uhh]hj�  ubh9�
device nodes.�����}�(h�
device nodes.�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKahj�  hhubjX  )��}�(hhh]�h}�(h]�h]�h]�h]�h]�jb  X�  digraph context
{
  compound=true;
  rankdir="LR";
  node [
    shape="box";
  ]

  devnode [label="/dev/input/event0"]

  libudev [label="libudev 'add' event"]
  xdriver [label="**libinput_path_add_device()**"]
  context [label="libinput context"]

  evdev [label="evdev_device_create()"]

  devnode -> xdriver;
  devnode -> libudev;
  xdriver -> context;
  libudev -> context;

  context->evdev;

}�jd  }�jf  h�suhjW  hj�  hhhh,hK�ubhE)��}�(hX  Entry point for all devices is ``evdev_device_create()``, this function
decides to create a ``struct evdev_device`` for the given device node.
Based on the udev tags (e.g. ``ID_INPUT_TOUCHPAD``), a
:ref:`architecture-dispatch` is initialized. All event handling is then in this
dispatch.�h]�(h9�Entry point for all devices is �����}�(h�Entry point for all devices is �hj  hhhNhNubh^)��}�(h�``evdev_device_create()``�h]�h9�evdev_device_create()�����}�(h�evdev_device_create()�hj  ubah}�(h]�h]�h]�h]�h]�uhh]hj  ubh9�$, this function
decides to create a �����}�(h�$, this function
decides to create a �hj  hhhNhNubh^)��}�(h�``struct evdev_device``�h]�h9�struct evdev_device�����}�(h�struct evdev_device�hj3  ubah}�(h]�h]�h]�h]�h]�uhh]hj  ubh9�9 for the given device node.
Based on the udev tags (e.g. �����}�(h�9 for the given device node.
Based on the udev tags (e.g. �hj  hhhNhNubh^)��}�(h�``ID_INPUT_TOUCHPAD``�h]�h9�ID_INPUT_TOUCHPAD�����}�(h�ID_INPUT_TOUCHPAD�hjG  ubah}�(h]�h]�h]�h]�h]�uhh]hj  ubh9�), a
�����}�(h�), a
�hj  hhhNhNubh�)��}�(h�:ref:`architecture-dispatch`�h]�h�)��}�(h�architecture-dispatch�h]�h9�architecture-dispatch�����}�(hhhj_  ubah}�(h]�h]�(h��std��std-ref�eh]�h]�h]�uhh�hj[  ubah}�(h]�h]�h]�h]�h]��refdoc�hƌ	refdomain�jj  �reftype��ref��refexplicit���refwarn��ȟarchitecture-dispatch�uhh�hh,hK�hj  ubh9�= is initialized. All event handling is then in this
dispatch.�����}�(h�= is initialized. All event handling is then in this
dispatch.�hj  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj�  hhubhE)��}�(h��Rejection of devices and the application of quirks is generally handled in
``evdev.c`` as well. Common functionality shared across multiple device types
(like button-scrolling) is also handled here.�h]�(h9�KRejection of devices and the application of quirks is generally handled in
�����}�(h�KRejection of devices and the application of quirks is generally handled in
�hj�  hhhNhNubh^)��}�(h�``evdev.c``�h]�h9�evdev.c�����}�(h�evdev.c�hj�  ubah}�(h]�h]�h]�h]�h]�uhh]hj�  ubh9�p as well. Common functionality shared across multiple device types
(like button-scrolling) is also handled here.�����}�(h�p as well. Common functionality shared across multiple device types
(like button-scrolling) is also handled here.�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj�  hhubh)��}�(h�.. _architecture-dispatch:�h]�h}�(h]�h]�h]�h]�h]�h*�architecture-dispatch�uhhhK�hj�  hhhh,ubeh}�(h]�(�device-initialization�j�  eh]�h]�(�device initialization��architecture-device�eh]�h]�uhh-hh/hhhh,hK[j�  }�j�  j�  sj�  }�j�  j�  subh.)��}�(hhh]�(h3)��}�(h�#Device-type specific event dispatch�h]�h9�#Device-type specific event dispatch�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj�  hhhh,hK�ubhE)��}�(hXO  Depending on the device type, ``evdev_configure_device`` creates the matching
``struct evdev_dispatch``. This dispatch interface contains the function
pointers to handle events. Four such dispatch methods are currently
implemented: touchpad, tablet, tablet pad, and the fallback dispatch which
handles mice, keyboards and touchscreens.�h]�(h9�Depending on the device type, �����}�(h�Depending on the device type, �hj�  hhhNhNubh^)��}�(h�``evdev_configure_device``�h]�h9�evdev_configure_device�����}�(h�evdev_configure_device�hj�  ubah}�(h]�h]�h]�h]�h]�uhh]hj�  ubh9� creates the matching
�����}�(h� creates the matching
�hj�  hhhNhNubh^)��}�(h�``struct evdev_dispatch``�h]�h9�struct evdev_dispatch�����}�(h�struct evdev_dispatch�hj�  ubah}�(h]�h]�h]�h]�h]�uhh]hj�  ubh9��. This dispatch interface contains the function
pointers to handle events. Four such dispatch methods are currently
implemented: touchpad, tablet, tablet pad, and the fallback dispatch which
handles mice, keyboards and touchscreens.�����}�(h��. This dispatch interface contains the function
pointers to handle events. Four such dispatch methods are currently
implemented: touchpad, tablet, tablet pad, and the fallback dispatch which
handles mice, keyboards and touchscreens.�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj�  hhubjX  )��}�(hhh]�h}�(h]�h]�h]�h]�h]�jb  X_  digraph context
{
  compound=true;
  rankdir="LR";
  node [
    shape="box";
  ]

  evdev [label="evdev_device_create()"]

  fallback [label="evdev-fallback.c"]
  touchpad [label="evdev-mt-touchpad.c"]
  tablet [label="evdev-tablet.c"]
  pad [label="evdev-tablet-pad.c"]

  evdev -> fallback;
  evdev -> touchpad;
  evdev -> tablet;
  evdev -> pad;

}�jd  }�jf  h�suhjW  hj�  hhhh,hK�ubhE)��}�(h�}While ``evdev.c`` pulls the event out of libevdev, the actual handling of the
events is performed within the dispatch method.�h]�(h9�While �����}�(h�While �hj  hhhNhNubh^)��}�(h�``evdev.c``�h]�h9�evdev.c�����}�(h�evdev.c�hj  ubah}�(h]�h]�h]�h]�h]�uhh]hj  ubh9�l pulls the event out of libevdev, the actual handling of the
events is performed within the dispatch method.�����}�(h�l pulls the event out of libevdev, the actual handling of the
events is performed within the dispatch method.�hj  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj�  hhubjX  )��}�(hhh]�h}�(h]�h]�h]�h]�h]�jb  Xm  digraph context
{
  compound=true;
  rankdir="LR";
  node [
    shape="box";
  ]

  evdev [label="evdev_device_dispatch()"]

  fallback [label="fallback_interface_process()"];
  touchpad [label="tp_interface_process()"]
  tablet [label="tablet_process()"]
  pad [label="pad_process()"]

  evdev -> fallback;
  evdev -> touchpad;
  evdev -> tablet;
  evdev -> pad;
}�jd  }�jf  h�suhjW  hj�  hhhh,hK�ubhE)��}�(hX  The dispatch methods then look at the ``struct input_event`` and proceed to
update the state. Note: the serialized nature of the kernel evdev protocol
requires that the device updates the state with each event but to delay
processing until the ``SYN_REPORT`` event is received.�h]�(h9�&The dispatch methods then look at the �����}�(h�&The dispatch methods then look at the �hjA  hhhNhNubh^)��}�(h�``struct input_event``�h]�h9�struct input_event�����}�(h�struct input_event�hjJ  ubah}�(h]�h]�h]�h]�h]�uhh]hjA  ubh9�� and proceed to
update the state. Note: the serialized nature of the kernel evdev protocol
requires that the device updates the state with each event but to delay
processing until the �����}�(h�� and proceed to
update the state. Note: the serialized nature of the kernel evdev protocol
requires that the device updates the state with each event but to delay
processing until the �hjA  hhhNhNubh^)��}�(h�``SYN_REPORT``�h]�h9�
SYN_REPORT�����}�(h�
SYN_REPORT�hj^  ubah}�(h]�h]�h]�h]�h]�uhh]hjA  ubh9� event is received.�����}�(h� event is received.�hjA  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj�  hhubh)��}�(h�.. _architecture-configuration:�h]�h}�(h]�h]�h]�h]�h]�h*�architecture-configuration�uhhhK�hj�  hhhh,ubeh}�(h]�(�#device-type-specific-event-dispatch�j�  eh]�h]�(�#device-type specific event dispatch��architecture-dispatch�eh]�h]�uhh-hh/hhhh,hK�j�  }�j�  j�  sj�  }�j�  j�  subh.)��}�(hhh]�(h3)��}�(h�Device configuration�h]�h9�Device configuration�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj�  hhhh,hK�ubhE)��}�(hX  All device-specific configuration is handled through ``struct
libinput_device_config_FOO`` instances. These are set up during device init
and provide the function pointers for the ``get``, ``set``, ``get_default``
triplet of configuration queries (or more, where applicable).�h]�(h9�5All device-specific configuration is handled through �����}�(h�5All device-specific configuration is handled through �hj�  hhhNhNubh^)��}�(h�%``struct
libinput_device_config_FOO``�h]�h9�!struct
libinput_device_config_FOO�����}�(h�!struct
libinput_device_config_FOO�hj�  ubah}�(h]�h]�h]�h]�h]�uhh]hj�  ubh9�Z instances. These are set up during device init
and provide the function pointers for the �����}�(h�Z instances. These are set up during device init
and provide the function pointers for the �hj�  hhhNhNubh^)��}�(h�``get``�h]�h9�get�����}�(h�get�hj�  ubah}�(h]�h]�h]�h]�h]�uhh]hj�  ubh9�, �����}�(h�, �hj�  hhhNhNubh^)��}�(h�``set``�h]�h9�set�����}�(h�set�hj�  ubah}�(h]�h]�h]�h]�h]�uhh]hj�  ubh9�, �����}�(hj�  hj�  ubh^)��}�(h�``get_default``�h]�h9�get_default�����}�(h�get_default�hj�  ubah}�(h]�h]�h]�h]�h]�uhh]hj�  ubh9�>
triplet of configuration queries (or more, where applicable).�����}�(h�>
triplet of configuration queries (or more, where applicable).�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj�  hhubhE)��}�(h��For example, the ``struct tablet_dispatch`` for tablet devices has a
``struct libinput_device_config_accel``. This struct is set up with the
required function pointers to change the profiles.�h]�(h9�For example, the �����}�(h�For example, the �hj�  hhhNhNubh^)��}�(h�``struct tablet_dispatch``�h]�h9�struct tablet_dispatch�����}�(h�struct tablet_dispatch�hj  ubah}�(h]�h]�h]�h]�h]�uhh]hj�  ubh9� for tablet devices has a
�����}�(h� for tablet devices has a
�hj�  hhhNhNubh^)��}�(h�'``struct libinput_device_config_accel``�h]�h9�#struct libinput_device_config_accel�����}�(h�#struct libinput_device_config_accel�hj  ubah}�(h]�h]�h]�h]�h]�uhh]hj�  ubh9�S. This struct is set up with the
required function pointers to change the profiles.�����}�(h�S. This struct is set up with the
required function pointers to change the profiles.�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj�  hhubjX  )��}�(hhh]�h}�(h]�h]�h]�h]�h]�jb  X  digraph context
{
  compound=true;
  rankdir="LR";
  node [
    shape="box";
  ]

  tablet [label="struct tablet_dispatch"]
  config [label="struct libinput_device_config_accel"];
  tablet_config [label="tablet_accel_config_set_profile()"];
  tablet->config;
  config->tablet_config;
}�jd  }�jf  h�suhjW  hj�  hhhh,hK�ubhE)��}�(hX  When the matching ``**libinput_device_config_set_FOO()**`` is called, this goes
through to the config struct and invokes the function there. Thus, it is
possible to have different configuration functions for a mouse vs a
touchpad, even though the interface is the same.�h]�(h9�When the matching �����}�(h�When the matching �hj?  hhhNhNubh^)��}�(h�(``**libinput_device_config_set_FOO()**``�h]�h9�$**libinput_device_config_set_FOO()**�����}�(h�$**libinput_device_config_set_FOO()**�hjH  ubah}�(h]�h]�h]�h]�h]�uhh]hj?  ubh9�� is called, this goes
through to the config struct and invokes the function there. Thus, it is
possible to have different configuration functions for a mouse vs a
touchpad, even though the interface is the same.�����}�(h�� is called, this goes
through to the config struct and invokes the function there. Thus, it is
possible to have different configuration functions for a mouse vs a
touchpad, even though the interface is the same.�hj?  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj�  hhubjX  )��}�(hhh]�h}�(h]�h]�h]�h]�h]�jb  ��digraph context
{
  compound=true;
  rankdir="LR";
  node [
    shape="box";
  ]

  libinput [label="**libinput_device_config_accel_set_profile()**"];
  tablet_config [label="tablet_accel_config_set_profile()"];
  libinput->tablet_config;
}�jd  }�jf  h�suhjW  hj�  hhhh,hMubh)��}�(h�.. _architecture-filter:�h]�h}�(h]�h]�h]�h]�h]�h*�architecture-filter�uhhhMhj�  hhhh,ubeh}�(h]�(�device-configuration�j�  eh]�h]�(�device configuration��architecture-configuration�eh]�h]�uhh-hh/hhhh,hK�j�  }�j~  jx  sj�  }�j�  jx  subh.)��}�(hhh]�(h3)��}�(h�Pointer acceleration filters�h]�h9�Pointer acceleration filters�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj�  hhhh,hMubhE)��}�(h�VAll pointer acceleration is handled in the ``filter.c`` file and its
associated files.�h]�(h9�+All pointer acceleration is handled in the �����}�(h�+All pointer acceleration is handled in the �hj�  hhhNhNubh^)��}�(h�``filter.c``�h]�h9�filter.c�����}�(h�filter.c�hj�  ubah}�(h]�h]�h]�h]�h]�uhh]hj�  ubh9� file and its
associated files.�����}�(h� file and its
associated files.�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hMhj�  hhubhE)��}�(h��The ``struct motion_filter`` is initialized during device init, whenever
deltas are available they are passed to ``filter_dispatch()``. This function
returns a set of :ref:`normalized coordinates <motion_normalization_customization>`.�h]�(h9�The �����}�(h�The �hj�  hhhNhNubh^)��}�(h�``struct motion_filter``�h]�h9�struct motion_filter�����}�(h�struct motion_filter�hj�  ubah}�(h]�h]�h]�h]�h]�uhh]hj�  ubh9�U is initialized during device init, whenever
deltas are available they are passed to �����}�(h�U is initialized during device init, whenever
deltas are available they are passed to �hj�  hhhNhNubh^)��}�(h�``filter_dispatch()``�h]�h9�filter_dispatch()�����}�(h�filter_dispatch()�hj�  ubah}�(h]�h]�h]�h]�h]�uhh]hj�  ubh9�!. This function
returns a set of �����}�(h�!. This function
returns a set of �hj�  hhhNhNubh�)��}�(h�B:ref:`normalized coordinates <motion_normalization_customization>`�h]�h�)��}�(h�;normalized coordinates <motion_normalization_customization>�h]�h9�normalized coordinates�����}�(hhhj�  ubah}�(h]�h]�(h��std��std-ref�eh]�h]�h]�uhh�hj�  ubah}�(h]�h]�h]�h]�h]��refdoc�hƌ	refdomain�j�  �reftype��ref��refexplicit���refwarn��ȟ"motion_normalization_customization�uhh�hh,hMhj�  ubh9�.�����}�(h�.�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hMhj�  hhubhE)��}�(h��All actual acceleration is handled within the filter, the device itself has
no further knowledge. Thus it is possible to have different acceleration
filters for the same device types (e.g. the Lenovo X230 touchpad has a
custom filter).�h]�h9��All actual acceleration is handled within the filter, the device itself has
no further knowledge. Thus it is possible to have different acceleration
filters for the same device types (e.g. the Lenovo X230 touchpad has a
custom filter).�����}�(hj  hj  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hMhj�  hhubjX  )��}�(hhh]�h}�(h]�h]�h]�h]�h]�jb  X�  digraph context
{
  compound=true;
  rankdir="LR";
  node [
    shape="box";
  ]

  fallback [label="fallback deltas"];
  touchpad [label="touchpad deltas"];
  tablet [label="tablet deltas"];

  filter [label="filter_dispatch"];

  fallback->filter;
  touchpad->filter;
  tablet->filter;

  flat [label="accelerator_interface_flat()"];
  x230 [label="accelerator_filter_x230()"];
  pen [label="tablet_accelerator_filter_flat_pen()"];

  filter->flat;
  filter->x230;
  filter->pen;

}�jd  }�jf  h�suhjW  hj�  hhhh,hMBubhE)��}�(hX"  Most filters convert the deltas (incl. timestamps) to a motion speed and
then apply a so-called profile function. This function returns a factor that
is then applied to the current delta, converting it into an accelerated
delta. See :ref:`pointer-acceleration` for more details.
the current�h]�(h9��Most filters convert the deltas (incl. timestamps) to a motion speed and
then apply a so-called profile function. This function returns a factor that
is then applied to the current delta, converting it into an accelerated
delta. See �����}�(h��Most filters convert the deltas (incl. timestamps) to a motion speed and
then apply a so-called profile function. This function returns a factor that
is then applied to the current delta, converting it into an accelerated
delta. See �hj-  hhhNhNubh�)��}�(h�:ref:`pointer-acceleration`�h]�h�)��}�(h�pointer-acceleration�h]�h9�pointer-acceleration�����}�(hhhj:  ubah}�(h]�h]�(h��std��std-ref�eh]�h]�h]�uhh�hj6  ubah}�(h]�h]�h]�h]�h]��refdoc�hƌ	refdomain�jE  �reftype��ref��refexplicit���refwarn��ȟpointer-acceleration�uhh�hh,hMChj-  ubh9� for more details.
the current�����}�(h� for more details.
the current�hj-  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hMChj�  hhubeh}�(h]�(�pointer-acceleration-filters�jw  eh]�h]�(�pointer acceleration filters��architecture-filter�eh]�h]�uhh-hh/hhhh,hMj�  }�jh  jm  sj�  }�jw  jm  subeh}�(h]�(� libinput-s-internal-architecture�h+eh]�h]�(� libinput's internal architecture��architecture�eh]�h]�uhh-hhhhhh,hKj�  }�js  h sj�  }�h+h subeh}�(h]�h]�h]�h]�h]��source�h,uhh�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(h2N�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}�(�git_version�h �substitution_definition���)��}�(h�,.. |git_version| replace:: :commit:`f65b1a2`�h]�h �	reference���)��}�(h�git commit f65b1a2�h]�h9�git commit f65b1a2�����}�(h�f65b1a2�hj�  ubah}�(h]�h]�h]�h]�h]��internal���refuri��?https://gitlab.freedesktop.org/libinput/libinput/commit/f65b1a2�uhj�  hj�  ubah}�(h]�h]�h]�j�  ah]�h]�uhj�  h�<rst_prolog>�hKhhub�git_version_full�j�  )��}�(h�].. |git_version_full| replace:: :commit:`<function get_git_version_full at 0x7f38617dc830>`

�h]�j�  )��}�(h�<git commit <function get_git_version_full at 0x7f38617dc830>�h]�h9�<git commit <function get_git_version_full at 0x7f38617dc830>�����}�(h�1<function get_git_version_full at 0x7f38617dc830>�hj�  ubah}�(h]�h]�h]�h]�h]��internal���refuri��ihttps://gitlab.freedesktop.org/libinput/libinput/commit/<function get_git_version_full at 0x7f38617dc830>�uhj�  hj�  ubah}�(h]�h]�h]�j�  ah]�h]�uhj�  hj�  hKhhubu�substitution_names�}�(�git_version�j�  �git_version_full�j�  u�refnames�}��refids�}�(h+]�h ah�]�h�aj�  ]�j�  aj�  ]�j�  aj�  ]�jx  ajw  ]�jm  au�nameids�}�(js  h+jr  jo  j�  h�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j~  j�  j}  jz  jh  jw  jg  jd  u�	nametypes�}�(js  �jr  Nj�  �j�  Nj�  �j�  Nj�  �j�  Nj~  �j}  Njh  �jg  Nuh}�(h+h/jo  h/h�h�j�  h�j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  jz  j�  jw  j�  jd  j�  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]��transform_messages�]�(h �system_message���)��}�(hhh]�hE)��}�(hhh]�h9�2Hyperlink target "architecture" is not referenced.�����}�(hhhj@  ubah}�(h]�h]�h]�h]�h]�uhhDhj=  ubah}�(h]�h]�h]�h]�h]��level�K�type��INFO��source�h,�line�Kuhj;  ubj<  )��}�(hhh]�hE)��}�(hhh]�h9�;Hyperlink target "architecture-contexts" is not referenced.�����}�(hhhj[  ubah}�(h]�h]�h]�h]�h]�uhhDhjX  ubah}�(h]�h]�h]�h]�h]��level�K�type�jU  �source�h,�line�Kuhj;  ubj<  )��}�(hhh]�hE)��}�(hhh]�h9�9Hyperlink target "architecture-device" is not referenced.�����}�(hhhju  ubah}�(h]�h]�h]�h]�h]�uhhDhjr  ubah}�(h]�h]�h]�h]�h]��level�K�type�jU  �source�h,�line�K\uhj;  ubj<  )��}�(hhh]�hE)��}�(hhh]�h9�;Hyperlink target "architecture-dispatch" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�jU  �source�h,�line�K�uhj;  ubj<  )��}�(hhh]�hE)��}�(hhh]�h9�@Hyperlink target "architecture-configuration" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�jU  �source�h,�line�K�uhj;  ubj<  )��}�(hhh]�hE)��}�(hhh]�h9�9Hyperlink target "architecture-filter" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�jU  �source�h,�line�Muhj;  ube�transformer�N�
decoration�Nhhub.