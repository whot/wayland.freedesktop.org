��zo      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �block_quote���)��}�(hhh]��
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�h	�parent�hhh�source�N�line�Nubh �target���)��}�(h�.. _what_is_libinput:�h]�h}�(h]�h]�h]�h]�h]��refid��what-is-libinput�uhhhKhhhhh�</home/whot/code/libinput/build/doc/user/what-is-libinput.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�What is libinput?�h]�h �Text����What is libinput?�����}�(hh6hh4hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hh/hhhh,hKubh �	paragraph���)��}�(h�fThis page describes what libinput is, but more importantly it also describes
what libinput is **not**.�h]�(h9�^This page describes what libinput is, but more importantly it also describes
what libinput is �����}�(h�^This page describes what libinput is, but more importantly it also describes
what libinput is �hhFhhhNhNubh �strong���)��}�(h�**not**�h]�h9�not�����}�(h�not�hhQubah}�(h]�h]�h]�h]�h]�uhhOhhFubh9�.�����}�(h�.�hhFhhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKhh/hhubh)��}�(h�.. _what_libinput_is:�h]�h}�(h]�h]�h]�h]�h]�h*�what-libinput-is�uhhhKhh/hhhh,ubh.)��}�(hhh]�(h3)��}�(h�What libinput is�h]�h9�What libinput is�����}�(hh{hhyhhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hhvhhhh,hKubhE)��}�(hX  libinput is an input stack for processes that need to provide events from
commonly used input devices. That includes mice, keyboards, touchpads,
touchscreens and graphics tablets. libinput handles device-specific quirks
and provides an easy-to-use API to receive events from devices.�h]�h9X  libinput is an input stack for processes that need to provide events from
commonly used input devices. That includes mice, keyboards, touchpads,
touchscreens and graphics tablets. libinput handles device-specific quirks
and provides an easy-to-use API to receive events from devices.�����}�(hh�hh�hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKhhvhhubhE)��}�(hXn  libinput is designed to handle all input devices available on a system but
it is possible to limit which devices libinput has access to.
For example, the use of xf86-input-libinput depends on xorg.conf snippets
for specific devices. But libinput works best if it handles all input
devices as this allows for smarter handling of features that affect multiple
devices.�h]�h9Xn  libinput is designed to handle all input devices available on a system but
it is possible to limit which devices libinput has access to.
For example, the use of xf86-input-libinput depends on xorg.conf snippets
for specific devices. But libinput works best if it handles all input
devices as this allows for smarter handling of features that affect multiple
devices.�����}�(hh�hh�hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKhhvhhubhE)��}�(hX  libinput restricts device-specific features to those devices that require
those features. One example for this are the top software buttons on the
touchpad in the Lenovo T440. While there may be use-cases for providing top
software buttons on other devices, libinput does not do so.�h]�h9X  libinput restricts device-specific features to those devices that require
those features. One example for this are the top software buttons on the
touchpad in the Lenovo T440. While there may be use-cases for providing top
software buttons on other devices, libinput does not do so.�����}�(hh�hh�hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKhhvhhubhE)��}�(h��`This introductory blog post from 2015
<https://who-t.blogspot.com/2015/06/libinput-and-lack-of-device-types.html>`_
describes some of the motivations.�h]�(h �	reference���)��}�(h�t`This introductory blog post from 2015
<https://who-t.blogspot.com/2015/06/libinput-and-lack-of-device-types.html>`_�h]�h9�%This introductory blog post from 2015�����}�(h�%This introductory blog post from 2015�hh�ubah}�(h]�h]�h]�h]�h]��name��%This introductory blog post from 2015��refuri��Ihttps://who-t.blogspot.com/2015/06/libinput-and-lack-of-device-types.html�uhh�hh�ubh)��}�(h�L
<https://who-t.blogspot.com/2015/06/libinput-and-lack-of-device-types.html>�h]�h}�(h]��%this-introductory-blog-post-from-2015�ah]�h]��%this introductory blog post from 2015�ah]�h]��refuri�h�uhh�
referenced�Khh�ubh9�#
describes some of the motivations.�����}�(h�#
describes some of the motivations.�hh�hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK"hhvhhubh)��}�(h�.. _what_libinput_is_not:�h]�h}�(h]�h]�h]�h]�h]�h*�what-libinput-is-not�uhhhK+hhvhhhh,ubeh}�(h]�(hu�id2�eh]�h]�(�what libinput is��what_libinput_is�eh]�h]�uhh-hh/hhhh,hK�expect_referenced_by_name�}�h�hks�expect_referenced_by_id�}�huhksubh.)��}�(hhh]�(h3)��}�(h�What libinput is not�h]�h9�What libinput is not�����}�(hj   hh�hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hh�hhhh,hK*ubhE)��}�(hXr  libinput is **not** a project to support experimental devices. Unless a
device is commonly available off-the-shelf, libinput will not support this
device. libinput can serve as a useful base for getting experimental devices
enabled and reduce the amount of boilerplate required. But such support will
not land in libinput master until the devices are commonly available.�h]�(h9�libinput is �����}�(h�libinput is �hj  hhhNhNubhP)��}�(h�**not**�h]�h9�not�����}�(h�not�hj  ubah}�(h]�h]�h]�h]�h]�uhhOhj  ubh9X_   a project to support experimental devices. Unless a
device is commonly available off-the-shelf, libinput will not support this
device. libinput can serve as a useful base for getting experimental devices
enabled and reduce the amount of boilerplate required. But such support will
not land in libinput master until the devices are commonly available.�����}�(hX_   a project to support experimental devices. Unless a
device is commonly available off-the-shelf, libinput will not support this
device. libinput can serve as a useful base for getting experimental devices
enabled and reduce the amount of boilerplate required. But such support will
not land in libinput master until the devices are commonly available.�hj  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK,hh�hhubhE)��}�(hXT  libinput is **not** a box of legos. It does not provide the pieces to
assemble a selection of features. Many features can be disabled through
configuration options, but some features are hardcoded and/or only available
on some devices. There are plenty of use-cases to provide niche features,
but libinput is not the place to support these.�h]�(h9�libinput is �����}�(h�libinput is �hj/  hhhNhNubhP)��}�(h�**not**�h]�h9�not�����}�(h�not�hj8  ubah}�(h]�h]�h]�h]�h]�uhhOhj/  ubh9XA   a box of legos. It does not provide the pieces to
assemble a selection of features. Many features can be disabled through
configuration options, but some features are hardcoded and/or only available
on some devices. There are plenty of use-cases to provide niche features,
but libinput is not the place to support these.�����}�(hXA   a box of legos. It does not provide the pieces to
assemble a selection of features. Many features can be disabled through
configuration options, but some features are hardcoded and/or only available
on some devices. There are plenty of use-cases to provide niche features,
but libinput is not the place to support these.�hj/  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK2hh�hhubhE)��}�(hXu  libinput is **not** a showcase for features. There are a lot of potential
features that could be provided on input devices. But unless they have
common usage, libinput is not the place to implement them. Every feature
multiplies the maintenance effort, any feature that is provided but unused
is a net drain on the already sparse developer resources libinput has
available.�h]�(h9�libinput is �����}�(h�libinput is �hjR  hhhNhNubhP)��}�(h�**not**�h]�h9�not�����}�(h�not�hj[  ubah}�(h]�h]�h]�h]�h]�uhhOhjR  ubh9Xb   a showcase for features. There are a lot of potential
features that could be provided on input devices. But unless they have
common usage, libinput is not the place to implement them. Every feature
multiplies the maintenance effort, any feature that is provided but unused
is a net drain on the already sparse developer resources libinput has
available.�����}�(hXb   a showcase for features. There are a lot of potential
features that could be provided on input devices. But unless they have
common usage, libinput is not the place to implement them. Every feature
multiplies the maintenance effort, any feature that is provided but unused
is a net drain on the already sparse developer resources libinput has
available.�hjR  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK8hh�hhubhE)��}�(hX  libinput is boring. It does not intend to break new grounds on how devices
are handled. Instead, it takes best practice and the common use-cases and
provides it in an easy-to-consume package for compositors or other processes
that need those interactions typically expected by users.�h]�h9X  libinput is boring. It does not intend to break new grounds on how devices
are handled. Instead, it takes best practice and the common use-cases and
provides it in an easy-to-consume package for compositors or other processes
that need those interactions typically expected by users.�����}�(hjw  hju  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK?hh�hhubh)��}�(h�.. _libinput-wayland:�h]�h}�(h]�h]�h]�h]�h]�h*�libinput-wayland�uhhhKIhh�hhhh,ubeh}�(h]�(h�id3�eh]�h]�(�what libinput is not��what_libinput_is_not�eh]�h]�uhh-hh/hhhh,hK*h�}�j�  h�sh�}�h�h�subh.)��}�(hhh]�(h3)��}�(h�libinput and Wayland�h]�h9�libinput and Wayland�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj�  hhhh,hKHubhE)��}�(h��libinput is not used directly by Wayland applications, it is an input stack
used by the compositor. The typical software stack for a system running
Wayland is:�h]�h9��libinput is not used directly by Wayland applications, it is an input stack
used by the compositor. The typical software stack for a system running
Wayland is:�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKJhj�  hhub�sphinx.ext.graphviz��graphviz���)��}�(hhh]�h}�(h]�h]�h]�h]�h]��code�X�  digraph stack
{
  compound=true;
  rankdir="LR";
  node [
    shape="box";
  ]

  subgraph cluster_2 {
	  label="Kernel";
	  event0 [label="/dev/input/event0"]
	  event1 [label="/dev/input/event1"]
  }

  subgraph cluster_0 {
	  label="Compositor process";
	  libinput;
  }

  client [label="Wayland client"];

  event0 -> libinput;
  event1 -> libinput;
  libinput -> client [ltail=cluster_0 label="Wayland protocol"];
}
��options�}��docname��what-is-libinput�suhj�  hj�  hhhh,hKOubhE)��}�(hX3  The Wayland compositor may be Weston, mutter, KWin, etc. Note that
Wayland encourages the use of toolkits, so the Wayland client (your
application) does not usually talk directly to the compositor but rather
employs a toolkit (e.g. GTK) to do so. The Wayland client does not know
whether libinput is in use.�h]�h9X3  The Wayland compositor may be Weston, mutter, KWin, etc. Note that
Wayland encourages the use of toolkits, so the Wayland client (your
application) does not usually talk directly to the compositor but rather
employs a toolkit (e.g. GTK) to do so. The Wayland client does not know
whether libinput is in use.�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKPhj�  hhubhE)��}�(h��libinput is not a requirement for Wayland or even a Wayland compositor.
There are some specialized compositors that do not need or want libinput.�h]�h9��libinput is not a requirement for Wayland or even a Wayland compositor.
There are some specialized compositors that do not need or want libinput.�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKVhj�  hhubh)��}�(h�.. _libinput-xorg:�h]�h}�(h]�h]�h]�h]�h]�h*�libinput-xorg�uhhhK^hj�  hhhh,ubeh}�(h]�(�libinput-and-wayland�j�  eh]�h]�(�libinput and wayland��libinput-wayland�eh]�h]�uhh-hh/hhhh,hKHh�}�j�  j�  sh�}�j�  j�  subh.)��}�(hhh]�(h3)��}�(h�libinput and X.Org�h]�h9�libinput and X.Org�����}�(hj  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj�  hhhh,hK]ubhE)��}�(h��libinput is not used directly by X applications but rather through the
custom xf86-input-libinput driver. The simplified software stack for a
system running X.Org is:�h]�h9��libinput is not used directly by X applications but rather through the
custom xf86-input-libinput driver. The simplified software stack for a
system running X.Org is:�����}�(hj  hj  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK_hj�  hhubj�  )��}�(hhh]�h}�(h]�h]�h]�h]�h]�j�  X�  digraph stack
{
  compound=true;
  rankdir="LR";
  node [
    shape="box";
  ]

  subgraph cluster_2 {
	  label="Kernel";
	  event0 [label="/dev/input/event0"]
	  event1 [label="/dev/input/event1"]
  }

  subgraph cluster_0 {
	  label="X server process";
	  subgraph cluster_1 {
		  label="xf86-input-libinput"
		  libinput;
	  }
  }

  libinput;
  client [label="X11 client"];

  event0 -> libinput;
  event1 -> libinput;
  libinput -> client [ltail=cluster_0 label="X protocol"];
}
�j�  }�j�  j�  suhj�  hj�  hhhh,hKdubhE)��}�(h��libinput is not employed directly by the X server but by the
xf86-input-libinput driver instead. That driver is loaded by the server
on demand, depending on the xorg.conf.d configuration snippets. The X client
does not know whether libinput is in use.�h]�h9��libinput is not employed directly by the X server but by the
xf86-input-libinput driver instead. That driver is loaded by the server
on demand, depending on the xorg.conf.d configuration snippets. The X client
does not know whether libinput is in use.�����}�(hj(  hj&  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKehj�  hhubhE)��}�(h��libinput and xf86-input-libinput are not a requirement, the driver will only
handle those devices explicitly assigned through an xorg.conf.d snippets. It
is possible to mix xf86-input-libinput with other X.Org drivers.�h]�h9��libinput and xf86-input-libinput are not a requirement, the driver will only
handle those devices explicitly assigned through an xorg.conf.d snippets. It
is possible to mix xf86-input-libinput with other X.Org drivers.�����}�(hj6  hj4  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKjhj�  hhubeh}�(h]�(�libinput-and-x-org�j�  eh]�h]�(�libinput and x.org��libinput-xorg�eh]�h]�uhh-hh/hhhh,hK]h�}�jH  j�  sh�}�j�  j�  subh.)��}�(hhh]�(h3)��}�(h�Device types�h]�h9�Device types�����}�(hjR  hjP  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hjM  hhhh,hKpubhE)��}�(hXo  libinput handles all common devices used to interact with a desktop system.
This includes mice, keyboards, touchscreens, touchpads and graphics tablets.
libinput does not expose the device type to the caller, it solely provides
capabilities and the attached features (see
`this blog post <https://who-t.blogspot.com/2015/06/libinput-and-lack-of-device-types.html>`_).�h]�(h9X  libinput handles all common devices used to interact with a desktop system.
This includes mice, keyboards, touchscreens, touchpads and graphics tablets.
libinput does not expose the device type to the caller, it solely provides
capabilities and the attached features (see
�����}�(hX  libinput handles all common devices used to interact with a desktop system.
This includes mice, keyboards, touchscreens, touchpads and graphics tablets.
libinput does not expose the device type to the caller, it solely provides
capabilities and the attached features (see
�hj^  hhhNhNubh�)��}�(h�]`this blog post <https://who-t.blogspot.com/2015/06/libinput-and-lack-of-device-types.html>`_�h]�h9�this blog post�����}�(h�this blog post�hjg  ubah}�(h]�h]�h]�h]�h]��name��this blog post�hȌIhttps://who-t.blogspot.com/2015/06/libinput-and-lack-of-device-types.html�uhh�hj^  ubh)��}�(h�L <https://who-t.blogspot.com/2015/06/libinput-and-lack-of-device-types.html>�h]�h}�(h]��this-blog-post�ah]�h]��this blog post�ah]�h]��refuri�jx  uhhh�Khj^  ubh9�).�����}�(h�).�hj^  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKrhjM  hhubhE)��}�(hX  For example, a touchpad in libinput is a device that provides pointer
events, gestures and has a number of :ref:`config_options` such as
:ref:`tapping`. A caller may present the device as touchpad to the user, or
simply as device with a config knob to enable or disable tapping.�h]�(h9�kFor example, a touchpad in libinput is a device that provides pointer
events, gestures and has a number of �����}�(h�kFor example, a touchpad in libinput is a device that provides pointer
events, gestures and has a number of �hj�  hhhNhNub�sphinx.addnodes��pending_xref���)��}�(h�:ref:`config_options`�h]�h �inline���)��}�(h�config_options�h]�h9�config_options�����}�(hhhj�  ubah}�(h]�h]�(�xref��std��std-ref�eh]�h]�h]�uhj�  hj�  ubah}�(h]�h]�h]�h]�h]��refdoc�j�  �	refdomain�j�  �reftype��ref��refexplicit���refwarn���	reftarget��config_options�uhj�  hh,hKxhj�  ubh9�	 such as
�����}�(h�	 such as
�hj�  hhhNhNubj�  )��}�(h�:ref:`tapping`�h]�j�  )��}�(h�tapping�h]�h9�tapping�����}�(hhhj�  ubah}�(h]�h]�(j�  �std��std-ref�eh]�h]�h]�uhj�  hj�  ubah}�(h]�h]�h]�h]�h]��refdoc�j�  �	refdomain�j�  �reftype��ref��refexplicit���refwarn��j�  �tapping�uhj�  hh,hKxhj�  ubh9�. A caller may present the device as touchpad to the user, or
simply as device with a config knob to enable or disable tapping.�����}�(h�. A caller may present the device as touchpad to the user, or
simply as device with a config knob to enable or disable tapping.�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKxhjM  hhubh.)��}�(hhh]�(h3)��}�(h�Handled device types�h]�h9�Handled device types�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj�  hhhh,hKubh �bullet_list���)��}�(hhh]�(h �	list_item���)��}�(h�:ref:`Touchpads`�h]�hE)��}�(hj  h]�j�  )��}�(hj  h]�j�  )��}�(h�	Touchpads�h]�h9�	Touchpads�����}�(hhhj  ubah}�(h]�h]�(j�  �std��std-ref�eh]�h]�h]�uhj�  hj  ubah}�(h]�h]�h]�h]�h]��refdoc�j�  �	refdomain�j   �reftype��ref��refexplicit���refwarn��j�  �	touchpads�uhj�  hh,hK�hj  ubah}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj  ubah}�(h]�h]�h]�h]�h]�uhj	  hj  hhhh,hNubj
  )��}�(h�Touchscreens�h]�hE)��}�(hj@  h]�h9�Touchscreens�����}�(hj@  hjB  ubah}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj>  ubah}�(h]�h]�h]�h]�h]�uhj	  hj  hhhh,hNubj
  )��}�(h�Mice�h]�hE)��}�(hjW  h]�h9�Mice�����}�(hjW  hjY  ubah}�(h]�h]�h]�h]�h]�uhhDhh,hK�hjU  ubah}�(h]�h]�h]�h]�h]�uhj	  hj  hhhh,hNubj
  )��}�(h�	Keyboards�h]�hE)��}�(hjn  h]�h9�	Keyboards�����}�(hjn  hjp  ubah}�(h]�h]�h]�h]�h]�uhhDhh,hK�hjl  ubah}�(h]�h]�h]�h]�h]�uhj	  hj  hhhh,hNubj
  )��}�(h�JVirtual absolute pointing devices such as those used by QEMU or VirtualBox�h]�hE)��}�(hj�  h]�h9�JVirtual absolute pointing devices such as those used by QEMU or VirtualBox�����}�(hj�  hj�  ubah}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj�  ubah}�(h]�h]�h]�h]�h]�uhj	  hj  hhhh,hNubj
  )��}�(h�,Switches (Lid Switch and Tablet Mode switch)�h]�hE)��}�(hj�  h]�h9�,Switches (Lid Switch and Tablet Mode switch)�����}�(hj�  hj�  ubah}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj�  ubah}�(h]�h]�h]�h]�h]�uhj	  hj  hhhh,hNubj
  )��}�(h�Graphics tablets�h]�hE)��}�(hj�  h]�h9�Graphics tablets�����}�(hj�  hj�  ubah}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj�  ubah}�(h]�h]�h]�h]�h]�uhj	  hj  hhhh,hNubj
  )��}�(h�:ref:`Trackpoints`
�h]�hE)��}�(h�:ref:`Trackpoints`�h]�j�  )��}�(hj�  h]�j�  )��}�(h�Trackpoints�h]�h9�Trackpoints�����}�(hhhj�  ubah}�(h]�h]�(j�  �std��std-ref�eh]�h]�h]�uhj�  hj�  ubah}�(h]�h]�h]�h]�h]��refdoc�j�  �	refdomain�j�  �reftype��ref��refexplicit���refwarn��j�  �trackpoints�uhj�  hh,hK�hj�  ubah}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj�  ubah}�(h]�h]�h]�h]�h]�uhj	  hj  hhhh,hNubeh}�(h]�h]�h]�h]�h]��bullet��-�uhj  hh,hK�hj�  hhubhE)��}�(h�|If a device falls into one of the above categories but does not work as
expected, please :ref:`file a bug <reporting_bugs>`.�h]�(h9�YIf a device falls into one of the above categories but does not work as
expected, please �����}�(h�YIf a device falls into one of the above categories but does not work as
expected, please �hj  hhhNhNubj�  )��}�(h�":ref:`file a bug <reporting_bugs>`�h]�j�  )��}�(h�file a bug <reporting_bugs>�h]�h9�
file a bug�����}�(hhhj  ubah}�(h]�h]�(j�  �std��std-ref�eh]�h]�h]�uhj�  hj  ubah}�(h]�h]�h]�h]�h]��refdoc�j�  �	refdomain�j  �reftype��ref��refexplicit���refwarn��j�  �reporting_bugs�uhj�  hh,hK�hj  ubh9�.�����}�(hhdhj  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj�  hhubeh}�(h]��handled-device-types�ah]�h]��handled device types�ah]�h]�uhh-hjM  hhhh,hKubh.)��}�(hhh]�(h3)��}�(h�Unhandled device types�h]�h9�Unhandled device types�����}�(hjE  hjC  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj@  hhhh,hK�ubhE)��}�(h��libinput does not handle some devices. The primary reason is that these
device have no clear interaction with a desktop environment.�h]�h9��libinput does not handle some devices. The primary reason is that these
device have no clear interaction with a desktop environment.�����}�(hjS  hjQ  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj@  hhubh �definition_list���)��}�(hhh]�h �definition_list_item���)��}�(hX�  Joysticks:
Joysticks have one or more axes and one or more buttons. Beyond that it is
difficult to find common ground between joysticks and much of the
interaction is application-specific, not system-specific. libinput does not
provide support for joysticks for that reason, any abstraction libinput
would provide for joysticks would be so generic that libinput would
merely introduce complexity and processing delays for no real benefit.�h]�(h �term���)��}�(h�
Joysticks:�h]�h9�
Joysticks:�����}�(hjn  hjl  ubah}�(h]�h]�h]�h]�h]�uhjj  hh,hK�hjf  ubh �
definition���)��}�(hhh]�hE)��}�(hX�  Joysticks have one or more axes and one or more buttons. Beyond that it is
difficult to find common ground between joysticks and much of the
interaction is application-specific, not system-specific. libinput does not
provide support for joysticks for that reason, any abstraction libinput
would provide for joysticks would be so generic that libinput would
merely introduce complexity and processing delays for no real benefit.�h]�h9X�  Joysticks have one or more axes and one or more buttons. Beyond that it is
difficult to find common ground between joysticks and much of the
interaction is application-specific, not system-specific. libinput does not
provide support for joysticks for that reason, any abstraction libinput
would provide for joysticks would be so generic that libinput would
merely introduce complexity and processing delays for no real benefit.�����}�(hj�  hj  ubah}�(h]�h]�h]�h]�h]�uhhDhh,hK�hj|  ubah}�(h]�h]�h]�h]�h]�uhjz  hjf  ubeh}�(h]�h]�h]�h]�h]�uhjd  hh,hK�hja  ubah}�(h]�h]�h]�h]�h]�uhj_  hj@  hhhh,hNubeh}�(h]��unhandled-device-types�ah]�h]��unhandled device types�ah]�h]�uhh-hjM  hhhh,hK�ubeh}�(h]��device-types�ah]�h]��device types�ah]�h]�uhh-hh/hhhh,hKpubeh}�(h]�(h+�id1�eh]�h]�(�what is libinput?��what_is_libinput�eh]�h]�uhh-hhhhhh,hKh�}�j�  h sh�}�h+h subeh}�(h]�h]�h]�h]�h]��source�h,uhh�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(h2N�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}�(�git_version�h �substitution_definition���)��}�(h�,.. |git_version| replace:: :commit:`f65b1a2`�h]�h�)��}�(h�git commit f65b1a2�h]�h9�git commit f65b1a2�����}�(h�f65b1a2�hj  ubah}�(h]�h]�h]�h]�h]��internal���refuri��?https://gitlab.freedesktop.org/libinput/libinput/commit/f65b1a2�uhh�hj  ubah}�(h]�h]�h]�j  ah]�h]�uhj  h�<rst_prolog>�hKhhub�git_version_full�j  )��}�(h�^.. |git_version_full| replace:: :commit:`<function get_git_version_full at 0x7f38617dc830>`


�h]�h�)��}�(h�<git commit <function get_git_version_full at 0x7f38617dc830>�h]�h9�<git commit <function get_git_version_full at 0x7f38617dc830>�����}�(h�1<function get_git_version_full at 0x7f38617dc830>�hj9  ubah}�(h]�h]�h]�h]�h]��internal���refuri��ihttps://gitlab.freedesktop.org/libinput/libinput/commit/<function get_git_version_full at 0x7f38617dc830>�uhh�hj5  ubah}�(h]�h]�h]�j4  ah]�h]�uhj  hj3  hKhhubu�substitution_names�}�(�git_version�j  �git_version_full�j4  u�refnames�}��refids�}�(h+]�h ahu]�hkah�]�h�aj�  ]�j�  aj�  ]�j�  au�nameids�}�(j�  h+j�  j�  h�huh�h�h�h�j�  h�j�  j�  j�  j�  j�  j�  jH  j�  jG  jD  j�  j�  j�  j  j=  j:  j�  j�  u�	nametypes�}�(j�  �j�  Nh�h�Nhӈj�  �j�  Nj�  �j�  NjH  �jG  Nj�  Nj�  �j=  Nj�  Nuh}�(h+h/j�  h/huhvh�hvh�h�h�h�j�  h�j�  j�  j�  j�  j�  j�  jD  j�  j�  jM  j  jy  j:  j�  j�  j@  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]��transform_messages�]�(h �system_message���)��}�(hhh]�hE)��}�(hhh]�h9�6Hyperlink target "what-is-libinput" is not referenced.�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]�uhhDhj|  ubah}�(h]�h]�h]�h]�h]��level�K�type��INFO��source�h,�line�Kuhjz  ubj{  )��}�(hhh]�hE)��}�(hhh]�h9�6Hyperlink target "what-libinput-is" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h,�line�Kuhjz  ubj{  )��}�(hhh]�hE)��}�(hhh]�h9�:Hyperlink target "what-libinput-is-not" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h,�line�K+uhjz  ubj{  )��}�(hhh]�hE)��}�(hhh]�h9�6Hyperlink target "libinput-wayland" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h,�line�KIuhjz  ubj{  )��}�(hhh]�hE)��}�(hhh]�h9�3Hyperlink target "libinput-xorg" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h,�line�K^uhjz  ube�transformer�N�
decoration�Nhhub.