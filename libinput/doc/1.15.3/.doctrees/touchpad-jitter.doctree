���M      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �block_quote���)��}�(hhh]��
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�h	�parent�hhh�source�N�line�Nubh �target���)��}�(h�.. _touchpad_jitter:�h]�h}�(h]�h]�h]�h]�h]��refid��touchpad-jitter�uhhhKhhhhh�Z/home/whot/tmp/2020-03-06-Fri/wayland-web-sync/libinput/build/doc/user/touchpad-jitter.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�Touchpad jitter�h]�h �Text����Touchpad jitter�����}�(hh6hh4hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hh/hhhh,hKubh �	paragraph���)��}�(h�bTouchpad jitter describes random movement by a few pixels even when the
user's finger is unmoving.�h]�h9�dTouchpad jitter describes random movement by a few pixels even when the
user’s finger is unmoving.�����}�(hhHhhFhhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKhh/hhubhE)��}�(hXZ  libinput has a mechanism called a **hysteresis** to avoid that jitter. When
active, movement with in the **hysteresis margin** is discarded. If the
movement delta is larger than the margin, the movement is passed on as
pointer movement. This is a simplified summary, developers should
read the implementation of the hysteresis in ``src/evdev.c``.�h]�(h9�"libinput has a mechanism called a �����}�(h�"libinput has a mechanism called a �hhThhhNhNubh �strong���)��}�(h�**hysteresis**�h]�h9�
hysteresis�����}�(h�
hysteresis�hh_ubah}�(h]�h]�h]�h]�h]�uhh]hhTubh9�9 to avoid that jitter. When
active, movement with in the �����}�(h�9 to avoid that jitter. When
active, movement with in the �hhThhhNhNubh^)��}�(h�**hysteresis margin**�h]�h9�hysteresis margin�����}�(h�hysteresis margin�hhsubah}�(h]�h]�h]�h]�h]�uhh]hhTubh9�� is discarded. If the
movement delta is larger than the margin, the movement is passed on as
pointer movement. This is a simplified summary, developers should
read the implementation of the hysteresis in �����}�(h�� is discarded. If the
movement delta is larger than the margin, the movement is passed on as
pointer movement. This is a simplified summary, developers should
read the implementation of the hysteresis in �hhThhhNhNubh �literal���)��}�(h�``src/evdev.c``�h]�h9�src/evdev.c�����}�(h�src/evdev.c�hh�ubah}�(h]�h]�h]�h]�h]�uhh�hhTubh9�.�����}�(h�.�hhThhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK
hh/hhubhE)��}�(h��libinput uses the kernel ``fuzz`` value to determine the size of the
hysteresis. Users should override this with a udev hwdb entry where the
device itself does not provide the correct value.�h]�(h9�libinput uses the kernel �����}�(h�libinput uses the kernel �hh�hhhNhNubh�)��}�(h�``fuzz``�h]�h9�fuzz�����}�(h�fuzz�hh�ubah}�(h]�h]�h]�h]�h]�uhh�hh�ubh9�� value to determine the size of the
hysteresis. Users should override this with a udev hwdb entry where the
device itself does not provide the correct value.�����}�(h�� value to determine the size of the
hysteresis. Users should override this with a udev hwdb entry where the
device itself does not provide the correct value.�hh�hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKhh/hhubh)��}�(h�".. _touchpad_jitter_fuzz_override:�h]�h}�(h]�h]�h]�h]�h]�h*�touchpad-jitter-fuzz-override�uhhhKhh/hhhh,ubh.)��}�(hhh]�(h3)��}�(h�!Overriding the hysteresis margins�h]�h9�!Overriding the hysteresis margins�����}�(hh�hh�hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hh�hhhh,hKubhE)��}�(hX  libinput provides the debugging tool ``libinput measure fuzz`` to help edit or
test a fuzz value. This tool is interactive and provides a udev hwdb entry
that matches the device. To check if a fuzz is currently present, simply run
without arguments or with the touchpad's device node:�h]�(h9�%libinput provides the debugging tool �����}�(h�%libinput provides the debugging tool �hh�hhhNhNubh�)��}�(h�``libinput measure fuzz``�h]�h9�libinput measure fuzz�����}�(h�libinput measure fuzz�hh�ubah}�(h]�h]�h]�h]�h]�uhh�hh�ubh9�� to help edit or
test a fuzz value. This tool is interactive and provides a udev hwdb entry
that matches the device. To check if a fuzz is currently present, simply run
without arguments or with the touchpad’s device node:�����}�(h�� to help edit or
test a fuzz value. This tool is interactive and provides a udev hwdb entry
that matches the device. To check if a fuzz is currently present, simply run
without arguments or with the touchpad's device node:�hh�hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKhh�hhubh �literal_block���)��}�(h��$ sudo libinput measure fuzz
Using Synaptics TM2668-002: /dev/input/event17
  Checking udev property... not set
  Checking axes... x=16 y=16�h]�h9��$ sudo libinput measure fuzz
Using Synaptics TM2668-002: /dev/input/event17
  Checking udev property... not set
  Checking axes... x=16 y=16�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]��	xml:space��preserve�uhj  hh,hK"hh�hhubhE)��}�(h�rIn the above output, the axis fuzz is set to 16. To set a specific fuzz, run
with the ``--fuzz=<value>`` argument.�h]�(h9�VIn the above output, the axis fuzz is set to 16. To set a specific fuzz, run
with the �����}�(h�VIn the above output, the axis fuzz is set to 16. To set a specific fuzz, run
with the �hj  hhhNhNubh�)��}�(h�``--fuzz=<value>``�h]�h9�--fuzz=<value>�����}�(h�--fuzz=<value>�hj   ubah}�(h]�h]�h]�h]�h]�uhh�hj  ubh9�
 argument.�����}�(h�
 argument.�hj  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK(hh�hhubj  )��}�(h�%$ sudo libinput measure fuzz --fuzz=8�h]�h9�%$ sudo libinput measure fuzz --fuzz=8�����}�(hhhj:  ubah}�(h]�h]�h]�h]�h]�j  j  uhj  hh,hK.hh�hhubhE)��}�(h�mThe tool will attempt to construct a hwdb file that matches your touchpad
device. Follow the printed prompts.�h]�h9�mThe tool will attempt to construct a hwdb file that matches your touchpad
device. Follow the printed prompts.�����}�(hjJ  hjH  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK1hh�hhubhE)��}�(h�qIn the ideal case, the tool will provide you with a file that can be
submitted to the systemd repo for inclusion.�h]�h9�qIn the ideal case, the tool will provide you with a file that can be
submitted to the systemd repo for inclusion.�����}�(hjX  hjV  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK4hh�hhubhE)��}�(h��However, hwdb entry creation is difficult to automate and it's likely
that the tools fails in doing so, especially if an existing entry is already
present.�h]�h9��However, hwdb entry creation is difficult to automate and it’s likely
that the tools fails in doing so, especially if an existing entry is already
present.�����}�(hjf  hjd  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK7hh�hhubhE)��}�(h��Below is the outline of what a user needs to do to override a device's fuzz
value in case the ``libinput measure fuzz`` tool fails.�h]�(h9�`Below is the outline of what a user needs to do to override a device’s fuzz
value in case the �����}�(h�^Below is the outline of what a user needs to do to override a device's fuzz
value in case the �hjr  hhhNhNubh�)��}�(h�``libinput measure fuzz``�h]�h9�libinput measure fuzz�����}�(h�libinput measure fuzz�hj{  ubah}�(h]�h]�h]�h]�h]�uhh�hjr  ubh9� tool fails.�����}�(h� tool fails.�hjr  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK;hh�hhubhE)��}�(hX0  Check with ``udevadm info /sys/class/input/eventX`` (replace your device node
number) whether an existing hwdb override exists. If the ``EVDEV_ABS_``
properties are present, the hwdb override exists. Find the file that
contains that entry, most likely in ``/etc/udev/hwdb.d`` or
``/usr/lib/udev/hwdb.d``.�h]�(h9�Check with �����}�(h�Check with �hj�  hhhNhNubh�)��}�(h�(``udevadm info /sys/class/input/eventX``�h]�h9�$udevadm info /sys/class/input/eventX�����}�(h�$udevadm info /sys/class/input/eventX�hj�  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubh9�T (replace your device node
number) whether an existing hwdb override exists. If the �����}�(h�T (replace your device node
number) whether an existing hwdb override exists. If the �hj�  hhhNhNubh�)��}�(h�``EVDEV_ABS_``�h]�h9�
EVDEV_ABS_�����}�(h�
EVDEV_ABS_�hj�  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubh9�j
properties are present, the hwdb override exists. Find the file that
contains that entry, most likely in �����}�(h�j
properties are present, the hwdb override exists. Find the file that
contains that entry, most likely in �hj�  hhhNhNubh�)��}�(h�``/etc/udev/hwdb.d``�h]�h9�/etc/udev/hwdb.d�����}�(h�/etc/udev/hwdb.d�hj�  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubh9� or
�����}�(h� or
�hj�  hhhNhNubh�)��}�(h�``/usr/lib/udev/hwdb.d``�h]�h9�/usr/lib/udev/hwdb.d�����}�(h�/usr/lib/udev/hwdb.d�hj�  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubh9�.�����}�(hh�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK>hh�hhubhE)��}�(hX   The content of the property is a set of values in the format
``EVDEV_ABS_00=min:max:resolution:fuzz``. You need to set the ``fuzz`` part,
leaving the remainder of the property as-is. Values may be empty, e.g. a
property that only sets resolution and fuzz reads as ``EVDEV_ABS_00=::32:8``.�h]�(h9�=The content of the property is a set of values in the format
�����}�(h�=The content of the property is a set of values in the format
�hj�  hhhNhNubh�)��}�(h�(``EVDEV_ABS_00=min:max:resolution:fuzz``�h]�h9�$EVDEV_ABS_00=min:max:resolution:fuzz�����}�(h�$EVDEV_ABS_00=min:max:resolution:fuzz�hj�  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubh9�. You need to set the �����}�(h�. You need to set the �hj�  hhhNhNubh�)��}�(h�``fuzz``�h]�h9�fuzz�����}�(h�fuzz�hj  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubh9�� part,
leaving the remainder of the property as-is. Values may be empty, e.g. a
property that only sets resolution and fuzz reads as �����}�(h�� part,
leaving the remainder of the property as-is. Values may be empty, e.g. a
property that only sets resolution and fuzz reads as �hj�  hhhNhNubh�)��}�(h�``EVDEV_ABS_00=::32:8``�h]�h9�EVDEV_ABS_00=::32:8�����}�(h�EVDEV_ABS_00=::32:8�hj$  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubh9�.�����}�(hh�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKDhh�hhubhE)��}�(h�LIf no properties exist, your hwdb.entry should look approximately like this:�h]�h9�LIf no properties exist, your hwdb.entry should look approximately like this:�����}�(hj?  hj=  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKIhh�hhubj  )��}�(h��evdev:name:Synaptics TM2668-002:dmi:*:svnLENOVO*:pvrThinkPadT440s*:
 EVDEV_ABS_00=:::8
 EVDEV_ABS_01=:::8
 EVDEV_ABS_35=:::8
 EVDEV_ABS_36=:::8�h]�h9��evdev:name:Synaptics TM2668-002:dmi:*:svnLENOVO*:pvrThinkPadT440s*:
 EVDEV_ABS_00=:::8
 EVDEV_ABS_01=:::8
 EVDEV_ABS_35=:::8
 EVDEV_ABS_36=:::8�����}�(hhhjK  ubah}�(h]�h]�h]�h]�h]�j  j  uhj  hh,hKMhh�hhubhE)��}�(h��Substitute the ``name`` field with the device name (see the output of
``libinput measure fuzz`` and the DMI match content with your hardware. See
:ref:`hwdb_modifying` for details.�h]�(h9�Substitute the �����}�(h�Substitute the �hjY  hhhNhNubh�)��}�(h�``name``�h]�h9�name�����}�(h�name�hjb  ubah}�(h]�h]�h]�h]�h]�uhh�hjY  ubh9�/ field with the device name (see the output of
�����}�(h�/ field with the device name (see the output of
�hjY  hhhNhNubh�)��}�(h�``libinput measure fuzz``�h]�h9�libinput measure fuzz�����}�(h�libinput measure fuzz�hjv  ubah}�(h]�h]�h]�h]�h]�uhh�hjY  ubh9�3 and the DMI match content with your hardware. See
�����}�(h�3 and the DMI match content with your hardware. See
�hjY  hhhNhNub�sphinx.addnodes��pending_xref���)��}�(h�:ref:`hwdb_modifying`�h]�h �inline���)��}�(h�hwdb_modifying�h]�h9�hwdb_modifying�����}�(hhhj�  ubah}�(h]�h]�(�xref��std��std-ref�eh]�h]�h]�uhj�  hj�  ubah}�(h]�h]�h]�h]�h]��refdoc��touchpad-jitter��	refdomain�j�  �reftype��ref��refexplicit���refwarn���	reftarget��hwdb_modifying�uhj�  hh,hKThjY  ubh9� for details.�����}�(h� for details.�hjY  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKThh�hhubhE)��}�(h��Once the hwdb entry has been modified, added, or created,
:ref:`reload the hwdb <hwdb_reloading>`. Once reloaded, :ref:`libinput-record`
"libinput record" should show the new fuzz value for the axes.�h]�(h9�:Once the hwdb entry has been modified, added, or created,
�����}�(h�:Once the hwdb entry has been modified, added, or created,
�hj�  hhhNhNubj�  )��}�(h�':ref:`reload the hwdb <hwdb_reloading>`�h]�j�  )��}�(h� reload the hwdb <hwdb_reloading>�h]�h9�reload the hwdb�����}�(hhhj�  ubah}�(h]�h]�(j�  �std��std-ref�eh]�h]�h]�uhj�  hj�  ubah}�(h]�h]�h]�h]�h]��refdoc�j�  �	refdomain�j�  �reftype��ref��refexplicit���refwarn��j�  �hwdb_reloading�uhj�  hh,hKXhj�  ubh9�. Once reloaded, �����}�(h�. Once reloaded, �hj�  hhhNhNubj�  )��}�(h�:ref:`libinput-record`�h]�j�  )��}�(h�libinput-record�h]�h9�libinput-record�����}�(hhhj�  ubah}�(h]�h]�(j�  �std��std-ref�eh]�h]�h]�uhj�  hj�  ubah}�(h]�h]�h]�h]�h]��refdoc�j�  �	refdomain�j�  �reftype��ref��refexplicit���refwarn��j�  �libinput-record�uhj�  hh,hKXhj�  ubh9�C
“libinput record” should show the new fuzz value for the axes.�����}�(h�?
"libinput record" should show the new fuzz value for the axes.�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKXhh�hhubhE)��}�(h�ERestart the host and libinput should pick up the revised fuzz values.�h]�h9�ERestart the host and libinput should pick up the revised fuzz values.�����}�(hj  hj  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK\hh�hhubh)��}�(h�.. _kernel_fuzz:�h]�h}�(h]�h]�h]�h]�h]�h*�kernel-fuzz�uhhhKchh�hhhh,ubeh}�(h]�(�!overriding-the-hysteresis-margins�h�eh]�h]�(�!overriding the hysteresis margins��touchpad_jitter_fuzz_override�eh]�h]�uhh-hh/hhhh,hK�expect_referenced_by_name�}�j8  h�s�expect_referenced_by_id�}�h�h�subh.)��}�(hhh]�(h3)��}�(h�Kernel fuzz�h]�h9�Kernel fuzz�����}�(hjD  hjB  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj?  hhhh,hKbubhE)��}�(hX�  A fuzz set on an absolute axis in the kernel causes the kernel to apply
hysteresis-like behavior to the axis. Unfortunately, this behavior leads to
inconsistent deltas. To avoid this, libinput sets the kernel fuzz on the
device to 0 to disable this kernel behavior but remembers what the fuzz was
on startup. The fuzz is stored in the ``LIBINPUT_FUZZ_XX`` udev property, on
startup libinput will check that property as well as the axis itself.�h]�(h9XO  A fuzz set on an absolute axis in the kernel causes the kernel to apply
hysteresis-like behavior to the axis. Unfortunately, this behavior leads to
inconsistent deltas. To avoid this, libinput sets the kernel fuzz on the
device to 0 to disable this kernel behavior but remembers what the fuzz was
on startup. The fuzz is stored in the �����}�(hXO  A fuzz set on an absolute axis in the kernel causes the kernel to apply
hysteresis-like behavior to the axis. Unfortunately, this behavior leads to
inconsistent deltas. To avoid this, libinput sets the kernel fuzz on the
device to 0 to disable this kernel behavior but remembers what the fuzz was
on startup. The fuzz is stored in the �hjP  hhhNhNubh�)��}�(h�``LIBINPUT_FUZZ_XX``�h]�h9�LIBINPUT_FUZZ_XX�����}�(h�LIBINPUT_FUZZ_XX�hjY  ubah}�(h]�h]�h]�h]�h]�uhh�hjP  ubh9�X udev property, on
startup libinput will check that property as well as the axis itself.�����}�(h�X udev property, on
startup libinput will check that property as well as the axis itself.�hjP  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKdhj?  hhubeh}�(h]�(j1  �id2�eh]�h]�(�kernel fuzz��kernel_fuzz�eh]�h]�uhh-hh/hhhh,hKbj;  }�jy  j'  sj=  }�j1  j'  subeh}�(h]�(h+�id1�eh]�h]�(�touchpad jitter��touchpad_jitter�eh]�h]�uhh-hhhhhh,hKj;  }�j�  h sj=  }�h+h subeh}�(h]�h]�h]�h]�h]��source�h,uhh�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(h2N�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}�(�git_version�h �substitution_definition���)��}�(h�,.. |git_version| replace:: :commit:`a1c8909`�h]�h �	reference���)��}�(h�git commit a1c8909�h]�h9�git commit a1c8909�����}�(h�a1c8909�hj�  ubah}�(h]�h]�h]�h]�h]��internal���refuri��?https://gitlab.freedesktop.org/libinput/libinput/commit/a1c8909�uhj�  hj�  ubah}�(h]�h]�h]�j�  ah]�h]�uhj�  h�<rst_prolog>�hKhhub�git_version_full�j�  )��}�(h�].. |git_version_full| replace:: :commit:`<function get_git_version_full at 0x7f38f2bfb5f0>`

�h]�j�  )��}�(h�<git commit <function get_git_version_full at 0x7f38f2bfb5f0>�h]�h9�<git commit <function get_git_version_full at 0x7f38f2bfb5f0>�����}�(h�1<function get_git_version_full at 0x7f38f2bfb5f0>�hj
  ubah}�(h]�h]�h]�h]�h]��internal���refuri��ihttps://gitlab.freedesktop.org/libinput/libinput/commit/<function get_git_version_full at 0x7f38f2bfb5f0>�uhj�  hj  ubah}�(h]�h]�h]�j  ah]�h]�uhj�  hj  hKhhubu�substitution_names�}�(�git_version�j�  �git_version_full�j  u�refnames�}��refids�}�(h+]�h ah�]�h�aj1  ]�j'  au�nameids�}�(j�  h+j�  j�  j8  h�j7  j4  jy  j1  jx  ju  u�	nametypes�}�(j�  �j�  Nj8  �j7  Njy  �jx  Nuh}�(h+h/j�  h/h�h�j4  h�j1  j?  ju  j?  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]��transform_messages�]�(h �system_message���)��}�(hhh]�hE)��}�(hhh]�h9�5Hyperlink target "touchpad-jitter" is not referenced.�����}�(hhhjN  ubah}�(h]�h]�h]�h]�h]�uhhDhjK  ubah}�(h]�h]�h]�h]�h]��level�K�type��INFO��source�h,�line�KuhjI  ubjJ  )��}�(hhh]�hE)��}�(hhh]�h9�CHyperlink target "touchpad-jitter-fuzz-override" is not referenced.�����}�(hhhji  ubah}�(h]�h]�h]�h]�h]�uhhDhjf  ubah}�(h]�h]�h]�h]�h]��level�K�type�jc  �source�h,�line�KuhjI  ubjJ  )��}�(hhh]�hE)��}�(hhh]�h9�1Hyperlink target "kernel-fuzz" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�jc  �source�h,�line�KcuhjI  ube�transformer�N�
decoration�Nhhub.