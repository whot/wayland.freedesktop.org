���0      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �block_quote���)��}�(hhh]��
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�h	�parent�hhh�source�N�line�Nubh �target���)��}�(h�.. _switches:�h]�h}�(h]�h]�h]�h]�h]��refid��switches�uhhhKhhhhh�4/home/whot/code/libinput/build/doc/user/switches.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�Switches�h]�h �Text����Switches�����}�(hh6hh4hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hh/hhhh,hKubh �	paragraph���)��}�(h��libinput supports the lid and tablet-mode switches. Unlike button events
that come in press and release pairs, switches are usually toggled once and
left at the setting for an extended period of time.�h]�h9��libinput supports the lid and tablet-mode switches. Unlike button events
that come in press and release pairs, switches are usually toggled once and
left at the setting for an extended period of time.�����}�(hhHhhFhhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKhh/hhubhE)��}�(hX  Only some switches are handled by libinput, see **libinput_switch** for a
list of supported switches. Switch events are exposed to the caller, but
libinput may handle some switch events internally and enable or disable
specific features based on a switch state.�h]�(h9�0Only some switches are handled by libinput, see �����}�(h�0Only some switches are handled by libinput, see �hhThhhNhNubh �strong���)��}�(h�**libinput_switch**�h]�h9�libinput_switch�����}�(hhhh_ubah}�(h]�h]�h]�h]�h]�uhh]hhTubh9�� for a
list of supported switches. Switch events are exposed to the caller, but
libinput may handle some switch events internally and enable or disable
specific features based on a switch state.�����}�(h�� for a
list of supported switches. Switch events are exposed to the caller, but
libinput may handle some switch events internally and enable or disable
specific features based on a switch state.�hhThhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKhh/hhubhE)��}�(h��The order of switch events is guaranteed to be correct, i.e., a switch will
never send consecutive switch on, or switch off, events.�h]�h9��The order of switch events is guaranteed to be correct, i.e., a switch will
never send consecutive switch on, or switch off, events.�����}�(hhzhhxhhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKhh/hhubh)��}�(h�.. _switches_lid:�h]�h}�(h]�h]�h]�h]�h]�h*�switches-lid�uhhhKhh/hhhh,ubh.)��}�(hhh]�(h3)��}�(h�Lid switch handling�h]�h9�Lid switch handling�����}�(hh�hh�hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hh�hhhh,hKubhE)��}�(hX�  Where available, libinput listens to devices providing a lid switch.
The evdev event code ``EV_SW`` ``SW_LID`` is provided as
**LIBINPUT_SWITCH_LID**. If devices with a lid switch have a touchpad device,
the device is disabled while the lid is logically closed. This is to avoid
ghost touches that can be caused by interference with touchpads and the
closed lid. The touchpad is automatically re-enabled whenever the lid is
openend.�h]�(h9�ZWhere available, libinput listens to devices providing a lid switch.
The evdev event code �����}�(h�ZWhere available, libinput listens to devices providing a lid switch.
The evdev event code �hh�hhhNhNubh �literal���)��}�(h�	``EV_SW``�h]�h9�EV_SW�����}�(hhhh�ubah}�(h]�h]�h]�h]�h]�uhh�hh�ubh9� �����}�(h� �hh�hhhNhNubh�)��}�(h�
``SW_LID``�h]�h9�SW_LID�����}�(hhhh�ubah}�(h]�h]�h]�h]�h]�uhh�hh�ubh9� is provided as
�����}�(h� is provided as
�hh�hhhNhNubh^)��}�(h�**LIBINPUT_SWITCH_LID**�h]�h9�LIBINPUT_SWITCH_LID�����}�(hhhh�ubah}�(h]�h]�h]�h]�h]�uhh]hh�ubh9X  . If devices with a lid switch have a touchpad device,
the device is disabled while the lid is logically closed. This is to avoid
ghost touches that can be caused by interference with touchpads and the
closed lid. The touchpad is automatically re-enabled whenever the lid is
openend.�����}�(hX  . If devices with a lid switch have a touchpad device,
the device is disabled while the lid is logically closed. This is to avoid
ghost touches that can be caused by interference with touchpads and the
closed lid. The touchpad is automatically re-enabled whenever the lid is
openend.�hh�hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKhh�hhubhE)��}�(h��This handling of lid switches is transparent to the user, no notifications
are sent and the device appears as enabled at all times.�h]�h9��This handling of lid switches is transparent to the user, no notifications
are sent and the device appears as enabled at all times.�����}�(hh�hh�hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK!hh�hhubhE)��}�(hX  On some devices, the device's lid state does not always reflect the physical
state and the lid state may report as closed even when the lid is physicall
open. libinput employs some heuristics to detect user input (specificially
typing) to re-enable the touchpad on those devices.�h]�h9X  On some devices, the device’s lid state does not always reflect the physical
state and the lid state may report as closed even when the lid is physicall
open. libinput employs some heuristics to detect user input (specificially
typing) to re-enable the touchpad on those devices.�����}�(hh�hh�hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK$hh�hhubh)��}�(h�.. _switches_tablet_mode:�h]�h}�(h]�h]�h]�h]�h]�h*�switches-tablet-mode�uhhhK.hh�hhhh,ubeh}�(h]�(�lid-switch-handling�h�eh]�h]�(�lid switch handling��switches_lid�eh]�h]�uhh-hh/hhhh,hK�expect_referenced_by_name�}�j  h�s�expect_referenced_by_id�}�h�h�subh.)��}�(hhh]�(h3)��}�(h�Tablet mode switch handling�h]�h9�Tablet mode switch handling�����}�(hj%  hj#  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj   hhhh,hK-ubhE)��}�(h��Where available, libinput listens to devices providing a tablet mode switch.
This switch is usually triggered on devices that can switch between a normal
laptop layout and a tablet-like layout. One example for such a device is the
Lenovo Yoga.�h]�h9��Where available, libinput listens to devices providing a tablet mode switch.
This switch is usually triggered on devices that can switch between a normal
laptop layout and a tablet-like layout. One example for such a device is the
Lenovo Yoga.�����}�(hj3  hj1  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK/hj   hhubhE)��}�(hXI  The event sent by the kernel is ``EV_SW`` ``SW_TABLET_MODE`` and is provided as
**LIBINPUT_SWITCH_TABLET_MODE**. When the device switches to tablet mode,
the touchpad and internal keyboard are disabled. If a trackpoint exists,
it is disabled too. The input devices are automatically re-enabled whenever
tablet mode is disengaged.�h]�(h9� The event sent by the kernel is �����}�(h� The event sent by the kernel is �hj?  hhhNhNubh�)��}�(h�	``EV_SW``�h]�h9�EV_SW�����}�(hhhjH  ubah}�(h]�h]�h]�h]�h]�uhh�hj?  ubh9� �����}�(hh�hj?  hhhNhNubh�)��}�(h�``SW_TABLET_MODE``�h]�h9�SW_TABLET_MODE�����}�(hhhjZ  ubah}�(h]�h]�h]�h]�h]�uhh�hj?  ubh9� and is provided as
�����}�(h� and is provided as
�hj?  hhhNhNubh^)��}�(h�**LIBINPUT_SWITCH_TABLET_MODE**�h]�h9�LIBINPUT_SWITCH_TABLET_MODE�����}�(hhhjm  ubah}�(h]�h]�h]�h]�h]�uhh]hj?  ubh9��. When the device switches to tablet mode,
the touchpad and internal keyboard are disabled. If a trackpoint exists,
it is disabled too. The input devices are automatically re-enabled whenever
tablet mode is disengaged.�����}�(h��. When the device switches to tablet mode,
the touchpad and internal keyboard are disabled. If a trackpoint exists,
it is disabled too. The input devices are automatically re-enabled whenever
tablet mode is disengaged.�hj?  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK4hj   hhubhE)��}�(h��This handling of tablet mode switches is transparent to the user, no
notifications are sent and the device appears as enabled at all times.�h]�h9��This handling of tablet mode switches is transparent to the user, no
notifications are sent and the device appears as enabled at all times.�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK:hj   hhubeh}�(h]�(�tablet-mode-switch-handling�j  eh]�h]�(�tablet mode switch handling��switches_tablet_mode�eh]�h]�uhh-hh/hhhh,hK-j  }�j�  j  sj  }�j  j  subeh}�(h]�(h+�id1�eh]�h]��switches�ah]��switches�ah]�uhh-hhhhhh,hK�
referenced�Kj  }�j�  h sj  }�h+h subeh}�(h]�h]�h]�h]�h]��source�h,uhh�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(h2N�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}�(�git_version�h �substitution_definition���)��}�(h�,.. |git_version| replace:: :commit:`cc35d33`�h]�h �	reference���)��}�(h�git commit cc35d33�h]�h9�git commit cc35d33�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]��internal���refuri��?https://gitlab.freedesktop.org/libinput/libinput/commit/cc35d33�uhj  hj  ubah}�(h]�h]�h]�j  ah]�h]�uhj  h�<rst_prolog>�hKhhub�git_version_full�j  )��}�(h�].. |git_version_full| replace:: :commit:`<function get_git_version_full at 0x7f4c87c14cb0>`

�h]�j  )��}�(h�<git commit <function get_git_version_full at 0x7f4c87c14cb0>�h]�h9�<git commit <function get_git_version_full at 0x7f4c87c14cb0>�����}�(hhhj+  ubah}�(h]�h]�h]�h]�h]��internal���refuri��ihttps://gitlab.freedesktop.org/libinput/libinput/commit/<function get_git_version_full at 0x7f4c87c14cb0>�uhj  hj'  ubah}�(h]�h]�h]�j&  ah]�h]�uhj  hj%  hKhhubu�substitution_names�}�(�git_version�j  �git_version_full�j&  u�refnames�}��refids�}�(h+]�h ah�]�h�aj  ]�j  au�nameids�}�(j�  h+j  h�j  j  j�  j  j�  j�  u�	nametypes�}�(j�  �j  �j  Nj�  �j�  Nuh}�(h+h/j�  h/h�h�j  h�j  j   j�  j   u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]�h �system_message���)��}�(hhh]�hE)��}�(h�+Duplicate implicit target name: "switches".�h]�h9�/Duplicate implicit target name: “switches”.�����}�(hhhjl  ubah}�(h]�h]�h]�h]�h]�uhhDhji  ubah}�(h]�h]�h]�h]�h]�j�  a�level�K�type��INFO��source�h,�line�Kuhjg  hh/hhhh,hKuba�transform_messages�]�(jh  )��}�(hhh]�hE)��}�(hhh]�h9�.Hyperlink target "switches" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h,�line�Kuhjg  ubjh  )��}�(hhh]�hE)��}�(hhh]�h9�2Hyperlink target "switches-lid" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h,�line�Kuhjg  ubjh  )��}�(hhh]�hE)��}�(hhh]�h9�:Hyperlink target "switches-tablet-mode" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h,�line�K.uhjg  ube�transformer�N�
decoration�Nhhub.