��>>      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �block_quote���)��}�(hhh]��
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�h	�parent�hhh�source�N�line�Nubh �target���)��}�(h�.. _touchpad_pressure:�h]�h}�(h]�h]�h]�h]�h]��refid��touchpad-pressure�uhhhKhhhhh�=/home/whot/code/libinput/build/doc/user/touchpad-pressure.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�'Touchpad pressure-based touch detection�h]�h �Text����'Touchpad pressure-based touch detection�����}�(hh6hh4hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hh/hhhh,hKubh �	paragraph���)��}�(hX�  libinput uses the touchpad pressure values and/or touch size values to
detect whether a finger has been placed on the touchpad. This is
:ref:`kernel_pressure_information` and combines with a libinput-specific hardware
database to adjust the thresholds on a per-device basis. libinput uses
these thresholds primarily to filter out accidental light touches but
the information is also used for some :ref:`palm_detection`.�h]�(h9��libinput uses the touchpad pressure values and/or touch size values to
detect whether a finger has been placed on the touchpad. This is
�����}�(h��libinput uses the touchpad pressure values and/or touch size values to
detect whether a finger has been placed on the touchpad. This is
�hhFhhhNhNub�sphinx.addnodes��pending_xref���)��}�(h�":ref:`kernel_pressure_information`�h]�h �inline���)��}�(hhTh]�h9�kernel_pressure_information�����}�(hhhhXubah}�(h]�h]�(�xref��std��std-ref�eh]�h]�h]�uhhVhhRubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�hc�refexplicit���	reftarget��kernel_pressure_information��refdoc��touchpad-pressure��refwarn��uhhPhh,hKhhFubh9�� and combines with a libinput-specific hardware
database to adjust the thresholds on a per-device basis. libinput uses
these thresholds primarily to filter out accidental light touches but
the information is also used for some �����}�(h�� and combines with a libinput-specific hardware
database to adjust the thresholds on a per-device basis. libinput uses
these thresholds primarily to filter out accidental light touches but
the information is also used for some �hhFhhhNhNubhQ)��}�(h�:ref:`palm_detection`�h]�hW)��}�(hh~h]�h9�palm_detection�����}�(hhhh�ubah}�(h]�h]�(hb�std��std-ref�eh]�h]�h]�uhhVhh|ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�h��refexplicit��hr�palm_detection�hthuhv�uhhPhh,hKhhFubh9�.�����}�(h�.�hhFhhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKhh/hhubhE)��}�(hX  Most devices only support one of either touch pressure or touch size.
libinput uses whichever is available but a preference is given to touch size
as it provides more specific information. Since most devices only provide
one type anyway, this internal preference does not usually matter.�h]�h9X  Most devices only support one of either touch pressure or touch size.
libinput uses whichever is available but a preference is given to touch size
as it provides more specific information. Since most devices only provide
one type anyway, this internal preference does not usually matter.�����}�(hh�hh�hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKhh/hhubhE)��}�(hXS  Pressure and touch size thresholds are **not** directly configurable by the
user. Instead, libinput provides these thresholds for each device where
necessary. See :ref:`touchpad_pressure_hwdb` for instructions on how to adjust
the pressure ranges and :ref:`touchpad_touch_size_hwdb` for instructions on
how to adjust the touch size ranges.�h]�(h9�'Pressure and touch size thresholds are �����}�(h�'Pressure and touch size thresholds are �hh�hhhNhNubh �strong���)��}�(h�**not**�h]�h9�not�����}�(hhhh�ubah}�(h]�h]�h]�h]�h]�uhh�hh�ubh9�u directly configurable by the
user. Instead, libinput provides these thresholds for each device where
necessary. See �����}�(h�u directly configurable by the
user. Instead, libinput provides these thresholds for each device where
necessary. See �hh�hhhNhNubhQ)��}�(h�:ref:`touchpad_pressure_hwdb`�h]�hW)��}�(hh�h]�h9�touchpad_pressure_hwdb�����}�(hhhh�ubah}�(h]�h]�(hb�std��std-ref�eh]�h]�h]�uhhVhh�ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�hߌrefexplicit��hr�touchpad_pressure_hwdb�hthuhv�uhhPhh,hKhh�ubh9�; for instructions on how to adjust
the pressure ranges and �����}�(h�; for instructions on how to adjust
the pressure ranges and �hh�hhhNhNubhQ)��}�(h�:ref:`touchpad_touch_size_hwdb`�h]�hW)��}�(hh�h]�h9�touchpad_touch_size_hwdb�����}�(hhhh�ubah}�(h]�h]�(hb�std��std-ref�eh]�h]�h]�uhhVhh�ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�j  �refexplicit��hr�touchpad_touch_size_hwdb�hthuhv�uhhPhh,hKhh�ubh9�9 for instructions on
how to adjust the touch size ranges.�����}�(h�9 for instructions on
how to adjust the touch size ranges.�hh�hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKhh/hhubh)��}�(h� .. _kernel_pressure_information:�h]�h}�(h]�h]�h]�h]�h]�h*�kernel-pressure-information�uhhhKhh/hhhh,ubh.)��}�(hhh]�(h3)��}�(h�"Information provided by the kernel�h]�h9�"Information provided by the kernel�����}�(hj-  hj+  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj(  hhhh,hKubhE)��}�(hX  The kernel sends multiple values to inform userspace about a finger touching
the touchpad. The most basic is the ``EV_KEY/BTN_TOUCH`` boolean event
that simply announces physical contact with the touchpad. The decision when
this event is sent is usually made by the kernel driver and may depend on
device-specific thresholds. These thresholds are transparent to userspace
and cannot be modified. On touchpads where pressure or touch size is not
available, libinput uses ``BTN_TOUCH`` to determine when a finger is
logically down.�h]�(h9�qThe kernel sends multiple values to inform userspace about a finger touching
the touchpad. The most basic is the �����}�(h�qThe kernel sends multiple values to inform userspace about a finger touching
the touchpad. The most basic is the �hj9  hhhNhNubh �literal���)��}�(h�``EV_KEY/BTN_TOUCH``�h]�h9�EV_KEY/BTN_TOUCH�����}�(hhhjD  ubah}�(h]�h]�h]�h]�h]�uhjB  hj9  ubh9XQ   boolean event
that simply announces physical contact with the touchpad. The decision when
this event is sent is usually made by the kernel driver and may depend on
device-specific thresholds. These thresholds are transparent to userspace
and cannot be modified. On touchpads where pressure or touch size is not
available, libinput uses �����}�(hXQ   boolean event
that simply announces physical contact with the touchpad. The decision when
this event is sent is usually made by the kernel driver and may depend on
device-specific thresholds. These thresholds are transparent to userspace
and cannot be modified. On touchpads where pressure or touch size is not
available, libinput uses �hj9  hhhNhNubjC  )��}�(h�``BTN_TOUCH``�h]�h9�	BTN_TOUCH�����}�(hhhjW  ubah}�(h]�h]�h]�h]�h]�uhjB  hj9  ubh9�. to determine when a finger is
logically down.�����}�(h�. to determine when a finger is
logically down.�hj9  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKhj(  hhubhE)��}�(hX�  Many contemporary touchpad devices provide an absolute pressure axis in
addition to ``BTN_TOUCH``. This pressure generally increases as the pressure
increases, however few touchpads are capable of detecting true pressure. The
pressure value is usually related to the covered area - as the pressure
increases a finger flattens and thus covers a larger area. The range
provided by the kernel is not mapped to a specific physical range and
often requires adjustment. Pressure is sent by the ``ABS_PRESSURE`` axis
for single-touch touchpads or ``ABS_MT_PRESSURE`` on multi-touch capable
touchpads. Some devices can detect multiple fingers but only provide
``ABS_PRESSURE``.�h]�(h9�TMany contemporary touchpad devices provide an absolute pressure axis in
addition to �����}�(h�TMany contemporary touchpad devices provide an absolute pressure axis in
addition to �hjp  hhhNhNubjC  )��}�(h�``BTN_TOUCH``�h]�h9�	BTN_TOUCH�����}�(hhhjy  ubah}�(h]�h]�h]�h]�h]�uhjB  hjp  ubh9X�  . This pressure generally increases as the pressure
increases, however few touchpads are capable of detecting true pressure. The
pressure value is usually related to the covered area - as the pressure
increases a finger flattens and thus covers a larger area. The range
provided by the kernel is not mapped to a specific physical range and
often requires adjustment. Pressure is sent by the �����}�(hX�  . This pressure generally increases as the pressure
increases, however few touchpads are capable of detecting true pressure. The
pressure value is usually related to the covered area - as the pressure
increases a finger flattens and thus covers a larger area. The range
provided by the kernel is not mapped to a specific physical range and
often requires adjustment. Pressure is sent by the �hjp  hhhNhNubjC  )��}�(h�``ABS_PRESSURE``�h]�h9�ABS_PRESSURE�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhjB  hjp  ubh9�$ axis
for single-touch touchpads or �����}�(h�$ axis
for single-touch touchpads or �hjp  hhhNhNubjC  )��}�(h�``ABS_MT_PRESSURE``�h]�h9�ABS_MT_PRESSURE�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhjB  hjp  ubh9�] on multi-touch capable
touchpads. Some devices can detect multiple fingers but only provide
�����}�(h�] on multi-touch capable
touchpads. Some devices can detect multiple fingers but only provide
�hjp  hhhNhNubjC  )��}�(h�``ABS_PRESSURE``�h]�h9�ABS_PRESSURE�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhjB  hjp  ubh9�.�����}�(hh�hjp  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK(hj(  hhubhE)��}�(hX  Some devices provide additional touch size information through
the ``ABS_MT_TOUCH_MAJOR/ABS_MT_TOUCH_MINOR`` axes and/or
the ``ABS_MT_WIDTH_MAJOR/ABS_MT_WIDTH_MINOR`` axes. These axes specifcy
the size of the touch ellipse. While the kernel documentation specifies how
these axes are supposed to be mapped, few devices forward reliable
information. libinput uses these values together with a device-specific
:ref:`device-quirks` entry. In other words, touch size detection does not work
unless a device quirk is present for the device.�h]�(h9�CSome devices provide additional touch size information through
the �����}�(h�CSome devices provide additional touch size information through
the �hj�  hhhNhNubjC  )��}�(h�)``ABS_MT_TOUCH_MAJOR/ABS_MT_TOUCH_MINOR``�h]�h9�%ABS_MT_TOUCH_MAJOR/ABS_MT_TOUCH_MINOR�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhjB  hj�  ubh9� axes and/or
the �����}�(h� axes and/or
the �hj�  hhhNhNubjC  )��}�(h�)``ABS_MT_WIDTH_MAJOR/ABS_MT_WIDTH_MINOR``�h]�h9�%ABS_MT_WIDTH_MAJOR/ABS_MT_WIDTH_MINOR�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhjB  hj�  ubh9�� axes. These axes specifcy
the size of the touch ellipse. While the kernel documentation specifies how
these axes are supposed to be mapped, few devices forward reliable
information. libinput uses these values together with a device-specific
�����}�(h�� axes. These axes specifcy
the size of the touch ellipse. While the kernel documentation specifies how
these axes are supposed to be mapped, few devices forward reliable
information. libinput uses these values together with a device-specific
�hj�  hhhNhNubhQ)��}�(h�:ref:`device-quirks`�h]�hW)��}�(hj�  h]�h9�device-quirks�����}�(hhhj�  ubah}�(h]�h]�(hb�std��std-ref�eh]�h]�h]�uhhVhj�  ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�j  �refexplicit��hr�device-quirks�hthuhv�uhhPhh,hK3hj�  ubh9�k entry. In other words, touch size detection does not work
unless a device quirk is present for the device.�����}�(h�k entry. In other words, touch size detection does not work
unless a device quirk is present for the device.�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK3hj(  hhubeh}�(h]�(�"information-provided-by-the-kernel�j'  eh]�h]�(�"information provided by the kernel��kernel_pressure_information�eh]�h]�uhh-hh/hhhh,hK�expect_referenced_by_name�}�j(  j  s�expect_referenced_by_id�}�j'  j  subeh}�(h]�(�'touchpad-pressure-based-touch-detection�h+eh]�h]�(�'touchpad pressure-based touch detection��touchpad_pressure�eh]�h]�uhh-hhhhhh,hKj+  }�j5  h sj-  }�h+h subeh}�(h]�h]�h]�h]�h]��source�h,uhh�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(h2N�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j]  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}�(�git_version�h �substitution_definition���)��}�(h�,.. |git_version| replace:: :commit:`cc35d33`�h]�h �	reference���)��}�(h�git commit cc35d33�h]�h9�git commit cc35d33�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]��internal���refuri��?https://gitlab.freedesktop.org/libinput/libinput/commit/cc35d33�uhj�  hj�  ubah}�(h]�h]�h]�j�  ah]�h]�uhj�  h�<rst_prolog>�hKhhub�git_version_full�j�  )��}�(h�].. |git_version_full| replace:: :commit:`<function get_git_version_full at 0x7f4c87c14cb0>`

�h]�j�  )��}�(h�<git commit <function get_git_version_full at 0x7f4c87c14cb0>�h]�h9�<git commit <function get_git_version_full at 0x7f4c87c14cb0>�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]��internal���refuri��ihttps://gitlab.freedesktop.org/libinput/libinput/commit/<function get_git_version_full at 0x7f4c87c14cb0>�uhj�  hj�  ubah}�(h]�h]�h]�j�  ah]�h]�uhj�  hj�  hKhhubu�substitution_names�}�(�git_version�j�  �git_version_full�j�  u�refnames�}��refids�}�(h+]�h aj'  ]�j  au�nameids�}�(j5  h+j4  j1  j(  j'  j'  j$  u�	nametypes�}�(j5  �j4  Nj(  �j'  Nuh}�(h+h/j1  h/j'  j(  j$  j(  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]��transform_messages�]�(h �system_message���)��}�(hhh]�hE)��}�(hhh]�h9�7Hyperlink target "touchpad-pressure" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type��INFO��source�h,�line�Kuhj�  ubj�  )��}�(hhh]�hE)��}�(hhh]�h9�AHyperlink target "kernel-pressure-information" is not referenced.�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]�uhhDhj  ubah}�(h]�h]�h]�h]�h]��level�K�type�j  �source�h,�line�Kuhj�  ube�transformer�N�
decoration�Nhhub.