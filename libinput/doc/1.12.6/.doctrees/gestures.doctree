�� c      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �block_quote���)��}�(hhh]�(h �substitution_definition���)��}�(h�,.. |git_version| replace:: :commit:`9da9118`�h]�h �	reference���)��}�(h�git commit 9da9118�h]�h �Text����git commit 9da9118�����}�(hh�parent�huba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]��internal���refuri��?https://gitlab.freedesktop.org/libinput/libinput/commit/9da9118�u�tagname�hh hubah!}�(h#]�h%]�h']��git_version�ah)]�h+]�uh0h�source��<rst_prolog>��line�Kh hubh)��}�(h�].. |git_version_full| replace:: :commit:`<function get_git_version_full at 0x7f81e92b1598>`

�h]�h)��}�(h�<git commit <function get_git_version_full at 0x7f81e92b1598>�h]�h�<git commit <function get_git_version_full at 0x7f81e92b1598>�����}�(hhh h?ubah!}�(h#]�h%]�h']�h)]�h+]��internal���refuri��ihttps://gitlab.freedesktop.org/libinput/libinput/commit/<function get_git_version_full at 0x7f81e92b1598>�uh0hh h;ubah!}�(h#]�h%]�h']��git_version_full�ah)]�h+]�uh0hh8h9h:Kh hubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h	h hhhh8Nh:Nubh �target���)��}�(h�.. _gestures:�h]�h!}�(h#]�h%]�h']�h)]�h+]��refid��gestures�uh0h]h:Kh hhhh8�4/home/whot/code/libinput/build/doc/user/gestures.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�Gestures�h]�h�Gestures�����}�(hhuh hshhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh hnhhh8hkh:Kubh �	paragraph���)��}�(h��libinput supports :ref:`gestures_pinch` and :ref:`gestures_swipe` on most
modern touchpads and other indirect touch devices. Note that libinput **does
not** support gestures on touchscreens, see :ref:`gestures_touchscreens`.�h]�(h�libinput supports �����}�(h�libinput supports �h h�hhh8Nh:Nub�sphinx.addnodes��pending_xref���)��}�(h�:ref:`gestures_pinch`�h]�h �inline���)��}�(hh�h]�h�gestures_pinch�����}�(hhh h�ubah!}�(h#]�h%]�(�xref��std��std-ref�eh']�h)]�h+]�uh0h�h h�ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�h��refexplicit���	reftarget��gestures_pinch��refdoc��gestures��refwarn��uh0h�h8hkh:Kh h�ubh� and �����}�(h� and �h h�hhh8Nh:Nubh�)��}�(h�:ref:`gestures_swipe`�h]�h�)��}�(hh�h]�h�gestures_swipe�����}�(hhh h�ubah!}�(h#]�h%]�(h��std��std-ref�eh']�h)]�h+]�uh0h�h h�ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�hǌrefexplicit��h��gestures_swipe�h�h�h��uh0h�h8hkh:Kh h�ubh�O on most
modern touchpads and other indirect touch devices. Note that libinput �����}�(h�O on most
modern touchpads and other indirect touch devices. Note that libinput �h h�hhh8Nh:Nubh �strong���)��}�(h�**does
not**�h]�h�does
not�����}�(hhh h�ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h h�ubh�' support gestures on touchscreens, see �����}�(h�' support gestures on touchscreens, see �h h�hhh8Nh:Nubh�)��}�(h�:ref:`gestures_touchscreens`�h]�h�)��}�(hh�h]�h�gestures_touchscreens�����}�(hhh h�ubah!}�(h#]�h%]�(h��std��std-ref�eh']�h)]�h+]�uh0h�h h�ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�h��refexplicit��h��gestures_touchscreens�h�h�h��uh0h�h8hkh:Kh h�ubh�.�����}�(h�.�h h�hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh hnhhubh^)��}�(h�.. _gestures_lifetime:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�gestures-lifetime�uh0h]h:Kh hnhhh8hkubhm)��}�(hhh]�(hr)��}�(h�Lifetime of a gesture�h]�h�Lifetime of a gesture�����}�(hj*  h j(  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j%  hhh8hkh:Kubh�)��}�(h��A gesture starts when the finger position and/or finger motion is
unambiguous as to what gesture to trigger and continues until the first
finger belonging to this gesture is lifted.�h]�h��A gesture starts when the finger position and/or finger motion is
unambiguous as to what gesture to trigger and continues until the first
finger belonging to this gesture is lifted.�����}�(hj8  h j6  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh j%  hhubh�)��}�(hXh  A single gesture cannot change the finger count. For example, if a user
puts down a fourth finger during a three-finger swipe gesture, libinput will
end the three-finger gesture and, if applicable, start a four-finger swipe
gesture. A caller may however decide that those gestures are semantically
identical and continue the two gestures as one single gesture.�h]�hXh  A single gesture cannot change the finger count. For example, if a user
puts down a fourth finger during a three-finger swipe gesture, libinput will
end the three-finger gesture and, if applicable, start a four-finger swipe
gesture. A caller may however decide that those gestures are semantically
identical and continue the two gestures as one single gesture.�����}�(hjF  h jD  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh j%  hhubh^)��}�(h�.. _gestures_pinch:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�gestures-pinch�uh0h]h:K h j%  hhh8hkubeh!}�(h#]�(�lifetime-of-a-gesture�j$  eh%]�h']�(�lifetime of a gesture��gestures_lifetime�eh)]�h+]�uh0hlh hnhhh8hkh:K�expect_referenced_by_name�}�jc  j  s�expect_referenced_by_id�}�j$  j  subhm)��}�(hhh]�(hr)��}�(h�Pinch gestures�h]�h�Pinch gestures�����}�(hjo  h jm  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh jj  hhh8hkh:Kubh�)��}�(hX  Pinch gestures are executed when two or more fingers are located on the
touchpad and are either changing the relative distance to each other
(pinching) or are changing the relative angle (rotate). Pinch gestures may
change both rotation and distance at the same time. For such gestures,
libinput calculates a logical center for the gestures and provides the
caller with the delta x/y coordinates of that center, the relative angle of
the fingers compared to the previous event, and the absolute scale compared
to the initial finger position.�h]�hX  Pinch gestures are executed when two or more fingers are located on the
touchpad and are either changing the relative distance to each other
(pinching) or are changing the relative angle (rotate). Pinch gestures may
change both rotation and distance at the same time. For such gestures,
libinput calculates a logical center for the gestures and provides the
caller with the delta x/y coordinates of that center, the relative angle of
the fingers compared to the previous event, and the absolute scale compared
to the initial finger position.�����}�(hj}  h j{  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K!h jj  hhubh �figure���)��}�(hhh]�(h �image���)��}�(h�U.. figure:: pinch-gestures.svg
    :align: center

    The pinch and rotate gestures
�h]�h!}�(h#]�h%]�h']�h)]�h+]��uri��pinch-gestures.svg��
candidates�}��*�j�  suh0j�  h j�  h8hkh:K-ubh �caption���)��}�(h�The pinch and rotate gestures�h]�h�The pinch and rotate gestures�����}�(hj�  h j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j�  h8hkh:K-h j�  ubeh!}�(h#]��id2�ah%]�h']�h)]�h+]��align��center�uh0j�  h:K-h jj  hhh8hkubh�)��}�(h��The illustration above shows a basic pinch in the left image and a rotate in
the right angle. Not shown is a movement of the logical center if the
fingers move unevenly. Such a movement is supported by libinput, it is
merely left out of the illustration.�h]�h��The illustration above shows a basic pinch in the left image and a rotate in
the right angle. Not shown is a movement of the logical center if the
fingers move unevenly. Such a movement is supported by libinput, it is
merely left out of the illustration.�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K/h jj  hhubh�)��}�(h��Note that while position and angle is relative to the previous event, the
scale is always absolute and a multiplier of the initial finger position's
scale.�h]�h��Note that while position and angle is relative to the previous event, the
scale is always absolute and a multiplier of the initial finger position’s
scale.�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K4h jj  hhubh^)��}�(h�.. _gestures_swipe:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�gestures-swipe�uh0h]h:K=h jj  hhh8hkubeh!}�(h#]�(�pinch-gestures�j\  eh%]�h']�(�pinch gestures��gestures_pinch�eh)]�h+]�uh0hlh hnhhh8hkh:Kjf  }�j�  jR  sjh  }�j\  jR  subhm)��}�(hhh]�(hr)��}�(h�Swipe gestures�h]�h�Swipe gestures�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j�  hhh8hkh:K<ubh�)��}�(hXb  Swipe gestures are executed when three or more fingers are moved
synchronously in the same direction. libinput provides x and y coordinates
in the gesture and thus allows swipe gestures in any direction, including
the tracing of complex paths. It is up to the caller to interpret the
gesture into an action or limit a gesture to specific directions only.�h]�hXb  Swipe gestures are executed when three or more fingers are moved
synchronously in the same direction. libinput provides x and y coordinates
in the gesture and thus allows swipe gestures in any direction, including
the tracing of complex paths. It is up to the caller to interpret the
gesture into an action or limit a gesture to specific directions only.�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K>h j�  hhubj�  )��}�(hhh]�(j�  )��}�(h�J.. figure:: swipe-gestures.svg
    :align: center

    The swipe gestures
�h]�h!}�(h#]�h%]�h']�h)]�h+]��uri��swipe-gestures.svg�j�  }�j�  j  suh0j�  h j	  h8hkh:KGubj�  )��}�(h�The swipe gestures�h]�h�The swipe gestures�����}�(hj  h j  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j�  h8hkh:KGh j	  ubeh!}�(h#]��id3�ah%]�h']�h)]�h+]�j�  �center�uh0j�  h:KGh j�  hhh8hkubh�)��}�(h��The illustration above shows a vertical three-finger swipe. The coordinates
provided during the gesture are the movements of the logical center.�h]�h��The illustration above shows a vertical three-finger swipe. The coordinates
provided during the gesture are the movements of the logical center.�����}�(hj1  h j/  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:KIh j�  hhubh^)��}�(h�.. _gestures_touchscreens:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�gestures-touchscreens�uh0h]h:KQh j�  hhh8hkubeh!}�(h#]�(�swipe-gestures�j�  eh%]�h']�(�swipe gestures��gestures_swipe�eh)]�h+]�uh0hlh hnhhh8hkh:K<jf  }�jN  j�  sjh  }�j�  j�  subhm)��}�(hhh]�(hr)��}�(h�Touchscreen gestures�h]�h�Touchscreen gestures�����}�(hjX  h jV  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh jS  hhh8hkh:KPubh�)��}�(h��Touchscreen gestures are **not** interpreted by libinput. Rather, any touch
point is passed to the caller and any interpretation of gestures is up to
the caller or, eventually, the X or Wayland client.�h]�(h�Touchscreen gestures are �����}�(h�Touchscreen gestures are �h jd  hhh8Nh:Nubh�)��}�(h�**not**�h]�h�not�����}�(hhh jm  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h jd  ubh�� interpreted by libinput. Rather, any touch
point is passed to the caller and any interpretation of gestures is up to
the caller or, eventually, the X or Wayland client.�����}�(h�� interpreted by libinput. Rather, any touch
point is passed to the caller and any interpretation of gestures is up to
the caller or, eventually, the X or Wayland client.�h jd  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:KRh jS  hhubh�)��}�(h��Interpreting gestures on a touchscreen requires context that libinput does
not have, such as the location of windows and other virtual objects on the
screen as well as the context of those virtual objects:�h]�h��Interpreting gestures on a touchscreen requires context that libinput does
not have, such as the location of windows and other virtual objects on the
screen as well as the context of those virtual objects:�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:KVh jS  hhubj�  )��}�(hhh]�(j�  )��}�(h�i.. figure:: touchscreen-gestures.svg
    :align: center

    Context-sensitivity of touchscreen gestures
�h]�h!}�(h#]�h%]�h']�h)]�h+]��uri��touchscreen-gestures.svg�j�  }�j�  j�  suh0j�  h j�  h8hkh:K]ubj�  )��}�(h�+Context-sensitivity of touchscreen gestures�h]�h�+Context-sensitivity of touchscreen gestures�����}�(hj�  h j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j�  h8hkh:K]h j�  ubeh!}�(h#]��id4�ah%]�h']�h)]�h+]�j�  �center�uh0j�  h:K]h jS  hhh8hkubh�)��}�(hX`  In the above example, the finger movements are identical but in the left
case both fingers are located within the same window, thus suggesting an
attempt to zoom. In the right case  both fingers are located on a window
border, thus suggesting a window movement. libinput has no knowledge of the
window coordinates and thus cannot differentiate the two.�h]�hX`  In the above example, the finger movements are identical but in the left
case both fingers are located within the same window, thus suggesting an
attempt to zoom. In the right case  both fingers are located on a window
border, thus suggesting a window movement. libinput has no knowledge of the
window coordinates and thus cannot differentiate the two.�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K_h jS  hhubh^)��}�(h�.. _gestures_softbuttons:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�gestures-softbuttons�uh0h]h:Kjh jS  hhh8hkubeh!}�(h#]�(�touchscreen-gestures�jG  eh%]�h']�(�touchscreen gestures��gestures_touchscreens�eh)]�h+]�uh0hlh hnhhh8hkh:KPjf  }�j�  j=  sjh  }�jG  j=  subhm)��}�(hhh]�(hr)��}�(h�&Gestures with enabled software buttons�h]�h�&Gestures with enabled software buttons�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j�  hhh8hkh:Kiubh�)��}�(hXR  If the touchpad device is a :ref:`Clickpad <touchpads_buttons_clickpads>`, it
is recommended that a caller switches to :ref:`clickfinger`.
Usually fingers placed in a :ref:`software button area <software_buttons>`
are not considered for gestures, resulting in some gestures to be
interpreted as pointer motion or two-finger scroll events.�h]�(h�If the touchpad device is a �����}�(h�If the touchpad device is a �h j�  hhh8Nh:Nubh�)��}�(h�-:ref:`Clickpad <touchpads_buttons_clickpads>`�h]�h�)��}�(hj�  h]�h�Clickpad�����}�(hhh j�  ubah!}�(h#]�h%]�(h��std��std-ref�eh']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�j  �refexplicit��h��touchpads_buttons_clickpads�h�h�h��uh0h�h8hkh:Kkh j�  ubh�., it
is recommended that a caller switches to �����}�(h�., it
is recommended that a caller switches to �h j�  hhh8Nh:Nubh�)��}�(h�:ref:`clickfinger`�h]�h�)��}�(hj  h]�h�clickfinger�����}�(hhh j  ubah!}�(h#]�h%]�(h��std��std-ref�eh']�h)]�h+]�uh0h�h j  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�j)  �refexplicit��h��clickfinger�h�h�h��uh0h�h8hkh:Kkh j�  ubh�.
Usually fingers placed in a �����}�(h�.
Usually fingers placed in a �h j�  hhh8Nh:Nubh�)��}�(h�.:ref:`software button area <software_buttons>`�h]�h�)��}�(hj@  h]�h�software button area�����}�(hhh jB  ubah!}�(h#]�h%]�(h��std��std-ref�eh']�h)]�h+]�uh0h�h j>  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�jL  �refexplicit��h��software_buttons�h�h�h��uh0h�h8hkh:Kkh j�  ubh�}
are not considered for gestures, resulting in some gestures to be
interpreted as pointer motion or two-finger scroll events.�����}�(h�}
are not considered for gestures, resulting in some gestures to be
interpreted as pointer motion or two-finger scroll events.�h j�  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kkh j�  hhubj�  )��}�(hhh]�(j�  )��}�(h�w.. figure:: pinch-gestures-softbuttons.svg
    :align: center

    Interference of software buttons and pinch gestures
�h]�h!}�(h#]�h%]�h']�h)]�h+]��uri��pinch-gestures-softbuttons.svg�j�  }�j�  ju  suh0j�  h jg  h8hkh:Ktubj�  )��}�(h�3Interference of software buttons and pinch gestures�h]�h�3Interference of software buttons and pinch gestures�����}�(hjy  h jw  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j�  h8hkh:Kth jg  ubeh!}�(h#]��id5�ah%]�h']�h)]�h+]�j�  �center�uh0j�  h:Kth j�  hhh8hkubh�)��}�(hX?  In the example above, the software button area is highlighted in red. The
user executes a three-finger pinch gesture, with the thumb remaining in the
software button area. libinput ignores fingers within the software button
areas, the movement of the remaining fingers is thus interpreted as a
two-finger scroll motion.�h]�hX?  In the example above, the software button area is highlighted in red. The
user executes a three-finger pinch gesture, with the thumb remaining in the
software button area. libinput ignores fingers within the software button
areas, the movement of the remaining fingers is thus interpreted as a
two-finger scroll motion.�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kvh j�  hhubh^)��}�(h�!.. _gestures_twofinger_touchpads:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�gestures-twofinger-touchpads�uh0h]h:K�h j�  hhh8hkubeh!}�(h#]�(�&gestures-with-enabled-software-buttons�j�  eh%]�h']�(�&gestures with enabled software buttons��gestures_softbuttons�eh)]�h+]�uh0hlh hnhhh8hkh:Kijf  }�j�  j�  sjh  }�j�  j�  subhm)��}�(hhh]�(hr)��}�(h� Gestures on two-finger touchpads�h]�h� Gestures on two-finger touchpads�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j�  hhh8hkh:K�ubh�)��}�(hXP  As of kernel 4.2, many :ref:`touchpads_touch_partial_mt` provide only two
slots. This affects how gestures can be interpreted. Touchpads with only two
slots can identify two touches by position but can usually tell that there
is a third (or fourth) finger down on the touchpad - without providing
positional information for that finger.�h]�(h�As of kernel 4.2, many �����}�(h�As of kernel 4.2, many �h j�  hhh8Nh:Nubh�)��}�(h�!:ref:`touchpads_touch_partial_mt`�h]�h�)��}�(hj�  h]�h�touchpads_touch_partial_mt�����}�(hhh j�  ubah!}�(h#]�h%]�(h��std��std-ref�eh']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�j�  �refexplicit��h��touchpads_touch_partial_mt�h�h�h��uh0h�h8hkh:K�h j�  ubhX   provide only two
slots. This affects how gestures can be interpreted. Touchpads with only two
slots can identify two touches by position but can usually tell that there
is a third (or fourth) finger down on the touchpad - without providing
positional information for that finger.�����}�(hX   provide only two
slots. This affects how gestures can be interpreted. Touchpads with only two
slots can identify two touches by position but can usually tell that there
is a third (or fourth) finger down on the touchpad - without providing
positional information for that finger.�h j�  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h j�  hhubh�)��}�(hX~  Touchpoints are assigned in sequential order and only the first two touch
points are trackable. For libinput this produces an ambiguity where it is
impossible to detect whether a gesture is a pinch gesture or a swipe gesture
whenever a user puts the index and middle finger down first. Since the third
finger does not have positional information, it's location cannot be
determined.�h]�hX�  Touchpoints are assigned in sequential order and only the first two touch
points are trackable. For libinput this produces an ambiguity where it is
impossible to detect whether a gesture is a pinch gesture or a swipe gesture
whenever a user puts the index and middle finger down first. Since the third
finger does not have positional information, it’s location cannot be
determined.�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h j�  hhubj�  )��}�(hhh]�(j�  )��}�(h�y.. figure:: gesture-2fg-ambiguity.svg
    :align: center

    Ambiguity of three-finger gestures on two-finger touchpads
�h]�h!}�(h#]�h%]�h']�h)]�h+]��uri��gesture-2fg-ambiguity.svg�j�  }�j�  j  suh0j�  h j  h8hkh:K�ubj�  )��}�(h�:Ambiguity of three-finger gestures on two-finger touchpads�h]�h�:Ambiguity of three-finger gestures on two-finger touchpads�����}�(hj  h j  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j�  h8hkh:K�h j  ubeh!}�(h#]��id6�ah%]�h']�h)]�h+]�j�  �center�uh0j�  h:K�h j�  hhh8hkubh�)��}�(hX  The image above illustrates this ambiguity. The index and middle finger are
set down first, the data stream from both finger positions looks identical.
In this case, libinput assumes the fingers are in a horizontal arrangement
(the right image above) and use a swipe gesture.�h]�hX  The image above illustrates this ambiguity. The index and middle finger are
set down first, the data stream from both finger positions looks identical.
In this case, libinput assumes the fingers are in a horizontal arrangement
(the right image above) and use a swipe gesture.�����}�(hj*  h j(  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h j�  hhubeh!}�(h#]�(� gestures-on-two-finger-touchpads�j�  eh%]�h']�(� gestures on two-finger touchpads��gestures_twofinger_touchpads�eh)]�h+]�uh0hlh hnhhh8hkh:K�jf  }�j<  j�  sjh  }�j�  j�  subeh!}�(h#]�(hj�id1�eh%]�h']��gestures�ah)]��gestures�ah+]�uh0hlh hhhh8hkh:K�
referenced�Kjf  }�jF  h_sjh  }�hjh_subeh!}�(h#]�h%]�h']�h)]�h+]��source�hkuh0h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hqN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�jp  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�hk�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�N�gettext_compact��ub�reporter�N�indirect_targets�]��substitution_defs�}�(h5hhTh;u�substitution_names�}�(�git_version�h5�git_version_full�hTu�refnames�}��refids�}�(hj]�h_aj$  ]�j  aj\  ]�jR  aj�  ]�j�  ajG  ]�j=  aj�  ]�j�  aj�  ]�j�  au�nameids�}�(jF  hjjc  j$  jb  j_  j�  j\  j�  j�  jN  j�  jM  jJ  j�  jG  j�  j�  j�  j�  j�  j�  j<  j�  j;  j8  u�	nametypes�}�(jF  �jc  �jb  Nj�  �j�  NjN  �jM  Nj�  �j�  Nj�  �j�  Nj<  �j;  Nuh#}�(hjhnjC  hnj$  j%  j_  j%  j\  jj  j�  jj  j�  j�  jJ  j�  jG  jS  j�  jS  j�  j�  j�  j�  j�  j�  j8  j�  j�  j�  j)  j	  j�  j�  j�  jg  j"  j  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]�h �system_message���)��}�(hhh]�h�)��}�(h�+Duplicate implicit target name: "gestures".�h]�h�/Duplicate implicit target name: “gestures”.�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]�jC  a�level�K�type��INFO��source�hk�line�Kuh0j�  h hnhhh8hkh:Kuba�transform_messages�]�(j�  )��}�(hhh]�h�)��}�(hhh]�h�.Hyperlink target "gestures" is not referenced.�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j�  �source�hk�line�Kuh0j�  ubj�  )��}�(hhh]�h�)��}�(hhh]�h�7Hyperlink target "gestures-lifetime" is not referenced.�����}�(hhh j  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j�  �source�hk�line�Kuh0j�  ubj�  )��}�(hhh]�h�)��}�(hhh]�h�4Hyperlink target "gestures-pinch" is not referenced.�����}�(hhh j(  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j%  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j�  �source�hk�line�K uh0j�  ubj�  )��}�(hhh]�h�)��}�(hhh]�h�4Hyperlink target "gestures-swipe" is not referenced.�����}�(hhh jB  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j?  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j�  �source�hk�line�K=uh0j�  ubj�  )��}�(hhh]�h�)��}�(hhh]�h�;Hyperlink target "gestures-touchscreens" is not referenced.�����}�(hhh j\  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h jY  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j�  �source�hk�line�KQuh0j�  ubj�  )��}�(hhh]�h�)��}�(hhh]�h�:Hyperlink target "gestures-softbuttons" is not referenced.�����}�(hhh jv  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h js  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j�  �source�hk�line�Kjuh0j�  ubj�  )��}�(hhh]�h�)��}�(hhh]�h�BHyperlink target "gestures-twofinger-touchpads" is not referenced.�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j�  �source�hk�line�K�uh0j�  ube�transformer�N�
decoration�Nhhub.