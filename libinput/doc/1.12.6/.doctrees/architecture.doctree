���v      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �block_quote���)��}�(hhh]�(h �substitution_definition���)��}�(h�,.. |git_version| replace:: :commit:`9da9118`�h]�h �	reference���)��}�(h�git commit 9da9118�h]�h �Text����git commit 9da9118�����}�(hh�parent�huba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]��internal���refuri��?https://gitlab.freedesktop.org/libinput/libinput/commit/9da9118�u�tagname�hh hubah!}�(h#]�h%]�h']��git_version�ah)]�h+]�uh0h�source��<rst_prolog>��line�Kh hubh)��}�(h�].. |git_version_full| replace:: :commit:`<function get_git_version_full at 0x7f81e92b1598>`

�h]�h)��}�(h�<git commit <function get_git_version_full at 0x7f81e92b1598>�h]�h�<git commit <function get_git_version_full at 0x7f81e92b1598>�����}�(hhh h?ubah!}�(h#]�h%]�h']�h)]�h+]��internal���refuri��ihttps://gitlab.freedesktop.org/libinput/libinput/commit/<function get_git_version_full at 0x7f81e92b1598>�uh0hh h;ubah!}�(h#]�h%]�h']��git_version_full�ah)]�h+]�uh0hh8h9h:Kh hubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h	h hhhh8Nh:Nubh �target���)��}�(h�.. _architecture:�h]�h!}�(h#]�h%]�h']�h)]�h+]��refid��architecture�uh0h]h:Kh hhhh8�8/home/whot/code/libinput/build/doc/user/architecture.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h� libinput's internal architecture�h]�h�"libinput’s internal architecture�����}�(hhuh hshhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh hnhhh8hkh:Kubh �	paragraph���)��}�(h��This page provides an outline of libinput's internal architecture. The goal
here is to get the high-level picture across and point out the components
and their interplay to new developers.�h]�h��This page provides an outline of libinput’s internal architecture. The goal
here is to get the high-level picture across and point out the components
and their interplay to new developers.�����}�(hh�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh hnhhubh�)��}�(hX  The public facing API is in ``libinput.c``, this file is thus the entry point
for almost all API calls. General device handling is in ``evdev.c`` with the
device-type-specific implementations in ``evdev-<type>.c``. It is not
necessary to understand all of libinput to contribute a patch.�h]�(h�The public facing API is in �����}�(h�The public facing API is in �h h�hhh8Nh:Nubh �literal���)��}�(h�``libinput.c``�h]�h�
libinput.c�����}�(hhh h�ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h h�ubh�\, this file is thus the entry point
for almost all API calls. General device handling is in �����}�(h�\, this file is thus the entry point
for almost all API calls. General device handling is in �h h�hhh8Nh:Nubh�)��}�(h�``evdev.c``�h]�h�evdev.c�����}�(hhh h�ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h h�ubh�2 with the
device-type-specific implementations in �����}�(h�2 with the
device-type-specific implementations in �h h�hhh8Nh:Nubh�)��}�(h�``evdev-<type>.c``�h]�h�evdev-<type>.c�����}�(hhh h�ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h h�ubh�J. It is not
necessary to understand all of libinput to contribute a patch.�����}�(h�J. It is not
necessary to understand all of libinput to contribute a patch.�h h�hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh hnhhubh�)��}�(h��:ref:`architecture-contexts` is the only user-visible implementation detail,
everything else is purely internal implementation and may change when
required.�h]�(�sphinx.addnodes��pending_xref���)��}�(h�:ref:`architecture-contexts`�h]�h �inline���)��}�(hh�h]�h�architecture-contexts�����}�(hhh h�ubah!}�(h#]�h%]�(�xref��std��std-ref�eh']�h)]�h+]�uh0h�h h�ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�h�refexplicit���	reftarget��architecture-contexts��refdoc��architecture��refwarn��uh0h�h8hkh:Kh h�ubh�� is the only user-visible implementation detail,
everything else is purely internal implementation and may change when
required.�����}�(h�� is the only user-visible implementation detail,
everything else is purely internal implementation and may change when
required.�h h�hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh hnhhubh^)��}�(h�.. _architecture-contexts:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�architecture-contexts�uh0h]h:Kh hnhhh8hkubhm)��}�(hhh]�(hr)��}�(h�The udev and path contexts�h]�h�The udev and path contexts�����}�(hj"  h j   hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j  hhh8hkh:Kubh�)��}�(hX1  The first building block is the "context" which can be one of
two types, "path" and "udev". See **libinput_path_create_context()** and
**libinput_udev_create_context()**. The path/udev specific bits are in
``path-seat.c`` and ``udev-seat.c``. This includes the functions that add new
devices to a context.�h]�(h�lThe first building block is the “context” which can be one of
two types, “path” and “udev”. See �����}�(h�`The first building block is the "context" which can be one of
two types, "path" and "udev". See �h j.  hhh8Nh:Nubh �strong���)��}�(h�"**libinput_path_create_context()**�h]�h�libinput_path_create_context()�����}�(hhh j9  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j7  h j.  ubh� and
�����}�(h� and
�h j.  hhh8Nh:Nubj8  )��}�(h�"**libinput_udev_create_context()**�h]�h�libinput_udev_create_context()�����}�(hhh jL  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j7  h j.  ubh�%. The path/udev specific bits are in
�����}�(h�%. The path/udev specific bits are in
�h j.  hhh8Nh:Nubh�)��}�(h�``path-seat.c``�h]�h�path-seat.c�����}�(hhh j_  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j.  ubh� and �����}�(h� and �h j.  hhh8Nh:Nubh�)��}�(h�``udev-seat.c``�h]�h�udev-seat.c�����}�(hhh jr  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j.  ubh�@. This includes the functions that add new
devices to a context.�����}�(h�@. This includes the functions that add new
devices to a context.�h j.  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh j  hhub�sphinx.ext.graphviz��graphviz���)��}�(hhh]�h!}�(h#]�h%]�h']�h)]�h+]��code�XS  digraph context
{
  compound=true;
  rankdir="LR";
  node [
    shape="box";
  ]

  libudev [label="libudev 'add' event"]
  udev [label="**libinput_udev_create_context()**"];
  udev_backend [label="udev-specific backend"];
  context [label="libinput context"]
  udev -> udev_backend;
  libudev -> udev_backend;
  udev_backend -> context;
}��options�}�uh0j�  h j  hhh8hkh:K5ubh�)��}�(h��The udev context provides automatic device hotplugging as udev's "add"
events are handled directly by libinput. The path context requires that the
caller adds devices.�h]�h��The udev context provides automatic device hotplugging as udev’s “add”
events are handled directly by libinput. The path context requires that the
caller adds devices.�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K6h j  hhubj�  )��}�(hhh]�h!}�(h#]�h%]�h']�h)]�h+]�j�  X^  digraph context
{
  compound=true;
  rankdir="LR";
  node [
    shape="box";
  ]

  path [label="**libinput_path_create_context()**"];
  path_backend [label="path-specific backend"];
  xdriver [label="**libinput_path_add_device()**"]
  context [label="libinput context"]
  path -> path_backend;
  xdriver -> path_backend;
  path_backend -> context;
}�j�  }�uh0j�  h j  hhh8hkh:KOubh�)��}�(h�cAs a general rule: all Wayland compositors use a udev context, the X.org
stack uses a path context.�h]�h�cAs a general rule: all Wayland compositors use a udev context, the X.org
stack uses a path context.�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:KPh j  hhubh�)��}�(h��Which context was initialized only matters for creating/destroying a context
and adding devices. The device handling itself is the same for both types of
context.�h]�h��Which context was initialized only matters for creating/destroying a context
and adding devices. The device handling itself is the same for both types of
context.�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:KSh j  hhubh^)��}�(h�.. _architecture-device:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�architecture-device�uh0h]h:K\h j  hhh8hkubeh!}�(h#]�(�the-udev-and-path-contexts�j  eh%]�h']�(�the udev and path contexts��architecture-contexts�eh)]�h+]�uh0hlh hnhhh8hkh:K�expect_referenced_by_name�}�j�  j  s�expect_referenced_by_id�}�j  j  subhm)��}�(hhh]�(hr)��}�(h�Device initialization�h]�h�Device initialization�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j�  hhh8hkh:K[ubh�)��}�(h��libinput only supports evdev devices, all the device initialization is done
in ``evdev.c``. Much of the libinput public API is also a thin wrapper around
the matching implementation in the evdev device.�h]�(h�Olibinput only supports evdev devices, all the device initialization is done
in �����}�(h�Olibinput only supports evdev devices, all the device initialization is done
in �h j�  hhh8Nh:Nubh�)��}�(h�``evdev.c``�h]�h�evdev.c�����}�(hhh j  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubh�p. Much of the libinput public API is also a thin wrapper around
the matching implementation in the evdev device.�����}�(h�p. Much of the libinput public API is also a thin wrapper around
the matching implementation in the evdev device.�h j�  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K]h j�  hhubh�)��}�(h�WThere is a 1:1 mapping between libinput devices and ``/dev/input/eventX``
device nodes.�h]�(h�4There is a 1:1 mapping between libinput devices and �����}�(h�4There is a 1:1 mapping between libinput devices and �h j  hhh8Nh:Nubh�)��}�(h�``/dev/input/eventX``�h]�h�/dev/input/eventX�����}�(hhh j$  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j  ubh�
device nodes.�����}�(h�
device nodes.�h j  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kah j�  hhubj�  )��}�(hhh]�h!}�(h#]�h%]�h']�h)]�h+]�j�  X�  digraph context
{
  compound=true;
  rankdir="LR";
  node [
    shape="box";
  ]

  devnode [label="/dev/input/event0"]

  libudev [label="libudev 'add' event"]
  xdriver [label="**libinput_path_add_device()**"]
  context [label="libinput context"]

  evdev [label="evdev_device_create()"]

  devnode -> xdriver;
  devnode -> libudev;
  xdriver -> context;
  libudev -> context;

  context->evdev;

}�j�  }�uh0j�  h j�  hhh8hkh:K�ubh�)��}�(hX  Entry point for all devices is ``evdev_device_create()``, this function
decides to create a ``struct evdev_device`` for the given device node.
Based on the udev tags (e.g. ``ID_INPUT_TOUCHPAD``), a
:ref:`architecture-dispatch` is initialized. All event handling is then in this
dispatch.�h]�(h�Entry point for all devices is �����}�(h�Entry point for all devices is �h jH  hhh8Nh:Nubh�)��}�(h�``evdev_device_create()``�h]�h�evdev_device_create()�����}�(hhh jQ  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h jH  ubh�$, this function
decides to create a �����}�(h�$, this function
decides to create a �h jH  hhh8Nh:Nubh�)��}�(h�``struct evdev_device``�h]�h�struct evdev_device�����}�(hhh jd  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h jH  ubh�9 for the given device node.
Based on the udev tags (e.g. �����}�(h�9 for the given device node.
Based on the udev tags (e.g. �h jH  hhh8Nh:Nubh�)��}�(h�``ID_INPUT_TOUCHPAD``�h]�h�ID_INPUT_TOUCHPAD�����}�(hhh jw  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h jH  ubh�), a
�����}�(h�), a
�h jH  hhh8Nh:Nubh�)��}�(h�:ref:`architecture-dispatch`�h]�h�)��}�(hj�  h]�h�architecture-dispatch�����}�(hhh j�  ubah!}�(h#]�h%]�(h�std��std-ref�eh']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�j�  �refexplicit��j  �architecture-dispatch�j  j  j  �uh0h�h8hkh:K�h jH  ubh�= is initialized. All event handling is then in this
dispatch.�����}�(h�= is initialized. All event handling is then in this
dispatch.�h jH  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h j�  hhubh�)��}�(h��Rejection of devices and the application of quirks is generally handled in
``evdev.c`` as well. Common functionality shared across multiple device types
(like button-scrolling) is also handled here.�h]�(h�KRejection of devices and the application of quirks is generally handled in
�����}�(h�KRejection of devices and the application of quirks is generally handled in
�h j�  hhh8Nh:Nubh�)��}�(h�``evdev.c``�h]�h�evdev.c�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubh�p as well. Common functionality shared across multiple device types
(like button-scrolling) is also handled here.�����}�(h�p as well. Common functionality shared across multiple device types
(like button-scrolling) is also handled here.�h j�  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h j�  hhubh^)��}�(h�.. _architecture-dispatch:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�architecture-dispatch�uh0h]h:K�h j�  hhh8hkubeh!}�(h#]�(�device-initialization�j�  eh%]�h']�(�device initialization��architecture-device�eh)]�h+]�uh0hlh hnhhh8hkh:K[j�  }�j�  j�  sj�  }�j�  j�  subhm)��}�(hhh]�(hr)��}�(h�#Device-type specific event dispatch�h]�h�#Device-type specific event dispatch�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j�  hhh8hkh:K�ubh�)��}�(hXO  Depending on the device type, ``evdev_configure_device`` creates the matching
``struct evdev_dispatch``. This dispatch interface contains the function
pointers to handle events. Four such dispatch methods are currently
implemented: touchpad, tablet, tablet pad, and the fallback dispatch which
handles mice, keyboards and touchscreens.�h]�(h�Depending on the device type, �����}�(h�Depending on the device type, �h j�  hhh8Nh:Nubh�)��}�(h�``evdev_configure_device``�h]�h�evdev_configure_device�����}�(hhh j  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubh� creates the matching
�����}�(h� creates the matching
�h j�  hhh8Nh:Nubh�)��}�(h�``struct evdev_dispatch``�h]�h�struct evdev_dispatch�����}�(hhh j  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubh��. This dispatch interface contains the function
pointers to handle events. Four such dispatch methods are currently
implemented: touchpad, tablet, tablet pad, and the fallback dispatch which
handles mice, keyboards and touchscreens.�����}�(h��. This dispatch interface contains the function
pointers to handle events. Four such dispatch methods are currently
implemented: touchpad, tablet, tablet pad, and the fallback dispatch which
handles mice, keyboards and touchscreens.�h j�  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h j�  hhubj�  )��}�(hhh]�h!}�(h#]�h%]�h']�h)]�h+]�j�  X_  digraph context
{
  compound=true;
  rankdir="LR";
  node [
    shape="box";
  ]

  evdev [label="evdev_device_create()"]

  fallback [label="evdev-fallback.c"]
  touchpad [label="evdev-mt-touchpad.c"]
  tablet [label="evdev-tablet.c"]
  pad [label="evdev-tablet-pad.c"]

  evdev -> fallback;
  evdev -> touchpad;
  evdev -> tablet;
  evdev -> pad;

}�j�  }�uh0j�  h j�  hhh8hkh:K�ubh�)��}�(h�}While ``evdev.c`` pulls the event out of libevdev, the actual handling of the
events is performed within the dispatch method.�h]�(h�While �����}�(h�While �h j<  hhh8Nh:Nubh�)��}�(h�``evdev.c``�h]�h�evdev.c�����}�(hhh jE  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j<  ubh�l pulls the event out of libevdev, the actual handling of the
events is performed within the dispatch method.�����}�(h�l pulls the event out of libevdev, the actual handling of the
events is performed within the dispatch method.�h j<  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h j�  hhubj�  )��}�(hhh]�h!}�(h#]�h%]�h']�h)]�h+]�j�  Xm  digraph context
{
  compound=true;
  rankdir="LR";
  node [
    shape="box";
  ]

  evdev [label="evdev_device_dispatch()"]

  fallback [label="fallback_interface_process()"];
  touchpad [label="tp_interface_process()"]
  tablet [label="tablet_process()"]
  pad [label="pad_process()"]

  evdev -> fallback;
  evdev -> touchpad;
  evdev -> tablet;
  evdev -> pad;
}�j�  }�uh0j�  h j�  hhh8hkh:K�ubh�)��}�(hX  The dispatch methods then look at the ``struct input_event`` and proceed to
update the state. Note: the serialized nature of the kernel evdev protocol
requires that the device updates the state with each event but to delay
processing until the ``SYN_REPORT`` event is received.�h]�(h�&The dispatch methods then look at the �����}�(h�&The dispatch methods then look at the �h ji  hhh8Nh:Nubh�)��}�(h�``struct input_event``�h]�h�struct input_event�����}�(hhh jr  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h ji  ubh�� and proceed to
update the state. Note: the serialized nature of the kernel evdev protocol
requires that the device updates the state with each event but to delay
processing until the �����}�(h�� and proceed to
update the state. Note: the serialized nature of the kernel evdev protocol
requires that the device updates the state with each event but to delay
processing until the �h ji  hhh8Nh:Nubh�)��}�(h�``SYN_REPORT``�h]�h�
SYN_REPORT�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h ji  ubh� event is received.�����}�(h� event is received.�h ji  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h j�  hhubh^)��}�(h�.. _architecture-configuration:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�architecture-configuration�uh0h]h:K�h j�  hhh8hkubeh!}�(h#]�(�#device-type-specific-event-dispatch�j�  eh%]�h']�(�#device-type specific event dispatch��architecture-dispatch�eh)]�h+]�uh0hlh hnhhh8hkh:K�j�  }�j�  j�  sj�  }�j�  j�  subhm)��}�(hhh]�(hr)��}�(h�Device configuration�h]�h�Device configuration�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j�  hhh8hkh:K�ubh�)��}�(hX  All device-specific configuration is handled through ``struct
libinput_device_config_FOO`` instances. These are set up during device init
and provide the function pointers for the ``get``, ``set``, ``get_default``
triplet of configuration queries (or more, where applicable).�h]�(h�5All device-specific configuration is handled through �����}�(h�5All device-specific configuration is handled through �h j�  hhh8Nh:Nubh�)��}�(h�%``struct
libinput_device_config_FOO``�h]�h�!struct
libinput_device_config_FOO�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubh�Z instances. These are set up during device init
and provide the function pointers for the �����}�(h�Z instances. These are set up during device init
and provide the function pointers for the �h j�  hhh8Nh:Nubh�)��}�(h�``get``�h]�h�get�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubh�, �����}�(h�, �h j�  hhh8Nh:Nubh�)��}�(h�``set``�h]�h�set�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubh�, �����}�(hj�  h j�  ubh�)��}�(h�``get_default``�h]�h�get_default�����}�(hhh j  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubh�>
triplet of configuration queries (or more, where applicable).�����}�(h�>
triplet of configuration queries (or more, where applicable).�h j�  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h j�  hhubh�)��}�(h��For example, the ``struct tablet_dispatch`` for tablet devices has a
``struct libinput_device_config_accel``. This struct is set up with the
required function pointers to change the profiles.�h]�(h�For example, the �����}�(h�For example, the �h j  hhh8Nh:Nubh�)��}�(h�``struct tablet_dispatch``�h]�h�struct tablet_dispatch�����}�(hhh j(  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j  ubh� for tablet devices has a
�����}�(h� for tablet devices has a
�h j  hhh8Nh:Nubh�)��}�(h�'``struct libinput_device_config_accel``�h]�h�#struct libinput_device_config_accel�����}�(hhh j;  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j  ubh�S. This struct is set up with the
required function pointers to change the profiles.�����}�(h�S. This struct is set up with the
required function pointers to change the profiles.�h j  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h j�  hhubj�  )��}�(hhh]�h!}�(h#]�h%]�h']�h)]�h+]�j�  X  digraph context
{
  compound=true;
  rankdir="LR";
  node [
    shape="box";
  ]

  tablet [label="struct tablet_dispatch"]
  config [label="struct libinput_device_config_accel"];
  tablet_config [label="tablet_accel_config_set_profile()"];
  tablet->config;
  config->tablet_config;
}�j�  }�uh0j�  h j�  hhh8hkh:K�ubh�)��}�(hX  When the matching ``**libinput_device_config_set_FOO()**`` is called, this goes
through to the config struct and invokes the function there. Thus, it is
possible to have different configuration functions for a mouse vs a
touchpad, even though the interface is the same.�h]�(h�When the matching �����}�(h�When the matching �h j_  hhh8Nh:Nubh�)��}�(h�(``**libinput_device_config_set_FOO()**``�h]�h�$**libinput_device_config_set_FOO()**�����}�(hhh jh  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j_  ubh�� is called, this goes
through to the config struct and invokes the function there. Thus, it is
possible to have different configuration functions for a mouse vs a
touchpad, even though the interface is the same.�����}�(h�� is called, this goes
through to the config struct and invokes the function there. Thus, it is
possible to have different configuration functions for a mouse vs a
touchpad, even though the interface is the same.�h j_  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h j�  hhubj�  )��}�(hhh]�h!}�(h#]�h%]�h']�h)]�h+]�j�  ��digraph context
{
  compound=true;
  rankdir="LR";
  node [
    shape="box";
  ]

  libinput [label="**libinput_device_config_accel_set_profile()**"];
  tablet_config [label="tablet_accel_config_set_profile()"];
  libinput->tablet_config;
}�j�  }�uh0j�  h j�  hhh8hkh:Mubh^)��}�(h�.. _architecture-filter:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�architecture-filter�uh0h]h:Mh j�  hhh8hkubeh!}�(h#]�(�device-configuration�j�  eh%]�h']�(�device configuration��architecture-configuration�eh)]�h+]�uh0hlh hnhhh8hkh:K�j�  }�j�  j�  sj�  }�j�  j�  subhm)��}�(hhh]�(hr)��}�(h�Pointer acceleration filters�h]�h�Pointer acceleration filters�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j�  hhh8hkh:Mubh�)��}�(h�VAll pointer acceleration is handled in the ``filter.c`` file and its
associated files.�h]�(h�+All pointer acceleration is handled in the �����}�(h�+All pointer acceleration is handled in the �h j�  hhh8Nh:Nubh�)��}�(h�``filter.c``�h]�h�filter.c�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubh� file and its
associated files.�����}�(h� file and its
associated files.�h j�  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Mh j�  hhubh�)��}�(h��The ``struct motion_filter`` is initialized during device init, whenever
deltas are available they are passed to ``filter_dispatch()``. This function
returns a set of :ref:`normalized coordinates <motion_normalization_customization>`.�h]�(h�The �����}�(h�The �h j�  hhh8Nh:Nubh�)��}�(h�``struct motion_filter``�h]�h�struct motion_filter�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubh�U is initialized during device init, whenever
deltas are available they are passed to �����}�(h�U is initialized during device init, whenever
deltas are available they are passed to �h j�  hhh8Nh:Nubh�)��}�(h�``filter_dispatch()``�h]�h�filter_dispatch()�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubh�!. This function
returns a set of �����}�(h�!. This function
returns a set of �h j�  hhh8Nh:Nubh�)��}�(h�B:ref:`normalized coordinates <motion_normalization_customization>`�h]�h�)��}�(hj  h]�h�normalized coordinates�����}�(hhh j  ubah!}�(h#]�h%]�(h�std��std-ref�eh']�h)]�h+]�uh0h�h j  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�j  �refexplicit��j  �"motion_normalization_customization�j  j  j  �uh0h�h8hkh:Mh j�  ubh�.�����}�(h�.�h j�  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Mh j�  hhubh�)��}�(h��All actual acceleration is handled within the filter, the device itself has
no further knowledge. Thus it is possible to have different acceleration
filters for the same device types (e.g. the Lenovo X230 touchpad has a
custom filter).�h]�h��All actual acceleration is handled within the filter, the device itself has
no further knowledge. Thus it is possible to have different acceleration
filters for the same device types (e.g. the Lenovo X230 touchpad has a
custom filter).�����}�(hj/  h j-  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Mh j�  hhubj�  )��}�(hhh]�h!}�(h#]�h%]�h']�h)]�h+]�j�  X�  digraph context
{
  compound=true;
  rankdir="LR";
  node [
    shape="box";
  ]

  fallback [label="fallback deltas"];
  touchpad [label="touchpad deltas"];
  tablet [label="tablet deltas"];

  filter [label="filter_dispatch"];

  fallback->filter;
  touchpad->filter;
  tablet->filter;

  flat [label="accelerator_interface_flat()"];
  x230 [label="accelerator_filter_x230()"];
  pen [label="tablet_accelerator_filter_flat_pen()"];

  filter->flat;
  filter->x230;
  filter->pen;

}�j�  }�uh0j�  h j�  hhh8hkh:MBubh�)��}�(hX"  Most filters convert the deltas (incl. timestamps) to a motion speed and
then apply a so-called profile function. This function returns a factor that
is then applied to the current delta, converting it into an accelerated
delta. See :ref:`pointer-acceleration` for more details.
the current�h]�(h��Most filters convert the deltas (incl. timestamps) to a motion speed and
then apply a so-called profile function. This function returns a factor that
is then applied to the current delta, converting it into an accelerated
delta. See �����}�(h��Most filters convert the deltas (incl. timestamps) to a motion speed and
then apply a so-called profile function. This function returns a factor that
is then applied to the current delta, converting it into an accelerated
delta. See �h jF  hhh8Nh:Nubh�)��}�(h�:ref:`pointer-acceleration`�h]�h�)��}�(hjQ  h]�h�pointer-acceleration�����}�(hhh jS  ubah!}�(h#]�h%]�(h�std��std-ref�eh']�h)]�h+]�uh0h�h jO  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�j]  �refexplicit��j  �pointer-acceleration�j  j  j  �uh0h�h8hkh:MCh jF  ubh� for more details.
the current�����}�(h� for more details.
the current�h jF  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:MCh j�  hhubeh!}�(h#]�(�pointer-acceleration-filters�j�  eh%]�h']�(�pointer acceleration filters��architecture-filter�eh)]�h+]�uh0hlh hnhhh8hkh:Mj�  }�j~  j�  sj�  }�j�  j�  subeh!}�(h#]�(� libinput-s-internal-architecture�hjeh%]�h']�(� libinput's internal architecture��architecture�eh)]�h+]�uh0hlh hhhh8hkh:Kj�  }�j�  h_sj�  }�hjh_subeh!}�(h#]�h%]�h']�h)]�h+]��source�hkuh0h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hqN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�hk�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�N�gettext_compact��ub�reporter�N�indirect_targets�]��substitution_defs�}�(h5hhTh;u�substitution_names�}�(�git_version�h5�git_version_full�hTu�refnames�}��refids�}�(hj]�h_aj  ]�j  aj�  ]�j�  aj�  ]�j�  aj�  ]�j�  aj�  ]�j�  au�nameids�}�(j�  hjj�  j�  j�  j  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j~  j�  j}  jz  u�	nametypes�}�(j�  �j�  Nj�  �j�  Nj�  �j�  Nj�  �j�  Nj�  �j�  Nj~  �j}  Nuh#}�(hjhnj�  hnj  j  j�  j  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  j�  jz  j�  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]��transform_messages�]�(h �system_message���)��}�(hhh]�h�)��}�(hhh]�h�2Hyperlink target "architecture" is not referenced.�����}�(hhh j  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type��INFO��source�hk�line�Kuh0j  ubj  )��}�(hhh]�h�)��}�(hhh]�h�;Hyperlink target "architecture-contexts" is not referenced.�����}�(hhh j3  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j0  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j-  �source�hk�line�Kuh0j  ubj  )��}�(hhh]�h�)��}�(hhh]�h�9Hyperlink target "architecture-device" is not referenced.�����}�(hhh jM  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h jJ  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j-  �source�hk�line�K\uh0j  ubj  )��}�(hhh]�h�)��}�(hhh]�h�;Hyperlink target "architecture-dispatch" is not referenced.�����}�(hhh jg  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h jd  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j-  �source�hk�line�K�uh0j  ubj  )��}�(hhh]�h�)��}�(hhh]�h�@Hyperlink target "architecture-configuration" is not referenced.�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j~  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j-  �source�hk�line�K�uh0j  ubj  )��}�(hhh]�h�)��}�(hhh]�h�9Hyperlink target "architecture-filter" is not referenced.�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j-  �source�hk�line�Muh0j  ube�transformer�N�
decoration�Nhhub.