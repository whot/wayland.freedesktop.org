���e      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �block_quote���)��}�(hhh]��
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�h	�parent�hhh�source�N�line�Nubh �target���)��}�(h�.. _clickpad_softbuttons:�h]�h}�(h]�h]�h]�h]�h]��refid��clickpad-softbuttons�uhhhKhhhhh�@/home/whot/code/libinput/build/doc/user/clickpad-softbuttons.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�!Clickpad software button behavior�h]�h �Text����!Clickpad software button behavior�����}�(hh6hh4hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hh/hhhh,hKubh �	paragraph���)��}�(hX�  "Clickpads" are touchpads without separate physical buttons. Instead, the
whole touchpad acts as a button and left or right button clicks are
distinguished by :ref:`the location of the fingers <software_buttons>` or
the :ref:`number of fingers on the touchpad <clickfinger>`.
"ClickPad" is a trademark by `Synaptics Inc. <http://www.synaptics.com/en/clickpad.php>`_
but for simplicity we refer to any touchpad with the above feature as Clickpad,
regardless of the manufacturer.�h]�(h9��“Clickpads” are touchpads without separate physical buttons. Instead, the
whole touchpad acts as a button and left or right button clicks are
distinguished by �����}�(h��"Clickpads" are touchpads without separate physical buttons. Instead, the
whole touchpad acts as a button and left or right button clicks are
distinguished by �hhFhhhNhNub�sphinx.addnodes��pending_xref���)��}�(h�5:ref:`the location of the fingers <software_buttons>`�h]�h �inline���)��}�(hhTh]�h9�the location of the fingers�����}�(hhhhXubah}�(h]�h]�(�xref��std��std-ref�eh]�h]�h]�uhhVhhRubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�hc�refexplicit���	reftarget��software_buttons��refdoc��clickpad-softbuttons��refwarn��uhhPhh,hKhhFubh9� or
the �����}�(h� or
the �hhFhhhNhNubhQ)��}�(h�6:ref:`number of fingers on the touchpad <clickfinger>`�h]�hW)��}�(hh~h]�h9�!number of fingers on the touchpad�����}�(hhhh�ubah}�(h]�h]�(hb�std��std-ref�eh]�h]�h]�uhhVhh|ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�h��refexplicit��hr�clickfinger�hthuhv�uhhPhh,hKhhFubh9�#.
“ClickPad” is a trademark by �����}�(h�.
"ClickPad" is a trademark by �hhFhhhNhNubh �	reference���)��}�(h�<`Synaptics Inc. <http://www.synaptics.com/en/clickpad.php>`_�h]�h9�Synaptics Inc.�����}�(hhhh�ubah}�(h]�h]�h]�h]�h]��name��Synaptics Inc.��refuri��(http://www.synaptics.com/en/clickpad.php�uhh�hhFubh)��}�(h�+ <http://www.synaptics.com/en/clickpad.php>�h]�h}�(h]��synaptics-inc�ah]�h]��synaptics inc.�ah]�h]��refuri�h�uhh�
referenced�KhhFubh9�p
but for simplicity we refer to any touchpad with the above feature as Clickpad,
regardless of the manufacturer.�����}�(h�p
but for simplicity we refer to any touchpad with the above feature as Clickpad,
regardless of the manufacturer.�hhFhhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKhh/hhubhE)��}�(hXH  The kernel marks clickpads with the
`INPUT_PROP_BUTTONPAD <https://www.kernel.org/doc/Documentation/input/event-codes.txt>`_
property. Without this property, libinput would not know whether a touchpad
is a clickpad or not. To perform a right-click on a Clickpad, libinput
provides :ref:`software_buttons` and :ref:`clickfinger`.�h]�(h9�$The kernel marks clickpads with the
�����}�(h�$The kernel marks clickpads with the
�hh�hhhNhNubh�)��}�(h�X`INPUT_PROP_BUTTONPAD <https://www.kernel.org/doc/Documentation/input/event-codes.txt>`_�h]�h9�INPUT_PROP_BUTTONPAD�����}�(hhhh�ubah}�(h]�h]�h]�h]�h]��name��INPUT_PROP_BUTTONPAD�h��>https://www.kernel.org/doc/Documentation/input/event-codes.txt�uhh�hh�ubh)��}�(h�A <https://www.kernel.org/doc/Documentation/input/event-codes.txt>�h]�h}�(h]��input-prop-buttonpad�ah]�h]��input_prop_buttonpad�ah]�h]��refuri�h�uhhh�Khh�ubh9��
property. Without this property, libinput would not know whether a touchpad
is a clickpad or not. To perform a right-click on a Clickpad, libinput
provides �����}�(h��
property. Without this property, libinput would not know whether a touchpad
is a clickpad or not. To perform a right-click on a Clickpad, libinput
provides �hh�hhhNhNubhQ)��}�(h�:ref:`software_buttons`�h]�hW)��}�(hh�h]�h9�software_buttons�����}�(hhhh�ubah}�(h]�h]�(hb�std��std-ref�eh]�h]�h]�uhhVhh�ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�j  �refexplicit��hr�software_buttons�hthuhv�uhhPhh,hKhh�ubh9� and �����}�(h� and �hh�hhhNhNubhQ)��}�(h�:ref:`clickfinger`�h]�hW)��}�(hj  h]�h9�clickfinger�����}�(hhhj  ubah}�(h]�h]�(hb�std��std-ref�eh]�h]�h]�uhhVhj  ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�j)  �refexplicit��hr�clickfinger�hthuhv�uhhPhh,hKhh�ubh9�.�����}�(h�.�hh�hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKhh/hhubh �note���)��}�(h��The term "click" refers refer to a physical button press
and/or release of the touchpad, the term "button event" refers to
the events generated by libinput in response to a click.�h]�hE)��}�(h��The term "click" refers refer to a physical button press
and/or release of the touchpad, the term "button event" refers to
the events generated by libinput in response to a click.�h]�h9��The term “click” refers refer to a physical button press
and/or release of the touchpad, the term “button event” refers to
the events generated by libinput in response to a click.�����}�(hjL  hjJ  ubah}�(h]�h]�h]�h]�h]�uhhDhh,hKhjF  ubah}�(h]�h]�h]�h]�h]�uhjD  hh/hhhh,hNubh)��}�(h�.. _software_buttons:�h]�h}�(h]�h]�h]�h]�h]�h*�software-buttons�uhhhKhh/hhhh,ubh.)��}�(hhh]�(h3)��}�(h�Software button areas�h]�h9�Software button areas�����}�(hjn  hjl  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hji  hhhh,hKubhE)��}�(h��The bottom of the touchpad is split into three distinct areas generate left,
middle or right button events on click. The height of the button area
depends on the hardware but is usually around 10mm.�h]�h9��The bottom of the touchpad is split into three distinct areas generate left,
middle or right button events on click. The height of the button area
depends on the hardware but is usually around 10mm.�����}�(hj|  hjz  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKhji  hhubh �figure���)��}�(hhh]�(h �image���)��}�(h�s.. figure :: software-buttons-visualized.svg
     :align: center

     The locations of the virtual button areas.

�h]�h}�(h]�h]�h]�h]�h]��uri��software-buttons-visualized.svg��
candidates�}��*�j�  suhj�  hj�  hh,hK&ubh �caption���)��}�(h�*The locations of the virtual button areas.�h]�h9�*The locations of the virtual button areas.�����}�(hj�  hj�  ubah}�(h]�h]�h]�h]�h]�uhj�  hh,hK&hj�  ubeh}�(h]��id2�ah]�h]�h]�h]��align��center�uhj�  hK&hji  hhhh,ubhE)��}�(h�ALeft, right and middle button events can be triggered as follows:�h]�h9�ALeft, right and middle button events can be triggered as follows:�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK)hji  hhubh �bullet_list���)��}�(hhh]�(h �	list_item���)��}�(h�^if a finger is in the main area or the left button area, a click generates
left button events.�h]�hE)��}�(h�^if a finger is in the main area or the left button area, a click generates
left button events.�h]�h9�^if a finger is in the main area or the left button area, a click generates
left button events.�����}�(hj�  hj�  ubah}�(h]�h]�h]�h]�h]�uhhDhh,hK+hj�  ubah}�(h]�h]�h]�h]�h]�uhj�  hj�  hhhh,hNubj�  )��}�(h�Hif a finger is in the right area, a click generates right button events.�h]�hE)��}�(hj�  h]�h9�Hif a finger is in the right area, a click generates right button events.�����}�(hj�  hj�  ubah}�(h]�h]�h]�h]�h]�uhhDhh,hK-hj�  ubah}�(h]�h]�h]�h]�h]�uhj�  hj�  hhhh,hNubj�  )��}�(h�Kif a finger is in the middle area, a click generates middle button events.
�h]�hE)��}�(h�Jif a finger is in the middle area, a click generates middle button events.�h]�h9�Jif a finger is in the middle area, a click generates middle button events.�����}�(hj  hj�  ubah}�(h]�h]�h]�h]�h]�uhhDhh,hK.hj�  ubah}�(h]�h]�h]�h]�h]�uhj�  hj�  hhhh,hNubeh}�(h]�h]�h]�h]�h]��bullet��-�uhj�  hh,hK+hji  hhubj�  )��}�(hhh]�(j�  )��}�(h�x.. figure:: software-buttons.svg
    :align: center

    Left, right and middle-button click with software button areas
�h]�h}�(h]�h]�h]�h]�h]��uri��software-buttons.svg�j�  }�j�  j)  suhj�  hj  hh,hK3ubj�  )��}�(h�>Left, right and middle-button click with software button areas�h]�h9�>Left, right and middle-button click with software button areas�����}�(hj-  hj+  ubah}�(h]�h]�h]�h]�h]�uhj�  hh,hK3hj  ubeh}�(h]��id3�ah]�h]�h]�h]�j�  �center�uhj�  hK3hji  hhhh,ubhE)��}�(hX  The middle button is always centered on the touchpad and smaller in size
than the left or right button. The actual size is device-dependent. Many
touchpads do not have visible markings so the exact location of the button
is unfortunately not visibly obvious.�h]�h9X  The middle button is always centered on the touchpad and smaller in size
than the left or right button. The actual size is device-dependent. Many
touchpads do not have visible markings so the exact location of the button
is unfortunately not visibly obvious.�����}�(hjC  hjA  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK5hji  hhubjE  )��}�(h��If :ref:`middle button emulation <middle_button_emulation>` is
enabled on a clickpad, only left and right button areas are
available.�h]�hE)��}�(h��If :ref:`middle button emulation <middle_button_emulation>` is
enabled on a clickpad, only left and right button areas are
available.�h]�(h9�If �����}�(h�If �hjS  ubhQ)��}�(h�8:ref:`middle button emulation <middle_button_emulation>`�h]�hW)��}�(hj^  h]�h9�middle button emulation�����}�(hhhj`  ubah}�(h]�h]�(hb�std��std-ref�eh]�h]�h]�uhhVhj\  ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�jj  �refexplicit��hr�middle_button_emulation�hthuhv�uhhPhh,hK:hjS  ubh9�J is
enabled on a clickpad, only left and right button areas are
available.�����}�(h�J is
enabled on a clickpad, only left and right button areas are
available.�hjS  ubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK:hjO  ubah}�(h]�h]�h]�h]�h]�uhjD  hji  hhhh,hNubhE)��}�(h��If fingers are down in the main area in addition to fingers in the
left or right button area, those fingers are are ignored.
A release event always releases the buttons logically down, regardless of
the current finger position�h]�h9��If fingers are down in the main area in addition to fingers in the
left or right button area, those fingers are are ignored.
A release event always releases the buttons logically down, regardless of
the current finger position�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK>hji  hhubj�  )��}�(hhh]�(j�  )��}�(h��.. figure:: software-buttons-thumbpress.svg
    :align: center

    Only the location of the thumb determines whether it is a left, right or
    middle click.
�h]�h}�(h]�h]�h]�h]�h]��uri��software-buttons-thumbpress.svg�j�  }�j�  j�  suhj�  hj�  hh,hKFubj�  )��}�(h�VOnly the location of the thumb determines whether it is a left, right or
middle click.�h]�h9�VOnly the location of the thumb determines whether it is a left, right or
middle click.�����}�(hj�  hj�  ubah}�(h]�h]�h]�h]�h]�uhj�  hh,hKFhj�  ubeh}�(h]��id4�ah]�h]�h]�h]�j�  �center�uhj�  hKFhji  hhhh,ubhE)��}�(h�<The movement of a finger can alter the button area behavior:�h]�h9�<The movement of a finger can alter the button area behavior:�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKIhji  hhubj�  )��}�(hhh]�(j�  )��}�(h�}if a finger starts in the main area and moves into the software button
area, the software buttons do not apply to that finger�h]�hE)��}�(h�}if a finger starts in the main area and moves into the software button
area, the software buttons do not apply to that finger�h]�h9�}if a finger starts in the main area and moves into the software button
area, the software buttons do not apply to that finger�����}�(hj�  hj�  ubah}�(h]�h]�h]�h]�h]�uhhDhh,hKKhj�  ubah}�(h]�h]�h]�h]�h]�uhj�  hj�  hhhh,hNubj�  )��}�(h�qonce a finger has moved out of the button area, it cannot move back in and
trigger a right or middle button event�h]�hE)��}�(h�qonce a finger has moved out of the button area, it cannot move back in and
trigger a right or middle button event�h]�h9�qonce a finger has moved out of the button area, it cannot move back in and
trigger a right or middle button event�����}�(hj�  hj�  ubah}�(h]�h]�h]�h]�h]�uhhDhh,hKMhj�  ubah}�(h]�h]�h]�h]�h]�uhj�  hj�  hhhh,hNubj�  )��}�(h�Ia finger moving within the software button area does not move the pointer�h]�hE)��}�(hj  h]�h9�Ia finger moving within the software button area does not move the pointer�����}�(hj  hj  ubah}�(h]�h]�h]�h]�h]�uhhDhh,hKOhj   ubah}�(h]�h]�h]�h]�h]�uhj�  hj�  hhhh,hNubj�  )��}�(h��once a finger moves out out of the button area it will control the
pointer (this only applies if there is no other finger down on the
touchpad)
�h]�hE)��}�(h��once a finger moves out out of the button area it will control the
pointer (this only applies if there is no other finger down on the
touchpad)�h]�h9��once a finger moves out out of the button area it will control the
pointer (this only applies if there is no other finger down on the
touchpad)�����}�(hj  hj  ubah}�(h]�h]�h]�h]�h]�uhhDhh,hKPhj  ubah}�(h]�h]�h]�h]�h]�uhj�  hj�  hhhh,hNubeh}�(h]�h]�h]�h]�h]�j  j  uhj�  hh,hKKhji  hhubj�  )��}�(hhh]�(j�  )��}�(h��.. figure:: software-buttons-conditions.svg
    :align: center

    **Left:** moving a finger into the right button area does not trigger a
    right-button click.
    **Right:** moving within the button areas does not generate pointer
    motion.
�h]�h}�(h]�h]�h]�h]�h]��uri��software-buttons-conditions.svg�j�  }�j�  jC  suhj�  hj5  hh,hKWubj�  )��}�(h��**Left:** moving a finger into the right button area does not trigger a
right-button click.
**Right:** moving within the button areas does not generate pointer
motion.�h]�(h �strong���)��}�(h�	**Left:**�h]�h9�Left:�����}�(hhhjK  ubah}�(h]�h]�h]�h]�h]�uhjI  hjE  ubh9�S moving a finger into the right button area does not trigger a
right-button click.
�����}�(h�S moving a finger into the right button area does not trigger a
right-button click.
�hjE  ubjJ  )��}�(h�
**Right:**�h]�h9�Right:�����}�(hhhj^  ubah}�(h]�h]�h]�h]�h]�uhjI  hjE  ubh9�A moving within the button areas does not generate pointer
motion.�����}�(h�A moving within the button areas does not generate pointer
motion.�hjE  ubeh}�(h]�h]�h]�h]�h]�uhj�  hh,hKWhj5  ubeh}�(h]��id5�ah]�h]�h]�h]�j�  �center�uhj�  hKWhji  hhhh,ubhE)��}�(hX�  On some touchpads, notably the 2015 Lenovo X1 Carbon 3rd series, the very
bottom end of the touchpad is outside of the sensor range but it is possible
to trigger a physical click there. To libinput, the click merely shows up as
a left button click without any positional finger data and it is
impossible to determine whether it is a left or a right click. libinput
ignores such button clicks, this behavior is intentional.�h]�h9X�  On some touchpads, notably the 2015 Lenovo X1 Carbon 3rd series, the very
bottom end of the touchpad is outside of the sensor range but it is possible
to trigger a physical click there. To libinput, the click merely shows up as
a left button click without any positional finger data and it is
impossible to determine whether it is a left or a right click. libinput
ignores such button clicks, this behavior is intentional.�����}�(hj�  hj  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK\hji  hhubh)��}�(h�.. _clickfinger:�h]�h}�(h]�h]�h]�h]�h]�h*�clickfinger�uhhhKhhji  hhhh,ubeh}�(h]�(�software-button-areas�jh  eh]�h]�(�software button areas��software_buttons�eh]�h]�uhh-hh/hhhh,hK�expect_referenced_by_name�}�j�  j^  s�expect_referenced_by_id�}�jh  j^  subh.)��}�(hhh]�(h3)��}�(h�Clickfinger behavior�h]�h9�Clickfinger behavior�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj�  hhhh,hKgubhE)��}�(hX$  This is the default behavior on Apple touchpads.
Here, a left, right, middle button event is generated when one, two, or
three fingers are held down on the touchpad when a physical click is
generated. The location of the fingers does not matter and there are no
software-defined button areas.�h]�h9X$  This is the default behavior on Apple touchpads.
Here, a left, right, middle button event is generated when one, two, or
three fingers are held down on the touchpad when a physical click is
generated. The location of the fingers does not matter and there are no
software-defined button areas.�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKihj�  hhubj�  )��}�(hhh]�(j�  )��}�(h�n.. figure:: clickfinger.svg
    :align: center

    One, two and three-finger click with Clickfinger behavior
�h]�h}�(h]�h]�h]�h]�h]��uri��clickfinger.svg�j�  }�j�  j�  suhj�  hj�  hh,hKrubj�  )��}�(h�9One, two and three-finger click with Clickfinger behavior�h]�h9�9One, two and three-finger click with Clickfinger behavior�����}�(hj�  hj�  ubah}�(h]�h]�h]�h]�h]�uhj�  hh,hKrhj�  ubeh}�(h]��id6�ah]�h]�h]�h]�j�  �center�uhj�  hKrhj�  hhhh,ubhE)��}�(h��On some touchpads, libinput imposes a limit on how the fingers may be placed
on the touchpad. In the most common use-case this allows for a user to
trigger a click with the thumb while leaving the pointer-moving finger on
the touchpad.�h]�h9��On some touchpads, libinput imposes a limit on how the fingers may be placed
on the touchpad. In the most common use-case this allows for a user to
trigger a click with the thumb while leaving the pointer-moving finger on
the touchpad.�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKthj�  hhubj�  )��}�(hhh]�(j�  )��}�(h�n.. figure:: clickfinger-distance.svg
    :align: center

    Illustration of the distance detection algorithm
�h]�h}�(h]�h]�h]�h]�h]��uri��clickfinger-distance.svg�j�  }�j�  j  suhj�  hj�  hh,hK|ubj�  )��}�(h�0Illustration of the distance detection algorithm�h]�h9�0Illustration of the distance detection algorithm�����}�(hj
  hj  ubah}�(h]�h]�h]�h]�h]�uhj�  hh,hK|hj�  ubeh}�(h]��id7�ah]�h]�h]�h]�j�  �center�uhj�  hK|hj�  hhhh,ubhE)��}�(h��In the illustration above the red area marks the proximity area around the
first finger. Since the thumb is outside of that area libinput considers the
click a single-finger click rather than a two-finger click.�h]�h9��In the illustration above the red area marks the proximity area around the
first finger. Since the thumb is outside of that area libinput considers the
click a single-finger click rather than a two-finger click.�����}�(hj   hj  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK~hj�  hhubh)��}�(h�.. _special_clickpads:�h]�h}�(h]�h]�h]�h]�h]�h*�special-clickpads�uhhhK�hj�  hhhh,ubeh}�(h]�(�clickfinger-behavior�j�  eh]�h]�(�clickfinger behavior��clickfinger�eh]�h]�uhh-hh/hhhh,hKgj�  }�j=  j�  sj�  }�j�  j�  subh.)��}�(hhh]�(h3)��}�(h�Special Clickpads�h]�h9�Special Clickpads�����}�(hjG  hjE  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hjB  hhhh,hK�ubhE)��}�(h��The Lenovo \*40 series laptops have a clickpad that provides two software button sections, one at
the top and one at the bottom. See :ref:`Lenovo \*40 series touchpad support <t440_support>`
for details on the top software button.�h]�(h9��The Lenovo *40 series laptops have a clickpad that provides two software button sections, one at
the top and one at the bottom. See �����}�(h��The Lenovo \*40 series laptops have a clickpad that provides two software button sections, one at
the top and one at the bottom. See �hjS  hhhNhNubhQ)��}�(h�9:ref:`Lenovo \*40 series touchpad support <t440_support>`�h]�hW)��}�(hj^  h]�h9�"Lenovo *40 series touchpad support�����}�(hhhj`  ubah}�(h]�h]�(hb�std��std-ref�eh]�h]�h]�uhhVhj\  ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�jj  �refexplicit��hr�t440_support�hthuhv�uhhPhh,hK�hjS  ubh9�(
for details on the top software button.�����}�(h�(
for details on the top software button.�hjS  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK�hjB  hhubhE)��}�(hX   Some Clickpads, notably some Cypress ones, perform right button detection in
firmware and appear to userspace as if the touchpad had physical buttons.
While physically clickpads, these are not handled by the software and
treated like traditional touchpads.�h]�h9X   Some Clickpads, notably some Cypress ones, perform right button detection in
firmware and appear to userspace as if the touchpad had physical buttons.
While physically clickpads, these are not handled by the software and
treated like traditional touchpads.�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK�hjB  hhubeh}�(h]�(j6  �id1�eh]�h]�(�special clickpads��special_clickpads�eh]�h]�uhh-hh/hhhh,hK�j�  }�j�  j,  sj�  }�j6  j,  subeh}�(h]�(�!clickpad-software-button-behavior�h+eh]�h]�(�!clickpad software button behavior��clickpad_softbuttons�eh]�h]�uhh-hhhhhh,hKj�  }�j�  h sj�  }�h+h subeh}�(h]�h]�h]�h]�h]��source�h,uhh�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(h2N�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}�(�git_version�h �substitution_definition���)��}�(h�,.. |git_version| replace:: :commit:`3325b81`�h]�h�)��}�(h�git commit 3325b81�h]�h9�git commit 3325b81�����}�(hhhj
  ubah}�(h]�h]�h]�h]�h]��internal���refuri��?https://gitlab.freedesktop.org/libinput/libinput/commit/3325b81�uhh�hj  ubah}�(h]�h]�h]�j  ah]�h]�uhj  h�<rst_prolog>�hKhhub�git_version_full�j  )��}�(h�].. |git_version_full| replace:: :commit:`<function get_git_version_full at 0x7f4db2896f28>`

�h]�h�)��}�(h�<git commit <function get_git_version_full at 0x7f4db2896f28>�h]�h9�<git commit <function get_git_version_full at 0x7f4db2896f28>�����}�(hhhj'  ubah}�(h]�h]�h]�h]�h]��internal���refuri��ihttps://gitlab.freedesktop.org/libinput/libinput/commit/<function get_git_version_full at 0x7f4db2896f28>�uhh�hj#  ubah}�(h]�h]�h]�j"  ah]�h]�uhj  hj!  hKhhubu�substitution_names�}�(�git_version�j  �git_version_full�j"  u�refnames�}��refids�}�(h+]�h ajh  ]�j^  aj�  ]�j�  aj6  ]�j,  au�nameids�}�(j�  h+j�  j�  h�h�h�h�j�  jh  j�  j�  j=  j�  j<  j9  j�  j6  j�  j�  u�	nametypes�}�(j�  �j�  Nh��h�j�  �j�  Nj=  �j<  Nj�  �j�  Nuh}�(h+h/j�  h/h�h�h�h�jh  ji  j�  ji  j�  j�  j9  j�  j6  jB  j�  jB  j�  j�  j;  j  j�  j�  jy  j5  j�  j�  j  j�  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]��transform_messages�]�(h �system_message���)��}�(hhh]�hE)��}�(hhh]�h9�:Hyperlink target "clickpad-softbuttons" is not referenced.�����}�(hhhjk  ubah}�(h]�h]�h]�h]�h]�uhhDhjh  ubah}�(h]�h]�h]�h]�h]��level�K�type��INFO��source�h,�line�Kuhjf  ubjg  )��}�(hhh]�hE)��}�(hhh]�h9�6Hyperlink target "software-buttons" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h,�line�Kuhjf  ubjg  )��}�(hhh]�hE)��}�(hhh]�h9�1Hyperlink target "clickfinger" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h,�line�Khuhjf  ubjg  )��}�(hhh]�hE)��}�(hhh]�h9�7Hyperlink target "special-clickpads" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h,�line�K�uhjf  ube�transformer�N�
decoration�Nhhub.