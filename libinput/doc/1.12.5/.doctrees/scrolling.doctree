���m      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �block_quote���)��}�(hhh]�(h �substitution_definition���)��}�(h�,.. |git_version| replace:: :commit:`7d25d9e`�h]�h �	reference���)��}�(h�git commit 7d25d9e�h]�h �Text����git commit 7d25d9e�����}�(hh�parent�huba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]��internal���refuri��?https://gitlab.freedesktop.org/libinput/libinput/commit/7d25d9e�u�tagname�hh hubah!}�(h#]�h%]�h']��git_version�ah)]�h+]�uh0h�source��<rst_prolog>��line�Kh hubh)��}�(h�].. |git_version_full| replace:: :commit:`<function get_git_version_full at 0x7f4b9dfe32f0>`

�h]�h)��}�(h�<git commit <function get_git_version_full at 0x7f4b9dfe32f0>�h]�h�<git commit <function get_git_version_full at 0x7f4b9dfe32f0>�����}�(hhh h?ubah!}�(h#]�h%]�h']�h)]�h+]��internal���refuri��ihttps://gitlab.freedesktop.org/libinput/libinput/commit/<function get_git_version_full at 0x7f4b9dfe32f0>�uh0hh h;ubah!}�(h#]�h%]�h']��git_version_full�ah)]�h+]�uh0hh8h9h:Kh hubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h	h hhhh8Nh:Nubh �target���)��}�(h�.. _scrolling:�h]�h!}�(h#]�h%]�h']�h)]�h+]��refid��	scrolling�uh0h]h:Kh hhhh8�5/home/whot/code/libinput/build/doc/user/scrolling.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�	Scrolling�h]�h�	Scrolling�����}�(hhuh hshhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh hnhhh8hkh:Kubh �	paragraph���)��}�(h��libinput supports three different types of scrolling methods:
:ref:`twofinger_scrolling`, :ref:`edge_scrolling` and
:ref:`button_scrolling`. Some devices support multiple methods, though only
one can be enabled at a time. As a general overview:�h]�(h�>libinput supports three different types of scrolling methods:
�����}�(h�>libinput supports three different types of scrolling methods:
�h h�hhh8Nh:Nub�sphinx.addnodes��pending_xref���)��}�(h�:ref:`twofinger_scrolling`�h]�h �inline���)��}�(hh�h]�h�twofinger_scrolling�����}�(hhh h�ubah!}�(h#]�h%]�(�xref��std��std-ref�eh']�h)]�h+]�uh0h�h h�ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�h��refexplicit���	reftarget��twofinger_scrolling��refdoc��	scrolling��refwarn��uh0h�h8hkh:Kh h�ubh�, �����}�(h�, �h h�hhh8Nh:Nubh�)��}�(h�:ref:`edge_scrolling`�h]�h�)��}�(hh�h]�h�edge_scrolling�����}�(hhh h�ubah!}�(h#]�h%]�(h��std��std-ref�eh']�h)]�h+]�uh0h�h h�ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�hǌrefexplicit��h��edge_scrolling�h�h�h��uh0h�h8hkh:Kh h�ubh� and
�����}�(h� and
�h h�hhh8Nh:Nubh�)��}�(h�:ref:`button_scrolling`�h]�h�)��}�(hh�h]�h�button_scrolling�����}�(hhh h�ubah!}�(h#]�h%]�(h��std��std-ref�eh']�h)]�h+]�uh0h�h h�ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�h�refexplicit��h��button_scrolling�h�h�h��uh0h�h8hkh:Kh h�ubh�i. Some devices support multiple methods, though only
one can be enabled at a time. As a general overview:�����}�(h�i. Some devices support multiple methods, though only
one can be enabled at a time. As a general overview:�h h�hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh hnhhubh �bullet_list���)��}�(hhh]�(h �	list_item���)��}�(h�_touchpad devices with physical buttons below the touchpad support edge and
two-finger scrolling�h]�h�)��}�(h�_touchpad devices with physical buttons below the touchpad support edge and
two-finger scrolling�h]�h�_touchpad devices with physical buttons below the touchpad support edge and
two-finger scrolling�����}�(hj  h j  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh j  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j
  h j  hhh8hkh:Nubj  )��}�(h�utouchpad devices without physical buttons (:ref:`ClickPads <clickpad_softbuttons>`)
support two-finger scrolling only�h]�h�)��}�(h�utouchpad devices without physical buttons (:ref:`ClickPads <clickpad_softbuttons>`)
support two-finger scrolling only�h]�(h�+touchpad devices without physical buttons (�����}�(h�+touchpad devices without physical buttons (�h j(  ubh�)��}�(h�':ref:`ClickPads <clickpad_softbuttons>`�h]�h�)��}�(hj3  h]�h�	ClickPads�����}�(hhh j5  ubah!}�(h#]�h%]�(h��std��std-ref�eh']�h)]�h+]�uh0h�h j1  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�j?  �refexplicit��h��clickpad_softbuttons�h�h�h��uh0h�h8hkh:Kh j(  ubh�#)
support two-finger scrolling only�����}�(h�#)
support two-finger scrolling only�h j(  ubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh j$  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j
  h j  hhh8hkh:Nubj  )��}�(h�6pointing sticks provide on-button scrolling by default�h]�h�)��}�(hjb  h]�h�6pointing sticks provide on-button scrolling by default�����}�(hjb  h jd  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh j`  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j
  h j  hhh8hkh:Nubj  )��}�(h�]mice and other pointing devices support on-button scrolling but it is not
enabled by default
�h]�h�)��}�(h�\mice and other pointing devices support on-button scrolling but it is not
enabled by default�h]�h�\mice and other pointing devices support on-button scrolling but it is not
enabled by default�����}�(hj}  h j{  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh jw  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j
  h j  hhh8hkh:Nubeh!}�(h#]�h%]�h']�h)]�h+]��bullet��-�uh0j  h8hkh:Kh hnhhubh�)��}�(hX  A device may differ from the above based on its capabilities. See
**libinput_device_config_scroll_set_method()** for documentation on how to
switch methods and **libinput_device_config_scroll_get_methods()** for
documentation on how to query a device for available scroll methods.�h]�(h�BA device may differ from the above based on its capabilities. See
�����}�(h�BA device may differ from the above based on its capabilities. See
�h j�  hhh8Nh:Nubh �strong���)��}�(h�.**libinput_device_config_scroll_set_method()**�h]�h�*libinput_device_config_scroll_set_method()�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j�  h j�  ubh�0 for documentation on how to
switch methods and �����}�(h�0 for documentation on how to
switch methods and �h j�  hhh8Nh:Nubj�  )��}�(h�/**libinput_device_config_scroll_get_methods()**�h]�h�+libinput_device_config_scroll_get_methods()�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j�  h j�  ubh�I for
documentation on how to query a device for available scroll methods.�����}�(h�I for
documentation on how to query a device for available scroll methods.�h j�  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh hnhhubh^)��}�(h�.. _horizontal_scrolling:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�horizontal-scrolling�uh0h]h:Kh hnhhh8hkubhm)��}�(hhh]�(hr)��}�(h�Horizontal scrolling�h]�h�Horizontal scrolling�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j�  hhh8hkh:Kubh�)��}�(hX�  Scroll movements provide vertical and horizontal directions, each
scroll event contains both directions where applicable, see
**libinput_event_pointer_get_axis_value()**. libinput does not provide separate
toggles to enable or disable horizontal scrolling. Instead, horizontal
scrolling is always enabled. This is intentional, libinput does not have
enough context to know when horizontal scrolling is appropriate for a given
widget. The task of filtering horizontal movements is up to the caller.�h]�(h�~Scroll movements provide vertical and horizontal directions, each
scroll event contains both directions where applicable, see
�����}�(h�~Scroll movements provide vertical and horizontal directions, each
scroll event contains both directions where applicable, see
�h j�  hhh8Nh:Nubj�  )��}�(h�+**libinput_event_pointer_get_axis_value()**�h]�h�'libinput_event_pointer_get_axis_value()�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j�  h j�  ubhXH  . libinput does not provide separate
toggles to enable or disable horizontal scrolling. Instead, horizontal
scrolling is always enabled. This is intentional, libinput does not have
enough context to know when horizontal scrolling is appropriate for a given
widget. The task of filtering horizontal movements is up to the caller.�����}�(hXH  . libinput does not provide separate
toggles to enable or disable horizontal scrolling. Instead, horizontal
scrolling is always enabled. This is intentional, libinput does not have
enough context to know when horizontal scrolling is appropriate for a given
widget. The task of filtering horizontal movements is up to the caller.�h j�  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh j�  hhubh^)��}�(h�.. _twofinger_scrolling:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�twofinger-scrolling�uh0h]h:K,h j�  hhh8hkubeh!}�(h#]�(j�  �id2�eh%]�h']�(�horizontal scrolling��horizontal_scrolling�eh)]�h+]�uh0hlh hnhhh8hkh:K�expect_referenced_by_name�}�j  j�  s�expect_referenced_by_id�}�j�  j�  subhm)��}�(hhh]�(hr)��}�(h�Two-finger scrolling�h]�h�Two-finger scrolling�����}�(hj)  h j'  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j$  hhh8hkh:K+ubh�)��}�(h��The default on two-finger capable touchpads (almost all modern touchpads are
capable of detecting two fingers). Scrolling is triggered by two fingers
being placed on the surface of the touchpad, then moving those fingers
vertically or horizontally.�h]�h��The default on two-finger capable touchpads (almost all modern touchpads are
capable of detecting two fingers). Scrolling is triggered by two fingers
being placed on the surface of the touchpad, then moving those fingers
vertically or horizontally.�����}�(hj7  h j5  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K-h j$  hhubh �figure���)��}�(hhh]�(h �image���)��}�(h�i.. figure:: twofinger-scrolling.svg
    :align: center

    Vertical and horizontal two-finger scrolling
�h]�h!}�(h#]�h%]�h']�h)]�h+]��uri��twofinger-scrolling.svg��
candidates�}��*�jU  suh0jH  h jE  h8hkh:K5ubh �caption���)��}�(h�,Vertical and horizontal two-finger scrolling�h]�h�,Vertical and horizontal two-finger scrolling�����}�(hj]  h j[  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0jY  h8hkh:K5h jE  ubeh!}�(h#]��id5�ah%]�h']�h)]�h+]��align��center�uh0jC  h:K5h j$  hhh8hkubh�)��}�(hX�  For scrolling to trigger, a built-in distance threshold has to be met but once
engaged any movement will scroll. In other words, to start scrolling a
sufficiently large movement is required, once scrolling tiny amounts of
movements will translate into tiny scroll movements.
Scrolling in both directions at once is possible by meeting the required
distance thresholds to enable each direction separately.�h]�hX�  For scrolling to trigger, a built-in distance threshold has to be met but once
engaged any movement will scroll. In other words, to start scrolling a
sufficiently large movement is required, once scrolling tiny amounts of
movements will translate into tiny scroll movements.
Scrolling in both directions at once is possible by meeting the required
distance thresholds to enable each direction separately.�����}�(hjt  h jr  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K7h j$  hhubh�)��}�(h��When a scroll gesture remains close to perfectly straight, it will be held to
exact 90-degree angles; but if the gesture moves diagonally, it is free to
scroll in any direction.�h]�h��When a scroll gesture remains close to perfectly straight, it will be held to
exact 90-degree angles; but if the gesture moves diagonally, it is free to
scroll in any direction.�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K>h j$  hhubh�)��}�(hX�  Two-finger scrolling requires the touchpad to track both touch points with
reasonable precision. Unfortunately, some so-called "semi-mt" touchpads can
only track the bounding box of the two fingers rather than the actual
position of each finger. In addition, that bounding box usually suffers from
a low resolution, causing jumpy movement during two-finger scrolling.
libinput does not provide two-finger scrolling on those touchpads.�h]�hX�  Two-finger scrolling requires the touchpad to track both touch points with
reasonable precision. Unfortunately, some so-called “semi-mt” touchpads can
only track the bounding box of the two fingers rather than the actual
position of each finger. In addition, that bounding box usually suffers from
a low resolution, causing jumpy movement during two-finger scrolling.
libinput does not provide two-finger scrolling on those touchpads.�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:KBh j$  hhubh^)��}�(h�.. _edge_scrolling:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�edge-scrolling�uh0h]h:KNh j$  hhh8hkubeh!}�(h#]�(�two-finger-scrolling�j  eh%]�h']�(�two-finger scrolling��twofinger_scrolling�eh)]�h+]�uh0hlh hnhhh8hkh:K+j   }�j�  j  sj"  }�j  j  subhm)��}�(hhh]�(hr)��}�(h�Edge scrolling�h]�h�Edge scrolling�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j�  hhh8hkh:KMubh�)��}�(h��On some touchpads, edge scrolling is available, triggered by moving a single
finger along the right edge (vertical scroll) or bottom edge (horizontal
scroll).�h]�h��On some touchpads, edge scrolling is available, triggered by moving a single
finger along the right edge (vertical scroll) or bottom edge (horizontal
scroll).�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:KOh j�  hhubjD  )��}�(hhh]�(jI  )��}�(h�^.. figure:: edge-scrolling.svg
    :align: center

    Vertical and horizontal edge scrolling
�h]�h!}�(h#]�h%]�h']�h)]�h+]��uri��edge-scrolling.svg�jV  }�jX  j�  suh0jH  h j�  h8hkh:KVubjZ  )��}�(h�&Vertical and horizontal edge scrolling�h]�h�&Vertical and horizontal edge scrolling�����}�(hj�  h j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0jY  h8hkh:KVh j�  ubeh!}�(h#]��id6�ah%]�h']�h)]�h+]�jp  �center�uh0jC  h:KVh j�  hhh8hkubh�)��}�(h��Due to the layout of the edges, diagonal scrolling is not possible. The
behavior of edge scrolling using both edges at the same time is undefined.�h]�h��Due to the layout of the edges, diagonal scrolling is not possible. The
behavior of edge scrolling using both edges at the same time is undefined.�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:KXh j�  hhubh�)��}�(h�hEdge scrolling overlaps with :ref:`clickpad_softbuttons`. A physical click on
a clickpad ends scrolling.�h]�(h�Edge scrolling overlaps with �����}�(h�Edge scrolling overlaps with �h j  hhh8Nh:Nubh�)��}�(h�:ref:`clickpad_softbuttons`�h]�h�)��}�(hj  h]�h�clickpad_softbuttons�����}�(hhh j  ubah!}�(h#]�h%]�(h��std��std-ref�eh']�h)]�h+]�uh0h�h j  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�j  �refexplicit��h��clickpad_softbuttons�h�h�h��uh0h�h8hkh:K[h j  ubh�0. A physical click on
a clickpad ends scrolling.�����}�(h�0. A physical click on
a clickpad ends scrolling.�h j  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K[h j�  hhubh^)��}�(h�.. _button_scrolling:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�button-scrolling�uh0h]h:Kch j�  hhh8hkubeh!}�(h#]�(j�  �id3�eh%]�h']�(�edge scrolling��edge_scrolling�eh)]�h+]�uh0hlh hnhhh8hkh:KMj   }�jH  j�  sj"  }�j�  j�  subhm)��}�(hhh]�(hr)��}�(h�On-Button scrolling�h]�h�On-Button scrolling�����}�(hjR  h jP  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh jM  hhh8hkh:Kbubh�)��}�(hX(  On-button scrolling converts the motion of a device into scroll events while
a designated button is held down. For example, Lenovo devices provide a
`pointing stick <http://en.wikipedia.org/wiki/Pointing_stick>`_ that emulates
scroll events when the trackstick's middle mouse button is held down.�h]�(h��On-button scrolling converts the motion of a device into scroll events while
a designated button is held down. For example, Lenovo devices provide a
�����}�(h��On-button scrolling converts the motion of a device into scroll events while
a designated button is held down. For example, Lenovo devices provide a
�h j^  hhh8Nh:Nubh)��}�(h�?`pointing stick <http://en.wikipedia.org/wiki/Pointing_stick>`_�h]�h�pointing stick�����}�(hhh jg  ubah!}�(h#]�h%]�h']�h)]�h+]��name��pointing stick��refuri��+http://en.wikipedia.org/wiki/Pointing_stick�uh0hh j^  ubh^)��}�(h�. <http://en.wikipedia.org/wiki/Pointing_stick>�h]�h!}�(h#]��pointing-stick�ah%]�h']��pointing stick�ah)]�h+]��refuri�jx  uh0h]�
referenced�Kh j^  ubh�V that emulates
scroll events when the trackstick’s middle mouse button is held down.�����}�(h�T that emulates
scroll events when the trackstick's middle mouse button is held down.�h j^  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kdh jM  hhubh �note���)��}�(h��On-button scrolling is enabled by default for pointing sticks. This
prevents middle-button dragging; all motion events while the middle
button is down are converted to scroll events.�h]�h�)��}�(h��On-button scrolling is enabled by default for pointing sticks. This
prevents middle-button dragging; all motion events while the middle
button is down are converted to scroll events.�h]�h��On-button scrolling is enabled by default for pointing sticks. This
prevents middle-button dragging; all motion events while the middle
button is down are converted to scroll events.�����}�(hj�  h j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kih j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j�  h jM  hhh8hkh:NubjD  )��}�(hhh]�(jI  )��}�(h�J.. figure:: button-scrolling.svg
    :align: center

    Button scrolling
�h]�h!}�(h#]�h%]�h']�h)]�h+]��uri��button-scrolling.svg�jV  }�jX  j�  suh0jH  h j�  h8hkh:KpubjZ  )��}�(h�Button scrolling�h]�h�Button scrolling�����}�(hj�  h j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0jY  h8hkh:Kph j�  ubeh!}�(h#]��id7�ah%]�h']�h)]�h+]�jp  �center�uh0jC  h:Kph jM  hhh8hkubh�)��}�(hX@  The button may be changed with
**libinput_device_config_scroll_set_button()** but must be on the same device as
the motion events. Cross-device scrolling is not supported but
for one exception: libinput's :ref:`t440_support` enables the use of the middle
button for button scrolling (even when the touchpad is disabled).�h]�(h�The button may be changed with
�����}�(h�The button may be changed with
�h j�  hhh8Nh:Nubj�  )��}�(h�.**libinput_device_config_scroll_set_button()**�h]�h�*libinput_device_config_scroll_set_button()�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j�  h j�  ubh�� but must be on the same device as
the motion events. Cross-device scrolling is not supported but
for one exception: libinput’s �����}�(h�� but must be on the same device as
the motion events. Cross-device scrolling is not supported but
for one exception: libinput's �h j�  hhh8Nh:Nubh�)��}�(h�:ref:`t440_support`�h]�h�)��}�(hj�  h]�h�t440_support�����}�(hhh j�  ubah!}�(h#]�h%]�(h��std��std-ref�eh']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�j�  �refexplicit��h��t440_support�h�h�h��uh0h�h8hkh:Krh j�  ubh�` enables the use of the middle
button for button scrolling (even when the touchpad is disabled).�����}�(h�` enables the use of the middle
button for button scrolling (even when the touchpad is disabled).�h j�  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Krh jM  hhubh^)��}�(h�.. _scroll_sources:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�scroll-sources�uh0h]h:K}h jM  hhh8hkubeh!}�(h#]�(�on-button-scrolling�jA  eh%]�h']�(�on-button scrolling��button_scrolling�eh)]�h+]�uh0hlh hnhhh8hkh:Kbj   }�j(  j7  sj"  }�jA  j7  subhm)��}�(hhh]�(hr)��}�(h�Scroll sources�h]�h�Scroll sources�����}�(hj2  h j0  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j-  hhh8hkh:K|ubh�)��}�(hXI  libinput provides a pointer axis *source* for each scroll event. The
source can be obtained with the **libinput_event_pointer_get_axis_source()**
function and is one of **wheel**, **finger**, or **continuous**. The source
information lets a caller decide when to implement kinetic scrolling.
Usually, a caller will process events of source wheel as they come in.
For events of source finger a caller should calculate the velocity of the
scroll motion and upon finger release start a kinetic scrolling motion (i.e.
continue executing a scroll according to some friction factor).
libinput expects the caller to be in charge of widget handling, the source
information is thus enough to provide kinetic scrolling on a per-widget
basis. A caller should cancel kinetic scrolling when the pointer leaves the
current widget or when a key is pressed.�h]�(h�!libinput provides a pointer axis �����}�(h�!libinput provides a pointer axis �h j>  hhh8Nh:Nubh �emphasis���)��}�(h�*source*�h]�h�source�����}�(hhh jI  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0jG  h j>  ubh�< for each scroll event. The
source can be obtained with the �����}�(h�< for each scroll event. The
source can be obtained with the �h j>  hhh8Nh:Nubj�  )��}�(h�,**libinput_event_pointer_get_axis_source()**�h]�h�(libinput_event_pointer_get_axis_source()�����}�(hhh j\  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j�  h j>  ubh�
function and is one of �����}�(h�
function and is one of �h j>  hhh8Nh:Nubj�  )��}�(h�	**wheel**�h]�h�wheel�����}�(hhh jo  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j�  h j>  ubh�, �����}�(h�, �h j>  hhh8Nh:Nubj�  )��}�(h�
**finger**�h]�h�finger�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j�  h j>  ubh�, or �����}�(h�, or �h j>  hhh8Nh:Nubj�  )��}�(h�**continuous**�h]�h�
continuous�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j�  h j>  ubhXx  . The source
information lets a caller decide when to implement kinetic scrolling.
Usually, a caller will process events of source wheel as they come in.
For events of source finger a caller should calculate the velocity of the
scroll motion and upon finger release start a kinetic scrolling motion (i.e.
continue executing a scroll according to some friction factor).
libinput expects the caller to be in charge of widget handling, the source
information is thus enough to provide kinetic scrolling on a per-widget
basis. A caller should cancel kinetic scrolling when the pointer leaves the
current widget or when a key is pressed.�����}�(hXx  . The source
information lets a caller decide when to implement kinetic scrolling.
Usually, a caller will process events of source wheel as they come in.
For events of source finger a caller should calculate the velocity of the
scroll motion and upon finger release start a kinetic scrolling motion (i.e.
continue executing a scroll according to some friction factor).
libinput expects the caller to be in charge of widget handling, the source
information is thus enough to provide kinetic scrolling on a per-widget
basis. A caller should cancel kinetic scrolling when the pointer leaves the
current widget or when a key is pressed.�h j>  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K~h j-  hhubh�)��}�(h�gSee the **libinput_event_pointer_get_axis_source()** for details on the
behavior of each scroll source.�h]�(h�See the �����}�(h�See the �h j�  hhh8Nh:Nubj�  )��}�(h�,**libinput_event_pointer_get_axis_source()**�h]�h�(libinput_event_pointer_get_axis_source()�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j�  h j�  ubh�3 for details on the
behavior of each scroll source.�����}�(h�3 for details on the
behavior of each scroll source.�h j�  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h j-  hhubh�)��}�(h�JSee also http://who-t.blogspot.com.au/2015/03/libinput-scroll-sources.html�h]�(h�	See also �����}�(h�	See also �h j�  hhh8Nh:Nubh)��}�(h�Ahttp://who-t.blogspot.com.au/2015/03/libinput-scroll-sources.html�h]�h�Ahttp://who-t.blogspot.com.au/2015/03/libinput-scroll-sources.html�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]��refuri�j�  uh0hh j�  ubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h j-  hhubeh!}�(h#]�(j!  �id4�eh%]�h']�(�scroll sources��scroll_sources�eh)]�h+]�uh0hlh hnhhh8hkh:K|j   }�j�  j  sj"  }�j!  j  subeh!}�(h#]�(hj�id1�eh%]�h']��	scrolling�ah)]��	scrolling�ah+]�uh0hlh hhhh8hkh:Kj�  Kj   }�j�  h_sj"  }�hjh_subeh!}�(h#]�h%]�h']�h)]�h+]��source�hkuh0h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hqN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j'  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�hk�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�N�gettext_compact��ub�reporter�N�indirect_targets�]��substitution_defs�}�(h5hhTh;u�substitution_names�}�(�git_version�h5�git_version_full�hTu�refnames�}��refids�}�(hj]�h_aj�  ]�j�  aj  ]�j  aj�  ]�j�  ajA  ]�j7  aj!  ]�j  au�nameids�}�(j�  hjj  j�  j  j  j�  j  j�  j�  jH  j�  jG  jD  j(  jA  j'  j$  j�  j  j�  j!  j�  j�  u�	nametypes�}�(j�  �j  �j  Nj�  �j�  NjH  �jG  Nj(  �j'  Nj�  �j�  �j�  Nuh#}�(hjhnj�  hnj�  j�  j  j�  j  j$  j�  j$  j�  j�  jD  j�  jA  jM  j$  jM  j  jy  j!  j-  j�  j-  jk  jE  j�  j�  j�  j�  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]�h �system_message���)��}�(hhh]�h�)��}�(h�,Duplicate implicit target name: "scrolling".�h]�h�0Duplicate implicit target name: “scrolling”.�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]�j�  a�level�K�type��INFO��source�hk�line�Kuh0j�  h hnhhh8hkh:Kuba�transform_messages�]�(j�  )��}�(hhh]�h�)��}�(hhh]�h�/Hyperlink target "scrolling" is not referenced.�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j�  �source�hk�line�Kuh0j�  ubj�  )��}�(hhh]�h�)��}�(hhh]�h�:Hyperlink target "horizontal-scrolling" is not referenced.�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j�  �source�hk�line�Kuh0j�  ubj�  )��}�(hhh]�h�)��}�(hhh]�h�9Hyperlink target "twofinger-scrolling" is not referenced.�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j�  �source�hk�line�K,uh0j�  ubj�  )��}�(hhh]�h�)��}�(hhh]�h�4Hyperlink target "edge-scrolling" is not referenced.�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j�  �source�hk�line�KNuh0j�  ubj�  )��}�(hhh]�h�)��}�(hhh]�h�6Hyperlink target "button-scrolling" is not referenced.�����}�(hhh j  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j�  �source�hk�line�Kcuh0j�  ubj�  )��}�(hhh]�h�)��}�(hhh]�h�4Hyperlink target "scroll-sources" is not referenced.�����}�(hhh j,  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j)  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j�  �source�hk�line�K}uh0j�  ube�transformer�N�
decoration�Nhhub.