���-      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �block_quote���)��}�(hhh]��
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�h	�parent�hhh�source�N�line�Nubh �target���)��}�(h�.. _tablet-debugging:�h]�h}�(h]�h]�h]�h]�h]��refid��tablet-debugging�uhhhKhhhhh�</home/whot/code/libinput/build/doc/user/tablet-debugging.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�Debugging tablet issues�h]�h �Text����Debugging tablet issues�����}�(hh6hh4hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hh/hhhh,hKubh)��}�(h�.. _tablet-capabilities:�h]�h}�(h]�h]�h]�h]�h]�h*�tablet-capabilities�uhhhKhh/hhhh,ubh.)��}�(hhh]�(h3)��}�(h�Required tablet capabilities�h]�h9�Required tablet capabilities�����}�(hhThhRhhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hhOhhhh,hKubh �	paragraph���)��}�(h��To handle a tablet correctly, libinput requires a set of capabilities
on the device. When these capabilities are missing, libinput ignores the
device and prints an error to the log. This error messages reads�h]�h9��To handle a tablet correctly, libinput requires a set of capabilities
on the device. When these capabilities are missing, libinput ignores the
device and prints an error to the log. This error messages reads�����}�(hhdhhbhhhNhNubah}�(h]�h]�h]�h]�h]�uhh`hh,hKhhOhhubh �literal_block���)��}�(h�Pmissing tablet capabilities: xy pen btn-stylus resolution. Ignoring this device.�h]�h9�Pmissing tablet capabilities: xy pen btn-stylus resolution. Ignoring this device.�����}�(hhhhrubah}�(h]�h]�h]�h]�h]��	xml:space��preserve�uhhphKhhOhhhh,ubha)��}�(h�(or in older versions of libinput simply:�h]�h9�(or in older versions of libinput simply:�����}�(hh�hh�hhhNhNubah}�(h]�h]�h]�h]�h]�uhh`hh,hKhhOhhubhq)��}�(h�Ilibinput bug: device does not meet tablet criteria. Ignoring this device.�h]�h9�Ilibinput bug: device does not meet tablet criteria. Ignoring this device.�����}�(hhhh�ubah}�(h]�h]�h]�h]�h]�h�h�uhhphKhhOhhhh,ubha)��}�(h�hWhen a tablet is rejected, it is usually possible to verify the issue with
the ``libinput record`` tool.�h]�(h9�OWhen a tablet is rejected, it is usually possible to verify the issue with
the �����}�(h�OWhen a tablet is rejected, it is usually possible to verify the issue with
the �hh�hhhNhNubh �literal���)��}�(h�``libinput record``�h]�h9�libinput record�����}�(hhhh�ubah}�(h]�h]�h]�h]�h]�uhh�hh�ubh9� tool.�����}�(h� tool.�hh�hhhNhNubeh}�(h]�h]�h]�h]�h]�uhh`hh,hKhhOhhubh �bullet_list���)��}�(hhh]�(h �	list_item���)��}�(hX  **xy** indicates that the tablet is missing the ``ABS_X`` and/or ``ABS_Y``
axis. This indicates that the device is mislabelled and the udev tag
``ID_INPUT_TABLET`` is applied to a device that is not a tablet.
A bug should be filed against `systemd <http://github.com/systemd/systemd>`__.�h]�ha)��}�(hX  **xy** indicates that the tablet is missing the ``ABS_X`` and/or ``ABS_Y``
axis. This indicates that the device is mislabelled and the udev tag
``ID_INPUT_TABLET`` is applied to a device that is not a tablet.
A bug should be filed against `systemd <http://github.com/systemd/systemd>`__.�h]�(h �strong���)��}�(h�**xy**�h]�h9�xy�����}�(hhhh�ubah}�(h]�h]�h]�h]�h]�uhh�hh�ubh9�* indicates that the tablet is missing the �����}�(h�* indicates that the tablet is missing the �hh�ubh�)��}�(h�	``ABS_X``�h]�h9�ABS_X�����}�(hhhh�ubah}�(h]�h]�h]�h]�h]�uhh�hh�ubh9� and/or �����}�(h� and/or �hh�ubh�)��}�(h�	``ABS_Y``�h]�h9�ABS_Y�����}�(hhhh�ubah}�(h]�h]�h]�h]�h]�uhh�hh�ubh9�F
axis. This indicates that the device is mislabelled and the udev tag
�����}�(h�F
axis. This indicates that the device is mislabelled and the udev tag
�hh�ubh�)��}�(h�``ID_INPUT_TABLET``�h]�h9�ID_INPUT_TABLET�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]�uhh�hh�ubh9�L is applied to a device that is not a tablet.
A bug should be filed against �����}�(h�L is applied to a device that is not a tablet.
A bug should be filed against �hh�ubh �	reference���)��}�(h�/`systemd <http://github.com/systemd/systemd>`__�h]�h9�systemd�����}�(hhhj!  ubah}�(h]�h]�h]�h]�h]��name��systemd��refuri��!http://github.com/systemd/systemd�uhj  hh�ubh9�.�����}�(h�.�hh�ubeh}�(h]�h]�h]�h]�h]�uhh`hh,hKhh�ubah}�(h]�h]�h]�h]�h]�uhh�hh�hhhh,hNubh�)��}�(h��**pen** or **btn-stylus** indicates that the tablet does not have the
``BTN_TOOL_PEN`` or ``BTN_STYLUS`` bit set. libinput requires either or both
of them to be present. This indicates a bug in the kernel driver
or the HID descriptors of the device.�h]�ha)��}�(h��**pen** or **btn-stylus** indicates that the tablet does not have the
``BTN_TOOL_PEN`` or ``BTN_STYLUS`` bit set. libinput requires either or both
of them to be present. This indicates a bug in the kernel driver
or the HID descriptors of the device.�h]�(h�)��}�(h�**pen**�h]�h9�pen�����}�(hhhjL  ubah}�(h]�h]�h]�h]�h]�uhh�hjH  ubh9� or �����}�(h� or �hjH  ubh�)��}�(h�**btn-stylus**�h]�h9�
btn-stylus�����}�(hhhj_  ubah}�(h]�h]�h]�h]�h]�uhh�hjH  ubh9�- indicates that the tablet does not have the
�����}�(h�- indicates that the tablet does not have the
�hjH  ubh�)��}�(h�``BTN_TOOL_PEN``�h]�h9�BTN_TOOL_PEN�����}�(hhhjr  ubah}�(h]�h]�h]�h]�h]�uhh�hjH  ubh9� or �����}�(hj^  hjH  ubh�)��}�(h�``BTN_STYLUS``�h]�h9�
BTN_STYLUS�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhh�hjH  ubh9�� bit set. libinput requires either or both
of them to be present. This indicates a bug in the kernel driver
or the HID descriptors of the device.�����}�(h�� bit set. libinput requires either or both
of them to be present. This indicates a bug in the kernel driver
or the HID descriptors of the device.�hjH  ubeh}�(h]�h]�h]�h]�h]�uhh`hh,hK#hjD  ubah}�(h]�h]�h]�h]�h]�uhh�hh�hhhh,hNubh�)��}�(hXc  **resolution** indicates that the device does not have a resolution set
for the x and y axes. This can be fixed with a hwdb entry, locate and read
the `60-evdev.hwdb
<https://github.com/systemd/systemd/tree/master/hwdb/60-evdev.hwdb>`__ file
on your machine and file a pull request with the fixes against
`systemd <https://github.com/systemd/systemd/>`__.�h]�ha)��}�(hXc  **resolution** indicates that the device does not have a resolution set
for the x and y axes. This can be fixed with a hwdb entry, locate and read
the `60-evdev.hwdb
<https://github.com/systemd/systemd/tree/master/hwdb/60-evdev.hwdb>`__ file
on your machine and file a pull request with the fixes against
`systemd <https://github.com/systemd/systemd/>`__.�h]�(h�)��}�(h�**resolution**�h]�h9�
resolution�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubh9�� indicates that the device does not have a resolution set
for the x and y axes. This can be fixed with a hwdb entry, locate and read
the �����}�(h�� indicates that the device does not have a resolution set
for the x and y axes. This can be fixed with a hwdb entry, locate and read
the �hj�  ubj   )��}�(h�U`60-evdev.hwdb
<https://github.com/systemd/systemd/tree/master/hwdb/60-evdev.hwdb>`__�h]�h9�60-evdev.hwdb�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]��name��60-evdev.hwdb�j1  �Ahttps://github.com/systemd/systemd/tree/master/hwdb/60-evdev.hwdb�uhj  hj�  ubh9�E file
on your machine and file a pull request with the fixes against
�����}�(h�E file
on your machine and file a pull request with the fixes against
�hj�  ubj   )��}�(h�1`systemd <https://github.com/systemd/systemd/>`__�h]�h9�systemd�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]��name��systemd�j1  �#https://github.com/systemd/systemd/�uhj  hj�  ubh9�.�����}�(hj7  hj�  ubeh}�(h]�h]�h]�h]�h]�uhh`hh,hK'hj�  ubah}�(h]�h]�h]�h]�h]�uhh�hh�hhhh,hNubeh}�(h]�h]�h]�h]�h]��bullet��-�uhh�hh,hKhhOhhubeh}�(h]�(�required-tablet-capabilities�hNeh]�h]�(�required tablet capabilities��tablet-capabilities�eh]�h]�uhh-hh/hhhh,hK�expect_referenced_by_name�}�j  hDs�expect_referenced_by_id�}�hNhDsubeh}�(h]�(�debugging-tablet-issues�h+eh]�h]�(�debugging tablet issues��tablet-debugging�eh]�h]�uhh-hhhhhh,hKj  }�j  h sj  }�h+h subeh}�(h]�h]�h]�h]�h]��source�h,uhh�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(h2N�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j8  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}�(�git_version�h �substitution_definition���)��}�(h�,.. |git_version| replace:: :commit:`1bfbcd8`�h]�j   )��}�(h�git commit 1bfbcd8�h]�h9�git commit 1bfbcd8�����}�(hhhjv  ubah}�(h]�h]�h]�h]�h]��internal���refuri��?https://gitlab.freedesktop.org/libinput/libinput/commit/1bfbcd8�uhj  hjr  ubah}�(h]�h]�h]�jo  ah]�h]�uhjp  h�<rst_prolog>�hKhhub�git_version_full�jq  )��}�(h�].. |git_version_full| replace:: :commit:`<function get_git_version_full at 0x7fcfe9b2aea0>`

�h]�j   )��}�(h�<git commit <function get_git_version_full at 0x7fcfe9b2aea0>�h]�h9�<git commit <function get_git_version_full at 0x7fcfe9b2aea0>�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]��internal���refuri��ihttps://gitlab.freedesktop.org/libinput/libinput/commit/<function get_git_version_full at 0x7fcfe9b2aea0>�uhj  hj�  ubah}�(h]�h]�h]�j�  ah]�h]�uhjp  hj�  hKhhubu�substitution_names�}�(�git_version�jo  �git_version_full�j�  u�refnames�}��refids�}�(h+]�h ahN]�hDau�nameids�}�(j  h+j  j  j  hNj  j�  u�	nametypes�}�(j  �j  Nj  �j  Nuh}�(h+h/j  h/hNhOj�  hOu�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]��transform_messages�]�(h �system_message���)��}�(hhh]�ha)��}�(hhh]�h9�6Hyperlink target "tablet-debugging" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhh`hj�  ubah}�(h]�h]�h]�h]�h]��level�K�type��INFO��source�h,�line�Kuhj�  ubj�  )��}�(hhh]�ha)��}�(hhh]�h9�9Hyperlink target "tablet-capabilities" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhh`hj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h,�line�Kuhj�  ube�transformer�N�
decoration�Nhhub.