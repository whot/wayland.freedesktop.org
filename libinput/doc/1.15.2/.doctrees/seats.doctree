���H      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �block_quote���)��}�(hhh]��
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�h	�parent�hhh�source�N�line�Nubh �target���)��}�(h�
.. _seats:�h]�h}�(h]�h]�h]�h]�h]��refid��seats�uhhhKhhhhh�P/home/whot/tmp/2020-02-20-Thu/wayland-web-sync/libinput/build/doc/user/seats.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�Seats�h]�h �Text����Seats�����}�(hh6hh4hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hh/hhhh,hKubh �	paragraph���)��}�(hX  Each device in libinput is assigned to one seat.
A seat has two identifiers, the physical name and the logical name. The
physical name is summarized as the list of devices a process on the same
physical seat has access to. The logical seat name is the seat name for a
logical group of devices. A compositor may use that to create additional
seats as independent device sets. Alternatively, a compositor may limit
itself to a single logical seat, leaving a second compositor to manage
devices on the other logical seats.�h]�h9X  Each device in libinput is assigned to one seat.
A seat has two identifiers, the physical name and the logical name. The
physical name is summarized as the list of devices a process on the same
physical seat has access to. The logical seat name is the seat name for a
logical group of devices. A compositor may use that to create additional
seats as independent device sets. Alternatively, a compositor may limit
itself to a single logical seat, leaving a second compositor to manage
devices on the other logical seats.�����}�(hhHhhFhhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKhh/hhubh)��}�(h�.. _seats_overview:�h]�h}�(h]�h]�h]�h]�h]�h*�seats-overview�uhhhKhh/hhhh,ubh.)��}�(hhh]�(h3)��}�(h�Overview�h]�h9�Overview�����}�(hhdhhbhhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hh_hhhh,hKubhE)��}�(h�JBelow is an illustration of how physical seats and logical seats interact:�h]�h9�JBelow is an illustration of how physical seats and logical seats interact:�����}�(hhrhhphhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKhh_hhub�sphinx.ext.graphviz��graphviz���)��}�(hhh]�h}�(h]�h]�h]�h]�h]��code�X�  digraph seats
{
  rankdir="BT";
  node [
    shape="box";
  ]

  kernel [label="Kernel"];

  event0 [URL="\ref libinput_event"];
  event1 [URL="\ref libinput_event"];
  event2 [URL="\ref libinput_event"];
  event3 [URL="\ref libinput_event"];

  pseat0 [label="phys seat0"; URL="\ref libinput_seat_get_physical_name"];
  pseat1 [label="phys seat1"; URL="\ref libinput_seat_get_physical_name"];

  lseatA [label="logical seat A"; URL="\ref libinput_seat_get_logical_name"];
  lseatB [label="logical seat B"; URL="\ref libinput_seat_get_logical_name"];
  lseatC [label="logical seat C"; URL="\ref libinput_seat_get_logical_name"];

  ctx1 [label="libinput context 1"; URL="\ref libinput"];
  ctx2 [label="libinput context 2"; URL="\ref libinput"];

  dev1 [label="device 'Foo'"];
  dev2 [label="device 'Bar'"];
  dev3 [label="device 'Spam'"];
  dev4 [label="device 'Egg'"];

  kernel -> event0
  kernel -> event1
  kernel -> event2
  kernel -> event3

  event0 -> pseat0
  event1 -> pseat0
  event2 -> pseat0
  event3 -> pseat1

  pseat0 -> ctx1
  pseat1 -> ctx2

  ctx1 -> lseatA
  ctx1 -> lseatB
  ctx2 -> lseatC

  lseatA -> dev1
  lseatA -> dev2
  lseatB -> dev3
  lseatC -> dev4
}
��options�}��docname��seats�suhhhh_hhhh,hKubhE)��}�(hX  The devices "Foo", "Bar" and "Spam" share the same physical seat and are
thus available in the same libinput context. Only "Foo" and "Bar" share the
same logical seat. The device "Egg" is not available in the libinput context
associated with the physical seat 0.�h]�h9X  The devices “Foo”, “Bar” and “Spam” share the same physical seat and are
thus available in the same libinput context. Only “Foo” and “Bar” share the
same logical seat. The device “Egg” is not available in the libinput context
associated with the physical seat 0.�����}�(hh�hh�hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKhh_hhubhE)��}�(h��The above graph is for illustration purposes only. In libinput, a struct
**libinput_seat** comprises both physical seat and logical seat. From a
caller's point-of-view the above device layout is presented as:�h]�(h9�IThe above graph is for illustration purposes only. In libinput, a struct
�����}�(h�IThe above graph is for illustration purposes only. In libinput, a struct
�hh�hhhNhNubh �strong���)��}�(h�**libinput_seat**�h]�h9�libinput_seat�����}�(h�libinput_seat�hh�ubah}�(h]�h]�h]�h]�h]�uhh�hh�ubh9�x comprises both physical seat and logical seat. From a
caller’s point-of-view the above device layout is presented as:�����}�(h�v comprises both physical seat and logical seat. From a
caller's point-of-view the above device layout is presented as:�hh�hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKhh_hhubh�)��}�(hhh]�h}�(h]�h]�h]�h]�h]�h�X9  digraph seats_libinput
{
  rankdir="BT";
  node [
    shape="box";
  ]

  ctx1 [label="libinput context 1"; URL="\ref libinput"];
  ctx2 [label="libinput context 2"; URL="\ref libinput"];

  seat0 [ label="seat phys 0 logical A"];
  seat1 [ label="seat phys 0 logical B"];
  seat2 [ label="seat phys 1 logical C"];

  dev1 [label="device 'Foo'"];
  dev2 [label="device 'Bar'"];
  dev3 [label="device 'Spam'"];
  dev4 [label="device 'Egg'"];

  ctx1 -> dev1
  ctx1 -> dev2
  ctx1 -> dev3
  ctx2 -> dev4

  dev1 -> seat0
  dev2 -> seat0
  dev3 -> seat1
  dev4 -> seat2
}
�h�}�h�h�suhhhh_hhhh,hK$ubhE)��}�(h��Thus, devices "Foo" and "Bar" both reference the same struct
**libinput_seat**, all other devices reference their own respective seats.�h]�(h9�EThus, devices “Foo” and “Bar” both reference the same struct
�����}�(h�=Thus, devices "Foo" and "Bar" both reference the same struct
�hh�hhhNhNubh�)��}�(h�**libinput_seat**�h]�h9�libinput_seat�����}�(h�libinput_seat�hh�ubah}�(h]�h]�h]�h]�h]�uhh�hh�ubh9�9, all other devices reference their own respective seats.�����}�(h�9, all other devices reference their own respective seats.�hh�hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK%hh_hhubh)��}�(h�.. _seats_and_features:�h]�h}�(h]�h]�h]�h]�h]�h*�seats-and-features�uhhhK-hh_hhhh,ubeh}�(h]�(�overview�h^eh]�h]�(�overview��seats_overview�eh]�h]�uhh-hh/hhhh,hK�expect_referenced_by_name�}�j  hTs�expect_referenced_by_id�}�h^hTsubh.)��}�(hhh]�(h3)��}�(h�The effect of seat assignment�h]�h9�The effect of seat assignment�����}�(hj  hj  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj	  hhhh,hK,ubhE)��}�(h��A logical set is interpreted as a group of devices that usually belong to a
single user that interacts with a computer. Thus, the devices are
semantically related. This means for devices within the same logical seat:�h]�h9��A logical set is interpreted as a group of devices that usually belong to a
single user that interacts with a computer. Thus, the devices are
semantically related. This means for devices within the same logical seat:�����}�(hj  hj  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK.hj	  hhubh �bullet_list���)��}�(hhh]�(h �	list_item���)��}�(h�pif the same button is pressed on different devices, the button should only
be considered logically pressed once.�h]�hE)��}�(h�pif the same button is pressed on different devices, the button should only
be considered logically pressed once.�h]�h9�pif the same button is pressed on different devices, the button should only
be considered logically pressed once.�����}�(hj5  hj3  ubah}�(h]�h]�h]�h]�h]�uhhDhh,hK2hj/  ubah}�(h]�h]�h]�h]�h]�uhj-  hj*  hhhh,hNubj.  )��}�(h�}if the same button is released on one device, the button should be
considered logically down if still down on another device.�h]�hE)��}�(h�}if the same button is released on one device, the button should be
considered logically down if still down on another device.�h]�h9�}if the same button is released on one device, the button should be
considered logically down if still down on another device.�����}�(hjM  hjK  ubah}�(h]�h]�h]�h]�h]�uhhDhh,hK4hjG  ubah}�(h]�h]�h]�h]�h]�uhj-  hj*  hhhh,hNubj.  )��}�(h�wif two different buttons or keys are pressed on different devices, the
logical state is that of both buttons/keys down.�h]�hE)��}�(h�wif two different buttons or keys are pressed on different devices, the
logical state is that of both buttons/keys down.�h]�h9�wif two different buttons or keys are pressed on different devices, the
logical state is that of both buttons/keys down.�����}�(hje  hjc  ubah}�(h]�h]�h]�h]�h]�uhhDhh,hK6hj_  ubah}�(h]�h]�h]�h]�h]�uhj-  hj*  hhhh,hNubj.  )��}�(h�]if a button is pressed on one device and another device moves, this should
count as dragging.�h]�hE)��}�(h�]if a button is pressed on one device and another device moves, this should
count as dragging.�h]�h9�]if a button is pressed on one device and another device moves, this should
count as dragging.�����}�(hj}  hj{  ubah}�(h]�h]�h]�h]�h]�uhhDhh,hK8hjw  ubah}�(h]�h]�h]�h]�h]�uhj-  hj*  hhhh,hNubj.  )��}�(h�]if two touches are down on different devices, the logical state is that of
two touches down.
�h]�hE)��}�(h�\if two touches are down on different devices, the logical state is that of
two touches down.�h]�h9�\if two touches are down on different devices, the logical state is that of
two touches down.�����}�(hj�  hj�  ubah}�(h]�h]�h]�h]�h]�uhhDhh,hK:hj�  ubah}�(h]�h]�h]�h]�h]�uhj-  hj*  hhhh,hNubeh}�(h]�h]�h]�h]�h]��bullet��-�uhj(  hh,hK2hj	  hhubhE)��}�(h��libinput provides functions to aid with the above:
**libinput_event_pointer_get_seat_button_count()**,
**libinput_event_keyboard_get_seat_key_count()**, and
**libinput_event_touch_get_seat_slot()**.�h]�(h9�3libinput provides functions to aid with the above:
�����}�(h�3libinput provides functions to aid with the above:
�hj�  hhhNhNubh�)��}�(h�2**libinput_event_pointer_get_seat_button_count()**�h]�h9�.libinput_event_pointer_get_seat_button_count()�����}�(h�.libinput_event_pointer_get_seat_button_count()�hj�  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubh9�,
�����}�(h�,
�hj�  hhhNhNubh�)��}�(h�0**libinput_event_keyboard_get_seat_key_count()**�h]�h9�,libinput_event_keyboard_get_seat_key_count()�����}�(h�,libinput_event_keyboard_get_seat_key_count()�hj�  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubh9�, and
�����}�(h�, and
�hj�  hhhNhNubh�)��}�(h�(**libinput_event_touch_get_seat_slot()**�h]�h9�$libinput_event_touch_get_seat_slot()�����}�(h�$libinput_event_touch_get_seat_slot()�hj�  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubh9�.�����}�(h�.�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK=hj	  hhubhE)��}�(hXI  Internally, libinput counts devices within the same logical seat as related.
Cross-device features only activate if all required devices are in the same
logical seat. For example, libinput will only activate the top software
buttons (see :ref:`t440_support`) if both trackstick and touchpad are assigned
to the same logical seat.�h]�(h9��Internally, libinput counts devices within the same logical seat as related.
Cross-device features only activate if all required devices are in the same
logical seat. For example, libinput will only activate the top software
buttons (see �����}�(h��Internally, libinput counts devices within the same logical seat as related.
Cross-device features only activate if all required devices are in the same
logical seat. For example, libinput will only activate the top software
buttons (see �hj�  hhhNhNub�sphinx.addnodes��pending_xref���)��}�(h�:ref:`t440_support`�h]�h �inline���)��}�(h�t440_support�h]�h9�t440_support�����}�(hhhj  ubah}�(h]�h]�(�xref��std��std-ref�eh]�h]�h]�uhj
  hj  ubah}�(h]�h]�h]�h]�h]��refdoc�h��	refdomain�j  �reftype��ref��refexplicit���refwarn���	reftarget��t440_support�uhj  hh,hKBhj�  ubh9�H) if both trackstick and touchpad are assigned
to the same logical seat.�����}�(h�H) if both trackstick and touchpad are assigned
to the same logical seat.�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKBhj	  hhubh)��}�(h�.. _changing_seats:�h]�h}�(h]�h]�h]�h]�h]�h*�changing-seats�uhhhKNhj	  hhhh,ubeh}�(h]�(�the-effect-of-seat-assignment�h�eh]�h]�(�the effect of seat assignment��seats_and_features�eh]�h]�uhh-hh/hhhh,hK,j  }�jG  h�sj  }�h�h�subh.)��}�(hhh]�(h3)��}�(h�Changing seats�h]�h9�Changing seats�����}�(hjQ  hjO  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hjL  hhhh,hKMubhE)��}�(h��A device may change the logical seat it is assigned to at runtime with
**libinput_device_set_seat_logical_name()**. The physical seat is immutable and
may not be changed.�h]�(h9�GA device may change the logical seat it is assigned to at runtime with
�����}�(h�GA device may change the logical seat it is assigned to at runtime with
�hj]  hhhNhNubh�)��}�(h�+**libinput_device_set_seat_logical_name()**�h]�h9�'libinput_device_set_seat_logical_name()�����}�(h�'libinput_device_set_seat_logical_name()�hjf  ubah}�(h]�h]�h]�h]�h]�uhh�hj]  ubh9�8. The physical seat is immutable and
may not be changed.�����}�(h�8. The physical seat is immutable and
may not be changed.�hj]  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKOhjL  hhubhE)��}�(h��Changing the logical seat for a device is equivalent to unplugging the
device and plugging it back in with the new logical seat. No device state
carries over across a logical seat change.�h]�h9��Changing the logical seat for a device is equivalent to unplugging the
device and plugging it back in with the new logical seat. No device state
carries over across a logical seat change.�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKShjL  hhubeh}�(h]�(j@  �id2�eh]�h]�(�changing seats��changing_seats�eh]�h]�uhh-hh/hhhh,hKMj  }�j�  j6  sj  }�j@  j6  subeh}�(h]�(h+�id1�eh]�h]��seats�ah]��seats�ah]�uhh-hhhhhh,hK�
referenced�Kj  }�j�  h sj  }�h+h subeh}�(h]�h]�h]�h]�h]��source�h,uhh�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(h2N�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}�(�git_version�h �substitution_definition���)��}�(h�,.. |git_version| replace:: :commit:`a3fb9bd`�h]�h �	reference���)��}�(h�git commit a3fb9bd�h]�h9�git commit a3fb9bd�����}�(h�a3fb9bd�hj  ubah}�(h]�h]�h]�h]�h]��internal���refuri��?https://gitlab.freedesktop.org/libinput/libinput/commit/a3fb9bd�uhj  hj  ubah}�(h]�h]�h]�j�  ah]�h]�uhj   h�<rst_prolog>�hKhhub�git_version_full�j  )��}�(h�].. |git_version_full| replace:: :commit:`<function get_git_version_full at 0x7fd3ac83e5f0>`

�h]�j  )��}�(h�<git commit <function get_git_version_full at 0x7fd3ac83e5f0>�h]�h9�<git commit <function get_git_version_full at 0x7fd3ac83e5f0>�����}�(h�1<function get_git_version_full at 0x7fd3ac83e5f0>�hj&  ubah}�(h]�h]�h]�h]�h]��internal���refuri��ihttps://gitlab.freedesktop.org/libinput/libinput/commit/<function get_git_version_full at 0x7fd3ac83e5f0>�uhj  hj"  ubah}�(h]�h]�h]�j!  ah]�h]�uhj   hj   hKhhubu�substitution_names�}�(�git_version�j�  �git_version_full�j!  u�refnames�}��refids�}�(h+]�h ah^]�hTah�]�h�aj@  ]�j6  au�nameids�}�(j�  h+j  h^j  h�jG  h�jF  jC  j�  j@  j�  j�  u�	nametypes�}�(j�  �j  �j  NjG  �jF  Nj�  �j�  Nuh}�(h+h/j�  h/h^h_h�h_h�j	  jC  j	  j@  jL  j�  jL  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]�h �system_message���)��}�(hhh]�hE)��}�(h�(Duplicate implicit target name: "seats".�h]�h9�,Duplicate implicit target name: “seats”.�����}�(hhhji  ubah}�(h]�h]�h]�h]�h]�uhhDhjf  ubah}�(h]�h]�h]�h]�h]�j�  a�level�K�type��INFO��source�h,�line�Kuhjd  hh/hhhh,hKuba�transform_messages�]�(je  )��}�(hhh]�hE)��}�(hhh]�h9�+Hyperlink target "seats" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j  �source�h,�line�Kuhjd  ubje  )��}�(hhh]�hE)��}�(hhh]�h9�4Hyperlink target "seats-overview" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j  �source�h,�line�Kuhjd  ubje  )��}�(hhh]�hE)��}�(hhh]�h9�8Hyperlink target "seats-and-features" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j  �source�h,�line�K-uhjd  ubje  )��}�(hhh]�hE)��}�(hhh]�h9�4Hyperlink target "changing-seats" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j  �source�h,�line�KNuhjd  ube�transformer�N�
decoration�Nhhub.