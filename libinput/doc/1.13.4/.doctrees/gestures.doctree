���b      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �block_quote���)��}�(hhh]��
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�h	�parent�hhh�source�N�line�Nubh �target���)��}�(h�.. _gestures:�h]�h}�(h]�h]�h]�h]�h]��refid��gestures�uhhhKhhhhh�4/home/whot/code/libinput/build/doc/user/gestures.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�Gestures�h]�h �Text����Gestures�����}�(hh6hh4hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hh/hhhh,hKubh �	paragraph���)��}�(h��libinput supports :ref:`gestures_pinch` and :ref:`gestures_swipe` on most
modern touchpads and other indirect touch devices. Note that libinput **does
not** support gestures on touchscreens, see :ref:`gestures_touchscreens`.�h]�(h9�libinput supports �����}�(h�libinput supports �hhFhhhNhNub�sphinx.addnodes��pending_xref���)��}�(h�:ref:`gestures_pinch`�h]�h �inline���)��}�(hhTh]�h9�gestures_pinch�����}�(hhhhXubah}�(h]�h]�(�xref��std��std-ref�eh]�h]�h]�uhhVhhRubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�hc�refexplicit���	reftarget��gestures_pinch��refdoc��gestures��refwarn��uhhPhh,hKhhFubh9� and �����}�(h� and �hhFhhhNhNubhQ)��}�(h�:ref:`gestures_swipe`�h]�hW)��}�(hh~h]�h9�gestures_swipe�����}�(hhhh�ubah}�(h]�h]�(hb�std��std-ref�eh]�h]�h]�uhhVhh|ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�h��refexplicit��hr�gestures_swipe�hthuhv�uhhPhh,hKhhFubh9�O on most
modern touchpads and other indirect touch devices. Note that libinput �����}�(h�O on most
modern touchpads and other indirect touch devices. Note that libinput �hhFhhhNhNubh �strong���)��}�(h�**does
not**�h]�h9�does
not�����}�(hhhh�ubah}�(h]�h]�h]�h]�h]�uhh�hhFubh9�' support gestures on touchscreens, see �����}�(h�' support gestures on touchscreens, see �hhFhhhNhNubhQ)��}�(h�:ref:`gestures_touchscreens`�h]�hW)��}�(hh�h]�h9�gestures_touchscreens�����}�(hhhh�ubah}�(h]�h]�(hb�std��std-ref�eh]�h]�h]�uhhVhh�ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�hrefexplicit��hr�gestures_touchscreens�hthuhv�uhhPhh,hKhhFubh9�.�����}�(h�.�hhFhhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKhh/hhubh)��}�(h�.. _gestures_lifetime:�h]�h}�(h]�h]�h]�h]�h]�h*�gestures-lifetime�uhhhKhh/hhhh,ubh.)��}�(hhh]�(h3)��}�(h�Lifetime of a gesture�h]�h9�Lifetime of a gesture�����}�(hh�hh�hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hh�hhhh,hKubhE)��}�(h��A gesture starts when the finger position and/or finger motion is
unambiguous as to what gesture to trigger and continues until the first
finger belonging to this gesture is lifted.�h]�h9��A gesture starts when the finger position and/or finger motion is
unambiguous as to what gesture to trigger and continues until the first
finger belonging to this gesture is lifted.�����}�(hh�hh�hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKhh�hhubhE)��}�(hXh  A single gesture cannot change the finger count. For example, if a user
puts down a fourth finger during a three-finger swipe gesture, libinput will
end the three-finger gesture and, if applicable, start a four-finger swipe
gesture. A caller may however decide that those gestures are semantically
identical and continue the two gestures as one single gesture.�h]�h9Xh  A single gesture cannot change the finger count. For example, if a user
puts down a fourth finger during a three-finger swipe gesture, libinput will
end the three-finger gesture and, if applicable, start a four-finger swipe
gesture. A caller may however decide that those gestures are semantically
identical and continue the two gestures as one single gesture.�����}�(hj	  hj  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKhh�hhubh)��}�(h�.. _gestures_pinch:�h]�h}�(h]�h]�h]�h]�h]�h*�gestures-pinch�uhhhK hh�hhhh,ubeh}�(h]�(�lifetime-of-a-gesture�h�eh]�h]�(�lifetime of a gesture��gestures_lifetime�eh]�h]�uhh-hh/hhhh,hK�expect_referenced_by_name�}�j&  h�s�expect_referenced_by_id�}�h�h�subh.)��}�(hhh]�(h3)��}�(h�Pinch gestures�h]�h9�Pinch gestures�����}�(hj2  hj0  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj-  hhhh,hKubhE)��}�(hX  Pinch gestures are executed when two or more fingers are located on the
touchpad and are either changing the relative distance to each other
(pinching) or are changing the relative angle (rotate). Pinch gestures may
change both rotation and distance at the same time. For such gestures,
libinput calculates a logical center for the gestures and provides the
caller with the delta x/y coordinates of that center, the relative angle of
the fingers compared to the previous event, and the absolute scale compared
to the initial finger position.�h]�h9X  Pinch gestures are executed when two or more fingers are located on the
touchpad and are either changing the relative distance to each other
(pinching) or are changing the relative angle (rotate). Pinch gestures may
change both rotation and distance at the same time. For such gestures,
libinput calculates a logical center for the gestures and provides the
caller with the delta x/y coordinates of that center, the relative angle of
the fingers compared to the previous event, and the absolute scale compared
to the initial finger position.�����}�(hj@  hj>  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK!hj-  hhubh �figure���)��}�(hhh]�(h �image���)��}�(h�U.. figure:: pinch-gestures.svg
    :align: center

    The pinch and rotate gestures
�h]�h}�(h]�h]�h]�h]�h]��uri��pinch-gestures.svg��
candidates�}��*�j^  suhjQ  hjN  hh,hK-ubh �caption���)��}�(h�The pinch and rotate gestures�h]�h9�The pinch and rotate gestures�����}�(hjf  hjd  ubah}�(h]�h]�h]�h]�h]�uhjb  hh,hK-hjN  ubeh}�(h]��id2�ah]�h]�h]�h]��align��center�uhjL  hK-hj-  hhhh,ubhE)��}�(h��The illustration above shows a basic pinch in the left image and a rotate in
the right angle. Not shown is a movement of the logical center if the
fingers move unevenly. Such a movement is supported by libinput, it is
merely left out of the illustration.�h]�h9��The illustration above shows a basic pinch in the left image and a rotate in
the right angle. Not shown is a movement of the logical center if the
fingers move unevenly. Such a movement is supported by libinput, it is
merely left out of the illustration.�����}�(hj}  hj{  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK/hj-  hhubhE)��}�(h��Note that while position and angle is relative to the previous event, the
scale is always absolute and a multiplier of the initial finger position's
scale.�h]�h9��Note that while position and angle is relative to the previous event, the
scale is always absolute and a multiplier of the initial finger position’s
scale.�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK4hj-  hhubh)��}�(h�.. _gestures_swipe:�h]�h}�(h]�h]�h]�h]�h]�h*�gestures-swipe�uhhhK=hj-  hhhh,ubeh}�(h]�(�pinch-gestures�j  eh]�h]�(�pinch gestures��gestures_pinch�eh]�h]�uhh-hh/hhhh,hKj)  }�j�  j  sj+  }�j  j  subh.)��}�(hhh]�(h3)��}�(h�Swipe gestures�h]�h9�Swipe gestures�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj�  hhhh,hK<ubhE)��}�(hXb  Swipe gestures are executed when three or more fingers are moved
synchronously in the same direction. libinput provides x and y coordinates
in the gesture and thus allows swipe gestures in any direction, including
the tracing of complex paths. It is up to the caller to interpret the
gesture into an action or limit a gesture to specific directions only.�h]�h9Xb  Swipe gestures are executed when three or more fingers are moved
synchronously in the same direction. libinput provides x and y coordinates
in the gesture and thus allows swipe gestures in any direction, including
the tracing of complex paths. It is up to the caller to interpret the
gesture into an action or limit a gesture to specific directions only.�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK>hj�  hhubjM  )��}�(hhh]�(jR  )��}�(h�J.. figure:: swipe-gestures.svg
    :align: center

    The swipe gestures
�h]�h}�(h]�h]�h]�h]�h]��uri��swipe-gestures.svg�j_  }�ja  j�  suhjQ  hj�  hh,hKGubjc  )��}�(h�The swipe gestures�h]�h9�The swipe gestures�����}�(hj�  hj�  ubah}�(h]�h]�h]�h]�h]�uhjb  hh,hKGhj�  ubeh}�(h]��id3�ah]�h]�h]�h]�jy  �center�uhjL  hKGhj�  hhhh,ubhE)��}�(h��The illustration above shows a vertical three-finger swipe. The coordinates
provided during the gesture are the movements of the logical center.�h]�h9��The illustration above shows a vertical three-finger swipe. The coordinates
provided during the gesture are the movements of the logical center.�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKIhj�  hhubh)��}�(h�.. _gestures_touchscreens:�h]�h}�(h]�h]�h]�h]�h]�h*�gestures-touchscreens�uhhhKQhj�  hhhh,ubeh}�(h]�(�swipe-gestures�j�  eh]�h]�(�swipe gestures��gestures_swipe�eh]�h]�uhh-hh/hhhh,hK<j)  }�j  j�  sj+  }�j�  j�  subh.)��}�(hhh]�(h3)��}�(h�Touchscreen gestures�h]�h9�Touchscreen gestures�����}�(hj  hj  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj  hhhh,hKPubhE)��}�(h��Touchscreen gestures are **not** interpreted by libinput. Rather, any touch
point is passed to the caller and any interpretation of gestures is up to
the caller or, eventually, the X or Wayland client.�h]�(h9�Touchscreen gestures are �����}�(h�Touchscreen gestures are �hj'  hhhNhNubh�)��}�(h�**not**�h]�h9�not�����}�(hhhj0  ubah}�(h]�h]�h]�h]�h]�uhh�hj'  ubh9�� interpreted by libinput. Rather, any touch
point is passed to the caller and any interpretation of gestures is up to
the caller or, eventually, the X or Wayland client.�����}�(h�� interpreted by libinput. Rather, any touch
point is passed to the caller and any interpretation of gestures is up to
the caller or, eventually, the X or Wayland client.�hj'  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKRhj  hhubhE)��}�(h��Interpreting gestures on a touchscreen requires context that libinput does
not have, such as the location of windows and other virtual objects on the
screen as well as the context of those virtual objects:�h]�h9��Interpreting gestures on a touchscreen requires context that libinput does
not have, such as the location of windows and other virtual objects on the
screen as well as the context of those virtual objects:�����}�(hjK  hjI  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKVhj  hhubjM  )��}�(hhh]�(jR  )��}�(h�i.. figure:: touchscreen-gestures.svg
    :align: center

    Context-sensitivity of touchscreen gestures
�h]�h}�(h]�h]�h]�h]�h]��uri��touchscreen-gestures.svg�j_  }�ja  je  suhjQ  hjW  hh,hK]ubjc  )��}�(h�+Context-sensitivity of touchscreen gestures�h]�h9�+Context-sensitivity of touchscreen gestures�����}�(hji  hjg  ubah}�(h]�h]�h]�h]�h]�uhjb  hh,hK]hjW  ubeh}�(h]��id4�ah]�h]�h]�h]�jy  �center�uhjL  hK]hj  hhhh,ubhE)��}�(hX`  In the above example, the finger movements are identical but in the left
case both fingers are located within the same window, thus suggesting an
attempt to zoom. In the right case  both fingers are located on a window
border, thus suggesting a window movement. libinput has no knowledge of the
window coordinates and thus cannot differentiate the two.�h]�h9X`  In the above example, the finger movements are identical but in the left
case both fingers are located within the same window, thus suggesting an
attempt to zoom. In the right case  both fingers are located on a window
border, thus suggesting a window movement. libinput has no knowledge of the
window coordinates and thus cannot differentiate the two.�����}�(hj  hj}  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK_hj  hhubh)��}�(h�.. _gestures_softbuttons:�h]�h}�(h]�h]�h]�h]�h]�h*�gestures-softbuttons�uhhhKjhj  hhhh,ubeh}�(h]�(�touchscreen-gestures�j
  eh]�h]�(�touchscreen gestures��gestures_touchscreens�eh]�h]�uhh-hh/hhhh,hKPj)  }�j�  j   sj+  }�j
  j   subh.)��}�(hhh]�(h3)��}�(h�&Gestures with enabled software buttons�h]�h9�&Gestures with enabled software buttons�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hj�  hhhh,hKiubhE)��}�(hXR  If the touchpad device is a :ref:`Clickpad <touchpads_buttons_clickpads>`, it
is recommended that a caller switches to :ref:`clickfinger`.
Usually fingers placed in a :ref:`software button area <software_buttons>`
are not considered for gestures, resulting in some gestures to be
interpreted as pointer motion or two-finger scroll events.�h]�(h9�If the touchpad device is a �����}�(h�If the touchpad device is a �hj�  hhhNhNubhQ)��}�(h�-:ref:`Clickpad <touchpads_buttons_clickpads>`�h]�hW)��}�(hj�  h]�h9�Clickpad�����}�(hhhj�  ubah}�(h]�h]�(hb�std��std-ref�eh]�h]�h]�uhhVhj�  ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�j�  �refexplicit��hr�touchpads_buttons_clickpads�hthuhv�uhhPhh,hKkhj�  ubh9�., it
is recommended that a caller switches to �����}�(h�., it
is recommended that a caller switches to �hj�  hhhNhNubhQ)��}�(h�:ref:`clickfinger`�h]�hW)��}�(hj�  h]�h9�clickfinger�����}�(hhhj�  ubah}�(h]�h]�(hb�std��std-ref�eh]�h]�h]�uhhVhj�  ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�j�  �refexplicit��hr�clickfinger�hthuhv�uhhPhh,hKkhj�  ubh9�.
Usually fingers placed in a �����}�(h�.
Usually fingers placed in a �hj�  hhhNhNubhQ)��}�(h�.:ref:`software button area <software_buttons>`�h]�hW)��}�(hj  h]�h9�software button area�����}�(hhhj  ubah}�(h]�h]�(hb�std��std-ref�eh]�h]�h]�uhhVhj  ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�j  �refexplicit��hr�software_buttons�hthuhv�uhhPhh,hKkhj�  ubh9�}
are not considered for gestures, resulting in some gestures to be
interpreted as pointer motion or two-finger scroll events.�����}�(h�}
are not considered for gestures, resulting in some gestures to be
interpreted as pointer motion or two-finger scroll events.�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hKkhj�  hhubjM  )��}�(hhh]�(jR  )��}�(h�w.. figure:: pinch-gestures-softbuttons.svg
    :align: center

    Interference of software buttons and pinch gestures
�h]�h}�(h]�h]�h]�h]�h]��uri��pinch-gestures-softbuttons.svg�j_  }�ja  j8  suhjQ  hj*  hh,hKtubjc  )��}�(h�3Interference of software buttons and pinch gestures�h]�h9�3Interference of software buttons and pinch gestures�����}�(hj<  hj:  ubah}�(h]�h]�h]�h]�h]�uhjb  hh,hKthj*  ubeh}�(h]��id5�ah]�h]�h]�h]�jy  �center�uhjL  hKthj�  hhhh,ubhE)��}�(hX?  In the example above, the software button area is highlighted in red. The
user executes a three-finger pinch gesture, with the thumb remaining in the
software button area. libinput ignores fingers within the software button
areas, the movement of the remaining fingers is thus interpreted as a
two-finger scroll motion.�h]�h9X?  In the example above, the software button area is highlighted in red. The
user executes a three-finger pinch gesture, with the thumb remaining in the
software button area. libinput ignores fingers within the software button
areas, the movement of the remaining fingers is thus interpreted as a
two-finger scroll motion.�����}�(hjR  hjP  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hKvhj�  hhubh)��}�(h�!.. _gestures_twofinger_touchpads:�h]�h}�(h]�h]�h]�h]�h]�h*�gestures-twofinger-touchpads�uhhhK�hj�  hhhh,ubeh}�(h]�(�&gestures-with-enabled-software-buttons�j�  eh]�h]�(�&gestures with enabled software buttons��gestures_softbuttons�eh]�h]�uhh-hh/hhhh,hKij)  }�jo  j�  sj+  }�j�  j�  subh.)��}�(hhh]�(h3)��}�(h� Gestures on two-finger touchpads�h]�h9� Gestures on two-finger touchpads�����}�(hjy  hjw  hhhNhNubah}�(h]�h]�h]�h]�h]�uhh2hjt  hhhh,hK�ubhE)��}�(hXP  As of kernel 4.2, many :ref:`touchpads_touch_partial_mt` provide only two
slots. This affects how gestures can be interpreted. Touchpads with only two
slots can identify two touches by position but can usually tell that there
is a third (or fourth) finger down on the touchpad - without providing
positional information for that finger.�h]�(h9�As of kernel 4.2, many �����}�(h�As of kernel 4.2, many �hj�  hhhNhNubhQ)��}�(h�!:ref:`touchpads_touch_partial_mt`�h]�hW)��}�(hj�  h]�h9�touchpads_touch_partial_mt�����}�(hhhj�  ubah}�(h]�h]�(hb�std��std-ref�eh]�h]�h]�uhhVhj�  ubah}�(h]�h]�h]�h]�h]��reftype��ref��	refdomain�j�  �refexplicit��hr�touchpads_touch_partial_mt�hthuhv�uhhPhh,hK�hj�  ubh9X   provide only two
slots. This affects how gestures can be interpreted. Touchpads with only two
slots can identify two touches by position but can usually tell that there
is a third (or fourth) finger down on the touchpad - without providing
positional information for that finger.�����}�(hX   provide only two
slots. This affects how gestures can be interpreted. Touchpads with only two
slots can identify two touches by position but can usually tell that there
is a third (or fourth) finger down on the touchpad - without providing
positional information for that finger.�hj�  hhhNhNubeh}�(h]�h]�h]�h]�h]�uhhDhh,hK�hjt  hhubhE)��}�(hX~  Touchpoints are assigned in sequential order and only the first two touch
points are trackable. For libinput this produces an ambiguity where it is
impossible to detect whether a gesture is a pinch gesture or a swipe gesture
whenever a user puts the index and middle finger down first. Since the third
finger does not have positional information, it's location cannot be
determined.�h]�h9X�  Touchpoints are assigned in sequential order and only the first two touch
points are trackable. For libinput this produces an ambiguity where it is
impossible to detect whether a gesture is a pinch gesture or a swipe gesture
whenever a user puts the index and middle finger down first. Since the third
finger does not have positional information, it’s location cannot be
determined.�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK�hjt  hhubjM  )��}�(hhh]�(jR  )��}�(h�y.. figure:: gesture-2fg-ambiguity.svg
    :align: center

    Ambiguity of three-finger gestures on two-finger touchpads
�h]�h}�(h]�h]�h]�h]�h]��uri��gesture-2fg-ambiguity.svg�j_  }�ja  j�  suhjQ  hj�  hh,hK�ubjc  )��}�(h�:Ambiguity of three-finger gestures on two-finger touchpads�h]�h9�:Ambiguity of three-finger gestures on two-finger touchpads�����}�(hj�  hj�  ubah}�(h]�h]�h]�h]�h]�uhjb  hh,hK�hj�  ubeh}�(h]��id6�ah]�h]�h]�h]�jy  �center�uhjL  hK�hjt  hhhh,ubhE)��}�(hX  The image above illustrates this ambiguity. The index and middle finger are
set down first, the data stream from both finger positions looks identical.
In this case, libinput assumes the fingers are in a horizontal arrangement
(the right image above) and use a swipe gesture.�h]�h9X  The image above illustrates this ambiguity. The index and middle finger are
set down first, the data stream from both finger positions looks identical.
In this case, libinput assumes the fingers are in a horizontal arrangement
(the right image above) and use a swipe gesture.�����}�(hj�  hj�  hhhNhNubah}�(h]�h]�h]�h]�h]�uhhDhh,hK�hjt  hhubeh}�(h]�(� gestures-on-two-finger-touchpads�jh  eh]�h]�(� gestures on two-finger touchpads��gestures_twofinger_touchpads�eh]�h]�uhh-hh/hhhh,hK�j)  }�j�  j^  sj+  }�jh  j^  subeh}�(h]�(h+�id1�eh]�h]��gestures�ah]��gestures�ah]�uhh-hhhhhh,hK�
referenced�Kj)  }�j	  h sj+  }�h+h subeh}�(h]�h]�h]�h]�h]��source�h,uhh�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(h2N�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j3  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}�(�git_version�h �substitution_definition���)��}�(h�,.. |git_version| replace:: :commit:`d2f4222`�h]�h �	reference���)��}�(h�git commit d2f4222�h]�h9�git commit d2f4222�����}�(hhhjs  ubah}�(h]�h]�h]�h]�h]��internal���refuri��?https://gitlab.freedesktop.org/libinput/libinput/commit/d2f4222�uhjq  hjm  ubah}�(h]�h]�h]�jj  ah]�h]�uhjk  h�<rst_prolog>�hKhhub�git_version_full�jl  )��}�(h�].. |git_version_full| replace:: :commit:`<function get_git_version_full at 0x7f7878870ea0>`

�h]�jr  )��}�(h�<git commit <function get_git_version_full at 0x7f7878870ea0>�h]�h9�<git commit <function get_git_version_full at 0x7f7878870ea0>�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]��internal���refuri��ihttps://gitlab.freedesktop.org/libinput/libinput/commit/<function get_git_version_full at 0x7f7878870ea0>�uhjq  hj�  ubah}�(h]�h]�h]�j�  ah]�h]�uhjk  hj�  hKhhubu�substitution_names�}�(�git_version�jj  �git_version_full�j�  u�refnames�}��refids�}�(h+]�h ah�]�h�aj  ]�j  aj�  ]�j�  aj
  ]�j   aj�  ]�j�  ajh  ]�j^  au�nameids�}�(j	  h+j&  h�j%  j"  j�  j  j�  j�  j  j�  j  j  j�  j
  j�  j�  jo  j�  jn  jk  j�  jh  j�  j�  u�	nametypes�}�(j	  �j&  �j%  Nj�  �j�  Nj  �j  Nj�  �j�  Njo  �jn  Nj�  �j�  Nuh}�(h+h/j  h/h�h�j"  h�j  j-  j�  j-  j�  j�  j  j�  j
  j  j�  j  j�  j�  jk  j�  jh  jt  j�  jt  jt  jN  j�  j�  jw  jW  jJ  j*  j�  j�  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]�h �system_message���)��}�(hhh]�hE)��}�(h�+Duplicate implicit target name: "gestures".�h]�h9�/Duplicate implicit target name: “gestures”.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubah}�(h]�h]�h]�h]�h]�j  a�level�K�type��INFO��source�h,�line�Kuhj�  hh/hhhh,hKuba�transform_messages�]�(j�  )��}�(hhh]�hE)��}�(hhh]�h9�.Hyperlink target "gestures" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h,�line�Kuhj�  ubj�  )��}�(hhh]�hE)��}�(hhh]�h9�7Hyperlink target "gestures-lifetime" is not referenced.�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]�uhhDhj
  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h,�line�Kuhj�  ubj�  )��}�(hhh]�hE)��}�(hhh]�h9�4Hyperlink target "gestures-pinch" is not referenced.�����}�(hhhj'  ubah}�(h]�h]�h]�h]�h]�uhhDhj$  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h,�line�K uhj�  ubj�  )��}�(hhh]�hE)��}�(hhh]�h9�4Hyperlink target "gestures-swipe" is not referenced.�����}�(hhhjA  ubah}�(h]�h]�h]�h]�h]�uhhDhj>  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h,�line�K=uhj�  ubj�  )��}�(hhh]�hE)��}�(hhh]�h9�;Hyperlink target "gestures-touchscreens" is not referenced.�����}�(hhhj[  ubah}�(h]�h]�h]�h]�h]�uhhDhjX  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h,�line�KQuhj�  ubj�  )��}�(hhh]�hE)��}�(hhh]�h9�:Hyperlink target "gestures-softbuttons" is not referenced.�����}�(hhhju  ubah}�(h]�h]�h]�h]�h]�uhhDhjr  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h,�line�Kjuhj�  ubj�  )��}�(hhh]�hE)��}�(hhh]�h9�BHyperlink target "gestures-twofinger-touchpads" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhhDhj�  ubah}�(h]�h]�h]�h]�h]��level�K�type�j�  �source�h,�line�K�uhj�  ube�transformer�N�
decoration�Nhhub.