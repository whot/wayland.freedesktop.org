���I      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �block_quote���)��}�(hhh]�(h �substitution_definition���)��}�(h�,.. |git_version| replace:: :commit:`e2e8db1`�h]�h �	reference���)��}�(h�git commit e2e8db1�h]�h �Text����git commit e2e8db1�����}�(hh�parent�huba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]��internal���refuri��?https://gitlab.freedesktop.org/libinput/libinput/commit/e2e8db1�u�tagname�hh hubah!}�(h#]�h%]�h']��git_version�ah)]�h+]�uh0h�source��<rst_prolog>��line�Kh hubh)��}�(h�].. |git_version_full| replace:: :commit:`<function get_git_version_full at 0x7f342f19c2f0>`

�h]�h)��}�(h�<git commit <function get_git_version_full at 0x7f342f19c2f0>�h]�h�<git commit <function get_git_version_full at 0x7f342f19c2f0>�����}�(hhh h?ubah!}�(h#]�h%]�h']�h)]�h+]��internal���refuri��ihttps://gitlab.freedesktop.org/libinput/libinput/commit/<function get_git_version_full at 0x7f342f19c2f0>�uh0hh h;ubah!}�(h#]�h%]�h']��git_version_full�ah)]�h+]�uh0hh8h9h:Kh hubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h	h hhhh8Nh:Nubh �target���)��}�(h�.. _t440_support:�h]�h!}�(h#]�h%]�h']�h)]�h+]��refid��t440-support�uh0h]h:Kh hhhh8�8/home/whot/code/libinput/build/doc/user/t440-support.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�#Lenovo \*40 series touchpad support�h]�h�"Lenovo *40 series touchpad support�����}�(h�#Lenovo \*40 series touchpad support�h hshhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh hnhhh8hkh:Kubh �	paragraph���)��}�(h�TThe Lenovo \*40 series emulates trackstick buttons on the top part of the
touchpads.�h]�h�SThe Lenovo *40 series emulates trackstick buttons on the top part of the
touchpads.�����}�(h�TThe Lenovo \*40 series emulates trackstick buttons on the top part of the
touchpads.�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh hnhhubh^)��}�(h�.. _t440_support_overview:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�t440-support-overview�uh0h]h:Kh hnhhh8hkubhm)��}�(hhh]�(hr)��}�(h�Overview�h]�h�Overview�����}�(hh�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh h�hhh8hkh:Kubh�)��}�(hX�  The Lenovo \*40 series introduced a new type of touchpad. Previously, all
laptops had a separate set of physical buttons for the
`trackstick <http://en.wikipedia.org/wiki/Pointing_stick>`_. This
series removed these buttons, relying on a software emulation of the top
section of the touchpad. This is visually marked on the trackpad itself,
and clicks can be triggered by pressing the touchpad down with a finger in
the respective area:�h]�(h��The Lenovo *40 series introduced a new type of touchpad. Previously, all
laptops had a separate set of physical buttons for the
�����}�(h��The Lenovo \*40 series introduced a new type of touchpad. Previously, all
laptops had a separate set of physical buttons for the
�h h�hhh8Nh:Nubh)��}�(h�;`trackstick <http://en.wikipedia.org/wiki/Pointing_stick>`_�h]�h�
trackstick�����}�(hhh h�ubah!}�(h#]�h%]�h']�h)]�h+]��name��
trackstick��refuri��+http://en.wikipedia.org/wiki/Pointing_stick�uh0hh h�ubh^)��}�(h�. <http://en.wikipedia.org/wiki/Pointing_stick>�h]�h!}�(h#]��
trackstick�ah%]�h']��
trackstick�ah)]�h+]��refuri�h�uh0h]�
referenced�Kh h�ubh��. This
series removed these buttons, relying on a software emulation of the top
section of the touchpad. This is visually marked on the trackpad itself,
and clicks can be triggered by pressing the touchpad down with a finger in
the respective area:�����}�(h��. This
series removed these buttons, relying on a software emulation of the top
section of the touchpad. This is visually marked on the trackpad itself,
and clicks can be triggered by pressing the touchpad down with a finger in
the respective area:�h h�hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh h�hhubh �figure���)��}�(hhh]�(h �image���)��}�(h��.. figure:: top-software-buttons.svg
    :align: center

    Left, right and middle-button click with top software button areas
�h]�h!}�(h#]�h%]�h']�h)]�h+]��uri��top-software-buttons.svg��
candidates�}��*�h�suh0h�h h�h8hkh:Kubh �caption���)��}�(h�BLeft, right and middle-button click with top software button areas�h]�h�BLeft, right and middle-button click with top software button areas�����}�(hh�h h�ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh h�ubeh!}�(h#]��id1�ah%]�h']�h)]�h+]��align��center�uh0h�h:Kh h�hhh8hkubh�)��}�(h��This page only covers the top software buttons, the bottom button behavior
is covered in :ref:`Clickpad software buttons <clickpad_softbuttons>`.�h]�(h�YThis page only covers the top software buttons, the bottom button behavior
is covered in �����}�(h�YThis page only covers the top software buttons, the bottom button behavior
is covered in �h j  hhh8Nh:Nub�sphinx.addnodes��pending_xref���)��}�(h�7:ref:`Clickpad software buttons <clickpad_softbuttons>`�h]�h �inline���)��}�(hj   h]�h�Clickpad software buttons�����}�(hhh j$  ubah!}�(h#]�h%]�(�xref��std��std-ref�eh']�h)]�h+]�uh0j"  h j  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�j/  �refexplicit���	reftarget��clickpad_softbuttons��refdoc��t440-support��refwarn��uh0j  h8hkh:Kh j  ubh�.�����}�(h�.�h j  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh h�hhubh�)��}�(h��Clickpads with a top button area are marked with the
`INPUT_PROP_TOPBUTTONPAD <https://www.kernel.org/doc/Documentation/input/event-codes.txt>`_
property.�h]�(h�5Clickpads with a top button area are marked with the
�����}�(h�5Clickpads with a top button area are marked with the
�h jN  hhh8Nh:Nubh)��}�(h�[`INPUT_PROP_TOPBUTTONPAD <https://www.kernel.org/doc/Documentation/input/event-codes.txt>`_�h]�h�INPUT_PROP_TOPBUTTONPAD�����}�(hhh jW  ubah!}�(h#]�h%]�h']�h)]�h+]��name��INPUT_PROP_TOPBUTTONPAD�hȌ>https://www.kernel.org/doc/Documentation/input/event-codes.txt�uh0hh jN  ubh^)��}�(h�A <https://www.kernel.org/doc/Documentation/input/event-codes.txt>�h]�h!}�(h#]��input-prop-topbuttonpad�ah%]�h']��input_prop_topbuttonpad�ah)]�h+]��refuri�jg  uh0h]h�Kh jN  ubh�

property.�����}�(h�

property.�h jN  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K h h�hhubh^)��}�(h�.. _t440_support_btn_size:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�t440-support-btn-size�uh0h]h:K)h h�hhh8hkubeh!}�(h#]�(�overview�h�eh%]�h']�(�overview��t440_support_overview�eh)]�h+]�uh0hlh hnhhh8hkh:K�expect_referenced_by_name�}�j�  h�s�expect_referenced_by_id�}�h�h�subhm)��}�(hhh]�(hr)��}�(h�Size of the buttons�h]�h�Size of the buttons�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j�  hhh8hkh:K(ubh�)��}�(h��The size of the buttons matches the visual markings on this touchpad.
The width of the left and right buttons is approximately 42% of the
touchpad's width, the middle button is centered and assigned 16% of the
touchpad width.�h]�h��The size of the buttons matches the visual markings on this touchpad.
The width of the left and right buttons is approximately 42% of the
touchpad’s width, the middle button is centered and assigned 16% of the
touchpad width.�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K*h j�  hhubh�)��}�(h��The line of the buttons is 5mm from the top edge of the touchpad,
measurements of button presses showed that the size of the buttons needs to
be approximately 10mm high to work reliable (especially when using the
thumb to press the button).�h]�h��The line of the buttons is 5mm from the top edge of the touchpad,
measurements of button presses showed that the size of the buttons needs to
be approximately 10mm high to work reliable (especially when using the
thumb to press the button).�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K/h j�  hhubh^)��}�(h�.. _t440_support_btn_behavior:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�t440-support-btn-behavior�uh0h]h:K9h j�  hhh8hkubeh!}�(h#]�(�size-of-the-buttons�j�  eh%]�h']�(�size of the buttons��t440_support_btn_size�eh)]�h+]�uh0hlh hnhhh8hkh:K(j�  }�j�  j�  sj�  }�j�  j�  subhm)��}�(hhh]�(hr)��}�(h�Button behavior�h]�h�Button behavior�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j�  hhh8hkh:K8ubh�)��}�(hX+  Movement in the top button area does not generate pointer movement. These
buttons are not replacement buttons for the bottom button area but have
their own behavior. Semantically attached to the trackstick device, libinput
re-routes events from these buttons to appear through the trackstick device.�h]�hX+  Movement in the top button area does not generate pointer movement. These
buttons are not replacement buttons for the bottom button area but have
their own behavior. Semantically attached to the trackstick device, libinput
re-routes events from these buttons to appear through the trackstick device.�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K:h j�  hhub�sphinx.ext.graphviz��graphviz���)��}�(hhh]�h!}�(h#]�h%]�h']�h)]�h+]��code�X�  digraph top_button_routing
{
    rankdir="LR";
    node [shape="box";]

    trackstick [label="trackstick kernel device"];
    touchpad [label="touchpad kernel device"];

    subgraph cluster0 {
            bgcolor = floralwhite
            label = "libinput"

            libinput_ts [label="trackstick libinput_device"
                         style=filled
                         fillcolor=white];
            libinput_tp [label="touchpad libinput_device"
                         style=filled
                         fillcolor=white];

            libinput_tp -> libinput_ts [constraint=false
                                        color="red4"];
    }

    trackstick -> libinput_ts [arrowhead="none"]
    touchpad -> libinput_tp [color="red4"]

    events_tp [label="other touchpad events"];
    events_topbutton [label="top software button events"];

    libinput_tp -> events_tp [arrowhead="none"]
    libinput_ts -> events_topbutton [color="red4"]
}��options�}�uh0j�  h j�  hhh8hkh:Keubh�)��}�(hX�  The top button areas work even if the touchpad is disabled but will be
disabled when the trackstick device is disabled. If the finger starts inside
the top area and moves outside the button area the finger is treated as dead
and must be lifted to generate future buttons.  Likewise, movement into the
top button area does not trigger button events, a click has to start inside
this area to take effect.�h]�hX�  The top button areas work even if the touchpad is disabled but will be
disabled when the trackstick device is disabled. If the finger starts inside
the top area and moves outside the button area the finger is treated as dead
and must be lifted to generate future buttons.  Likewise, movement into the
top button area does not trigger button events, a click has to start inside
this area to take effect.�����}�(hj  h j
  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kfh j�  hhubh^)��}�(h� .. _t440_support_identification:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�t440-support-identification�uh0h]h:Krh j�  hhh8hkubeh!}�(h#]�(�button-behavior�j�  eh%]�h']�(�button behavior��t440_support_btn_behavior�eh)]�h+]�uh0hlh hnhhh8hkh:K8j�  }�j)  j�  sj�  }�j�  j�  subhm)��}�(hhh]�(hr)��}�(h�Kernel support�h]�h�Kernel support�����}�(hj3  h j1  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j.  hhh8hkh:Kqubh�)��}�(hX  The firmware on the first generation of touchpads providing top software
buttons is buggy and announces wrong ranges.
`Kernel patches <https://lkml.org/lkml/2014/3/7/722>`_ are required;
these fixes are available in kernels 3.14.1, 3.15 and later but each
touchpad needs a separate fix.�h]�(h�vThe firmware on the first generation of touchpads providing top software
buttons is buggy and announces wrong ranges.
�����}�(h�vThe firmware on the first generation of touchpads providing top software
buttons is buggy and announces wrong ranges.
�h j?  hhh8Nh:Nubh)��}�(h�6`Kernel patches <https://lkml.org/lkml/2014/3/7/722>`_�h]�h�Kernel patches�����}�(hhh jH  ubah!}�(h#]�h%]�h']�h)]�h+]��name��Kernel patches�hȌ"https://lkml.org/lkml/2014/3/7/722�uh0hh j?  ubh^)��}�(h�% <https://lkml.org/lkml/2014/3/7/722>�h]�h!}�(h#]��kernel-patches�ah%]�h']��kernel patches�ah)]�h+]��refuri�jX  uh0h]h�Kh j?  ubh�r are required;
these fixes are available in kernels 3.14.1, 3.15 and later but each
touchpad needs a separate fix.�����}�(h�r are required;
these fixes are available in kernels 3.14.1, 3.15 and later but each
touchpad needs a separate fix.�h j?  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Ksh j.  hhubh�)��}�(hX  The October 2014 refresh of these laptops do not have this firmware bug
anymore and should work without per-device patches, though
`this kernel commit <http://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=02e07492cdfae9c86e3bd21c0beec88dbcc1e9e8>`_
is required.�h]�(h��The October 2014 refresh of these laptops do not have this firmware bug
anymore and should work without per-device patches, though
�����}�(h��The October 2014 refresh of these laptops do not have this firmware bug
anymore and should work without per-device patches, though
�h jq  hhh8Nh:Nubh)��}�(h��`this kernel commit <http://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=02e07492cdfae9c86e3bd21c0beec88dbcc1e9e8>`_�h]�h�this kernel commit�����}�(hhh jz  ubah!}�(h#]�h%]�h']�h)]�h+]��name��this kernel commit�hȌrhttp://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=02e07492cdfae9c86e3bd21c0beec88dbcc1e9e8�uh0hh jq  ubh^)��}�(h�u <http://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=02e07492cdfae9c86e3bd21c0beec88dbcc1e9e8>�h]�h!}�(h#]��this-kernel-commit�ah%]�h']��this kernel commit�ah)]�h+]��refuri�j�  uh0h]h�Kh jq  ubh�
is required.�����}�(h�
is required.�h jq  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kyh j.  hhubh�)��}�(h��For a complete list of supported touchpads check
`the kernel source <http://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/tree/drivers/input/mouse/synaptics.c>`_
(search for "topbuttonpad_pnp_ids").�h]�(h�1For a complete list of supported touchpads check
�����}�(h�1For a complete list of supported touchpads check
�h j�  hhh8Nh:Nubh)��}�(h�z`the kernel source <http://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/tree/drivers/input/mouse/synaptics.c>`_�h]�h�the kernel source�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]��name��the kernel source�hȌchttp://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/tree/drivers/input/mouse/synaptics.c�uh0hh j�  ubh^)��}�(h�f <http://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/tree/drivers/input/mouse/synaptics.c>�h]�h!}�(h#]��the-kernel-source�ah%]�h']��the kernel source�ah)]�h+]��refuri�j�  uh0h]h�Kh j�  ubh�)
(search for “topbuttonpad_pnp_ids”).�����}�(h�%
(search for "topbuttonpad_pnp_ids").�h j�  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K~h j.  hhubeh!}�(h#]�(�kernel-support�j"  eh%]�h']�(�kernel support��t440_support_identification�eh)]�h+]�uh0hlh hnhhh8hkh:Kqj�  }�j�  j  sj�  }�j"  j  subeh!}�(h#]�(�!lenovo-40-series-touchpad-support�hjeh%]�h']�(�"lenovo *40 series touchpad support��t440_support�eh)]�h+]�uh0hlh hhhh8hkh:Kj�  }�j�  h_sj�  }�hjh_subeh!}�(h#]�h%]�h']�h)]�h+]��source�hkuh0h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hqN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�hk�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�N�gettext_compact��ub�reporter�N�indirect_targets�]��substitution_defs�}�(h5hhTh;u�substitution_names�}�(�git_version�h5�git_version_full�hTu�refnames�}��refids�}�(hj]�h_ah�]�h�aj�  ]�j�  aj�  ]�j�  aj"  ]�j  au�nameids�}�(j�  hjj�  j�  j�  h�j�  j�  h�h�jq  jn  j�  j�  j�  j�  j)  j�  j(  j%  j�  j"  j�  j�  jb  j_  j�  j�  j�  j�  u�	nametypes�}�(j�  �j�  Nj�  �j�  Nhӈjq  �j�  �j�  Nj)  �j(  Nj�  �j�  Njb  �j�  �j�  �uh#}�(hjhnj�  hnh�h�j�  h�h�h�jn  jh  j�  j�  j�  j�  j�  j�  j%  j�  j"  j.  j�  j.  j_  jY  j�  j�  j�  j�  j  h�u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]��transform_messages�]�(h �system_message���)��}�(hhh]�h�)��}�(hhh]�h�2Hyperlink target "t440-support" is not referenced.�����}�(hhh jt  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h jq  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type��INFO��source�hk�line�Kuh0jo  ubjp  )��}�(hhh]�h�)��}�(hhh]�h�;Hyperlink target "t440-support-overview" is not referenced.�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j�  �source�hk�line�Kuh0jo  ubjp  )��}�(hhh]�h�)��}�(hhh]�h�;Hyperlink target "t440-support-btn-size" is not referenced.�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j�  �source�hk�line�K)uh0jo  ubjp  )��}�(hhh]�h�)��}�(hhh]�h�?Hyperlink target "t440-support-btn-behavior" is not referenced.�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j�  �source�hk�line�K9uh0jo  ubjp  )��}�(hhh]�h�)��}�(hhh]�h�AHyperlink target "t440-support-identification" is not referenced.�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j�  �source�hk�line�Kruh0jo  ube�transformer�N�
decoration�Nhhub.