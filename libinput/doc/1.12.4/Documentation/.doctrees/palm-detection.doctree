����      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �block_quote���)��}�(hhh]�(h �substitution_definition���)��}�(h�,.. |git_version| replace:: :commit:`e2e8db1`�h]�h �	reference���)��}�(h�git commit e2e8db1�h]�h �Text����git commit e2e8db1�����}�(hh�parent�huba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]��internal���refuri��?https://gitlab.freedesktop.org/libinput/libinput/commit/e2e8db1�u�tagname�hh hubah!}�(h#]�h%]�h']��git_version�ah)]�h+]�uh0h�source��<rst_prolog>��line�Kh hubh)��}�(h�].. |git_version_full| replace:: :commit:`<function get_git_version_full at 0x7f342f19c2f0>`

�h]�h)��}�(h�<git commit <function get_git_version_full at 0x7f342f19c2f0>�h]�h�<git commit <function get_git_version_full at 0x7f342f19c2f0>�����}�(hhh h?ubah!}�(h#]�h%]�h']�h)]�h+]��internal���refuri��ihttps://gitlab.freedesktop.org/libinput/libinput/commit/<function get_git_version_full at 0x7f342f19c2f0>�uh0hh h;ubah!}�(h#]�h%]�h']��git_version_full�ah)]�h+]�uh0hh8h9h:Kh hubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h	h hhhh8Nh:Nubh �target���)��}�(h�.. _palm_detection:�h]�h!}�(h#]�h%]�h']�h)]�h+]��refid��palm-detection�uh0h]h:Kh hhhh8�:/home/whot/code/libinput/build/doc/user/palm-detection.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�Palm detection�h]�h�Palm detection�����}�(hhuh hshhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh hnhhh8hkh:Kubh �	paragraph���)��}�(h��Palm detection tries to identify accidental touches while typing, while
using the trackpoint and/or during general use of the touchpad area.�h]�h��Palm detection tries to identify accidental touches while typing, while
using the trackpoint and/or during general use of the touchpad area.�����}�(hh�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh hnhhubh�)��}�(hX!  On most laptops typing on the keyboard generates accidental touches on the
touchpad with the palm (usually the area below the thumb). This can lead to
cursor jumps or accidental clicks. On large touchpads, the palm may also
touch the bottom edges of the touchpad during normal interaction.�h]�hX!  On most laptops typing on the keyboard generates accidental touches on the
touchpad with the palm (usually the area below the thumb). This can lead to
cursor jumps or accidental clicks. On large touchpads, the palm may also
touch the bottom edges of the touchpad during normal interaction.�����}�(hh�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K
h hnhhubh�)��}�(hXX  Interference from a palm depends on the size of the touchpad and the position
of the user's hand. Data from touchpads showed that almost all palm events
during tying on a Lenovo T440 happened in the left-most and right-most 5% of
the touchpad. The T440 series has one of the largest touchpads, other
touchpads are less affected by palm touches.�h]�hXZ  Interference from a palm depends on the size of the touchpad and the position
of the user’s hand. Data from touchpads showed that almost all palm events
during tying on a Lenovo T440 happened in the left-most and right-most 5% of
the touchpad. The T440 series has one of the largest touchpads, other
touchpads are less affected by palm touches.�����}�(hh�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh hnhhubh�)��}�(h�hlibinput has multiple ways of detecting a palm, each of which depends on
hardware-specific capabilities.�h]�h�hlibinput has multiple ways of detecting a palm, each of which depends on
hardware-specific capabilities.�����}�(hh�h h�hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh hnhhubh �bullet_list���)��}�(hhh]�(h �	list_item���)��}�(h�:ref:`palm_tool`�h]�h�)��}�(hh�h]��sphinx.addnodes��pending_xref���)��}�(hh�h]�h �inline���)��}�(hh�h]�h�	palm_tool�����}�(hhh h�ubah!}�(h#]�h%]�(�xref��std��std-ref�eh']�h)]�h+]�uh0h�h h�ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�h܌refexplicit���	reftarget��	palm_tool��refdoc��palm-detection��refwarn��uh0h�h8hkh:Kh h�ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh h�ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h h�hhh8hkh:Nubh�)��}�(h�:ref:`palm_pressure`�h]�h�)��}�(hh�h]�h�)��}�(hh�h]�h�)��}�(hh�h]�h�palm_pressure�����}�(hhh j  ubah!}�(h#]�h%]�(hیstd��std-ref�eh']�h)]�h+]�uh0h�h j  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�j  �refexplicit��h�palm_pressure�h�h�h�uh0h�h8hkh:Kh j   ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh h�ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h h�hhh8hkh:Nubh�)��}�(h�:ref:`palm_touch_size`�h]�h�)��}�(hj.  h]�h�)��}�(hj.  h]�h�)��}�(hj.  h]�h�palm_touch_size�����}�(hhh j6  ubah!}�(h#]�h%]�(hیstd��std-ref�eh']�h)]�h+]�uh0h�h j3  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�j@  �refexplicit��h�palm_touch_size�h�h�h�uh0h�h8hkh:Kh j0  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh j,  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h h�hhh8hkh:Nubh�)��}�(h�:ref:`palm_exclusion_zones`�h]�h�)��}�(hj^  h]�h�)��}�(hj^  h]�h�)��}�(hj^  h]�h�palm_exclusion_zones�����}�(hhh jf  ubah!}�(h#]�h%]�(hیstd��std-ref�eh']�h)]�h+]�uh0h�h jc  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�jp  �refexplicit��h�palm_exclusion_zones�h�h�h�uh0h�h8hkh:Kh j`  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh j\  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h h�hhh8hkh:Nubh�)��}�(h�:ref:`trackpoint-disabling`�h]�h�)��}�(hj�  h]�h�)��}�(hj�  h]�h�)��}�(hj�  h]�h�trackpoint-disabling�����}�(hhh j�  ubah!}�(h#]�h%]�(hیstd��std-ref�eh']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�j�  �refexplicit��h�trackpoint-disabling�h�h�h�uh0h�h8hkh:Kh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h h�hhh8hkh:Nubh�)��}�(h�:ref:`disable-while-typing`�h]�h�)��}�(hj�  h]�h�)��}�(hj�  h]�h�)��}�(hj�  h]�h�disable-while-typing�����}�(hhh j�  ubah!}�(h#]�h%]�(hیstd��std-ref�eh']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�j�  �refexplicit��h�disable-while-typing�h�h�h�uh0h�h8hkh:Kh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h h�hhh8hkh:Nubh�)��}�(h� :ref:`stylus-touch-arbitration`
�h]�h�)��}�(h�:ref:`stylus-touch-arbitration`�h]�h�)��}�(hj�  h]�h�)��}�(hj�  h]�h�stylus-touch-arbitration�����}�(hhh j�  ubah!}�(h#]�h%]�(hیstd��std-ref�eh']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�j  �refexplicit��h�stylus-touch-arbitration�h�h�h�uh0h�h8hkh:Kh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h h�hhh8hkh:Nubeh!}�(h#]�h%]�h']�h)]�h+]��bullet��-�uh0h�h8hkh:Kh hnhhubh�)��}�(h�MPalm detection is always enabled, with the exception of
disable-while-typing.�h]�h�MPalm detection is always enabled, with the exception of
disable-while-typing.�����}�(hj'  h j%  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K h hnhhubh^)��}�(h�.. _palm_tool:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�	palm-tool�uh0h]h:K(h hnhhh8hkubhm)��}�(hhh]�(hr)��}�(h�*Palm detection based on firmware labelling�h]�h�*Palm detection based on firmware labelling�����}�(hjC  h jA  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j>  hhh8hkh:K'ubh�)��}�(h��Some devices provide palm detection in the firmware, forwarded by the kernel
as the ``EV_ABS/ABS_MT_TOOL`` axis with a value of ``MT_TOOL_PALM``
(whenever a palm is detected). libinput honors that value and switches that
touch to a palm.�h]�(h�TSome devices provide palm detection in the firmware, forwarded by the kernel
as the �����}�(h�TSome devices provide palm detection in the firmware, forwarded by the kernel
as the �h jO  hhh8Nh:Nubh �literal���)��}�(h�``EV_ABS/ABS_MT_TOOL``�h]�h�EV_ABS/ABS_MT_TOOL�����}�(hhh jZ  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0jX  h jO  ubh� axis with a value of �����}�(h� axis with a value of �h jO  hhh8Nh:NubjY  )��}�(h�``MT_TOOL_PALM``�h]�h�MT_TOOL_PALM�����}�(hhh jm  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0jX  h jO  ubh�]
(whenever a palm is detected). libinput honors that value and switches that
touch to a palm.�����}�(h�]
(whenever a palm is detected). libinput honors that value and switches that
touch to a palm.�h jO  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K)h j>  hhubh^)��}�(h�.. _palm_pressure:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�palm-pressure�uh0h]h:K3h j>  hhh8hkubeh!}�(h#]�(�*palm-detection-based-on-firmware-labelling�j=  eh%]�h']�(�*palm detection based on firmware labelling��	palm_tool�eh)]�h+]�uh0hlh hnhhh8hkh:K'�expect_referenced_by_name�}�j�  j3  s�expect_referenced_by_id�}�j=  j3  subhm)��}�(hhh]�(hr)��}�(h� Palm detection based on pressure�h]�h� Palm detection based on pressure�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j�  hhh8hkh:K2ubh�)��}�(hX�  The simplest form of palm detection labels a touch as palm when the pressure
value goes above a certain threshold. This threshold is usually high enough
that it cannot be triggered by a finger movement. One a touch is labelled as
palm based on pressure, it will remain so even if the pressure drops below
the threshold again. This ensures that a palm remains a palm even when the
pressure changes as the user is typing.�h]�hX�  The simplest form of palm detection labels a touch as palm when the pressure
value goes above a certain threshold. This threshold is usually high enough
that it cannot be triggered by a finger movement. One a touch is labelled as
palm based on pressure, it will remain so even if the pressure drops below
the threshold again. This ensures that a palm remains a palm even when the
pressure changes as the user is typing.�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K4h j�  hhubh�)��}�(h�vFor some information on how to detect pressure on a touch and debug the
pressure ranges, see :ref:`touchpad_pressure`.�h]�(h�]For some information on how to detect pressure on a touch and debug the
pressure ranges, see �����}�(h�]For some information on how to detect pressure on a touch and debug the
pressure ranges, see �h j�  hhh8Nh:Nubh�)��}�(h�:ref:`touchpad_pressure`�h]�h�)��}�(hj�  h]�h�touchpad_pressure�����}�(hhh j�  ubah!}�(h#]�h%]�(hیstd��std-ref�eh']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�j�  �refexplicit��h�touchpad_pressure�h�h�h�uh0h�h8hkh:K;h j�  ubh�.�����}�(h�.�h j�  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K;h j�  hhubh^)��}�(h�.. _palm_touch_size:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�palm-touch-size�uh0h]h:KCh j�  hhh8hkubeh!}�(h#]�(� palm-detection-based-on-pressure�j�  eh%]�h']�(� palm detection based on pressure��palm_pressure�eh)]�h+]�uh0hlh hnhhh8hkh:K2j�  }�j   j�  sj�  }�j�  j�  subhm)��}�(hhh]�(hr)��}�(h�"Palm detection based on touch size�h]�h�"Palm detection based on touch size�����}�(hj
  h j  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j  hhh8hkh:KBubh�)��}�(hX  On touchads that support the ``ABS_MT_TOUCH_MAJOR`` axes, libinput can perform
palm detection based on the size of the touch ellipse. This works similar to
the pressure-based palm detection in that a touch is labelled as palm when
it exceeds the (device-specific) touch size threshold.�h]�(h�On touchads that support the �����}�(h�On touchads that support the �h j  hhh8Nh:NubjY  )��}�(h�``ABS_MT_TOUCH_MAJOR``�h]�h�ABS_MT_TOUCH_MAJOR�����}�(hhh j  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0jX  h j  ubh�� axes, libinput can perform
palm detection based on the size of the touch ellipse. This works similar to
the pressure-based palm detection in that a touch is labelled as palm when
it exceeds the (device-specific) touch size threshold.�����}�(h�� axes, libinput can perform
palm detection based on the size of the touch ellipse. This works similar to
the pressure-based palm detection in that a touch is labelled as palm when
it exceeds the (device-specific) touch size threshold.�h j  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:KDh j  hhubh�)��}�(h�xFor some information on how to detect the size of a touch and debug the
touch size ranges, see :ref:`touchpad_pressure`.�h]�(h�_For some information on how to detect the size of a touch and debug the
touch size ranges, see �����}�(h�_For some information on how to detect the size of a touch and debug the
touch size ranges, see �h j8  hhh8Nh:Nubh�)��}�(h�:ref:`touchpad_pressure`�h]�h�)��}�(hjC  h]�h�touchpad_pressure�����}�(hhh jE  ubah!}�(h#]�h%]�(hیstd��std-ref�eh']�h)]�h+]�uh0h�h jA  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�jO  �refexplicit��h�touchpad_pressure�h�h�h�uh0h�h8hkh:KIh j8  ubh�.�����}�(hj�  h j8  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:KIh j  hhubh^)��}�(h�.. _palm_exclusion_zones:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�palm-exclusion-zones�uh0h]h:KQh j  hhh8hkubeh!}�(h#]�(�"palm-detection-based-on-touch-size�j�  eh%]�h']�(�"palm detection based on touch size��palm_touch_size�eh)]�h+]�uh0hlh hnhhh8hkh:KBj�  }�jz  j�  sj�  }�j�  j�  subhm)��}�(hhh]�(hr)��}�(h�Palm exclusion zones�h]�h�Palm exclusion zones�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j  hhh8hkh:KPubh�)��}�(hX�  libinput enables palm detection on the left, right and top edges of the
touchpad. Two exclusion zones are defined  on the left and right edge of the
touchpad. If a touch starts in the exclusion zone, it is considered a palm
and the touch point is ignored. However, for fast cursor movements across
the screen, it is common for a finger to start inside an exclusion zone and
move rapidly across the touchpad. libinput detects such movements and avoids
palm detection on such touch sequences.�h]�hX�  libinput enables palm detection on the left, right and top edges of the
touchpad. Two exclusion zones are defined  on the left and right edge of the
touchpad. If a touch starts in the exclusion zone, it is considered a palm
and the touch point is ignored. However, for fast cursor movements across
the screen, it is common for a finger to start inside an exclusion zone and
move rapidly across the touchpad. libinput detects such movements and avoids
palm detection on such touch sequences.�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:KRh j  hhubh�)��}�(h��Another exclusion zone is defined on the top edge of the touchpad. As with
the edge zones, libinput detects vertical movements out of the edge zone and
avoids palm detection on such touch sequences.�h]�h��Another exclusion zone is defined on the top edge of the touchpad. As with
the edge zones, libinput detects vertical movements out of the edge zone and
avoids palm detection on such touch sequences.�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:KZh j  hhubh�)��}�(h��Each side edge exclusion zone is divided into a top part and a bottom part.
A touch starting in the top part of the exclusion zone does not trigger a
tap (see :ref:`tapping`).�h]�(h��Each side edge exclusion zone is divided into a top part and a bottom part.
A touch starting in the top part of the exclusion zone does not trigger a
tap (see �����}�(h��Each side edge exclusion zone is divided into a top part and a bottom part.
A touch starting in the top part of the exclusion zone does not trigger a
tap (see �h j�  hhh8Nh:Nubh�)��}�(h�:ref:`tapping`�h]�h�)��}�(hj�  h]�h�tapping�����}�(hhh j�  ubah!}�(h#]�h%]�(hیstd��std-ref�eh']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�j�  �refexplicit��h�tapping�h�h�h�uh0h�h8hkh:K^h j�  ubh�).�����}�(h�).�h j�  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K^h j  hhubh�)��}�(h��In the diagram below, the exclusion zones are painted red.
Touch 'A' starts inside the exclusion zone and moves
almost vertically. It is considered a palm and ignored for cursor movement,
despite moving out of the exclusion zone.�h]�h��In the diagram below, the exclusion zones are painted red.
Touch ‘A’ starts inside the exclusion zone and moves
almost vertically. It is considered a palm and ignored for cursor movement,
despite moving out of the exclusion zone.�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kbh j  hhubh�)��}�(h��Touch 'B' starts inside the exclusion zone but moves horizontally out of the
zone. It is considered a valid touch and controls the cursor.�h]�h��Touch ‘B’ starts inside the exclusion zone but moves horizontally out of the
zone. It is considered a valid touch and controls the cursor.�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kgh j  hhubh�)��}�(hX  Touch 'C' occurs in the top part of the exclusion zone. Despite being a
tapping motion, it does not generate an emulated button event. Touch 'D'
likewise occurs within the exclusion zone but in the bottom half. libinput
will generate a button event for this touch.�h]�hX  Touch ‘C’ occurs in the top part of the exclusion zone. Despite being a
tapping motion, it does not generate an emulated button event. Touch ‘D’
likewise occurs within the exclusion zone but in the bottom half. libinput
will generate a button event for this touch.�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kjh j  hhubh �figure���)��}�(hhh]�h �image���)��}�(h�2.. figure:: palm-detection.svg
    :align: center
�h]�h!}�(h#]�h%]�h']�h)]�h+]��uri��palm-detection.svg��
candidates�}��*�j  suh0j  h j
  h8hkh:Nubah!}�(h#]�h%]�h']�h)]�h+]��align��center�uh0j  h j  hhh8hkh:Nubh^)��}�(h�.. _trackpoint-disabling:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�trackpoint-disabling�uh0h]h:Kwh j  hhh8hkubeh!}�(h#]�(js  �id2�eh%]�h']�(�palm exclusion zones��palm_exclusion_zones�eh)]�h+]�uh0hlh hnhhh8hkh:KPj�  }�j7  ji  sj�  }�js  ji  subhm)��}�(hhh]�(hr)��}�(h�$Palm detection during trackpoint use�h]�h�$Palm detection during trackpoint use�����}�(hjA  h j?  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j<  hhh8hkh:Kvubh�)��}�(h��If a device provides a
`trackpoint <http://en.wikipedia.org/wiki/Pointing_stick>`_, it is
usually located above the touchpad. This increases the likelihood of
accidental touches whenever the trackpoint is used.�h]�(h�If a device provides a
�����}�(h�If a device provides a
�h jM  hhh8Nh:Nubh)��}�(h�;`trackpoint <http://en.wikipedia.org/wiki/Pointing_stick>`_�h]�h�
trackpoint�����}�(hhh jV  ubah!}�(h#]�h%]�h']�h)]�h+]��name��
trackpoint��refuri��+http://en.wikipedia.org/wiki/Pointing_stick�uh0hh jM  ubh^)��}�(h�. <http://en.wikipedia.org/wiki/Pointing_stick>�h]�h!}�(h#]��
trackpoint�ah%]�h']��
trackpoint�ah)]�h+]��refuri�jg  uh0h]�
referenced�Kh jM  ubh��, it is
usually located above the touchpad. This increases the likelihood of
accidental touches whenever the trackpoint is used.�����}�(h��, it is
usually located above the touchpad. This increases the likelihood of
accidental touches whenever the trackpoint is used.�h jM  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:Kxh j<  hhubh�)��}�(hX�  libinput disables the touchpad whenever it detects trackpoint activity for a
certain timeout until after trackpoint activity stops. Touches generated
during this timeout will not move the pointer, and touches started during
this timeout will likewise not move the pointer (allowing for a user to rest
the palm on the touchpad while using the trackstick).
If the touchpad is disabled, the :ref:`top software buttons <t440_support>`
remain enabled.�h]�(hX�  libinput disables the touchpad whenever it detects trackpoint activity for a
certain timeout until after trackpoint activity stops. Touches generated
during this timeout will not move the pointer, and touches started during
this timeout will likewise not move the pointer (allowing for a user to rest
the palm on the touchpad while using the trackstick).
If the touchpad is disabled, the �����}�(hX�  libinput disables the touchpad whenever it detects trackpoint activity for a
certain timeout until after trackpoint activity stops. Touches generated
during this timeout will not move the pointer, and touches started during
this timeout will likewise not move the pointer (allowing for a user to rest
the palm on the touchpad while using the trackstick).
If the touchpad is disabled, the �h j�  hhh8Nh:Nubh�)��}�(h�*:ref:`top software buttons <t440_support>`�h]�h�)��}�(hj�  h]�h�top software buttons�����}�(hhh j�  ubah!}�(h#]�h%]�(hیstd��std-ref�eh']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�j�  �refexplicit��h�t440_support�h�h�h�uh0h�h8hkh:K}h j�  ubh�
remain enabled.�����}�(h�
remain enabled.�h j�  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K}h j<  hhubh^)��}�(h�.. _disable-while-typing:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�disable-while-typing�uh0h]h:K�h j<  hhh8hkubeh!}�(h#]�(�$palm-detection-during-trackpoint-use�j0  eh%]�h']�(�$palm detection during trackpoint use��trackpoint-disabling�eh)]�h+]�uh0hlh hnhhh8hkh:Kvj�  }�j�  j&  sj�  }�j0  j&  subhm)��}�(hhh]�(hr)��}�(h�Disable-while-typing�h]�h�Disable-while-typing�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j�  hhh8hkh:K�ubh�)��}�(hX�  libinput automatically disables the touchpad for a timeout after a key
press, a feature traditionally referred to as "disable while typing" and
previously available through the
`syndaemon(1) <http://linux.die.net/man/1/syndaemon>`_ command. libinput does
not require an external command and the feature is currently enabled for all
touchpads but will be reduced in the future to only apply to touchpads where
finger width or pressure data is unreliable.�h]�(h��libinput automatically disables the touchpad for a timeout after a key
press, a feature traditionally referred to as “disable while typing” and
previously available through the
�����}�(h��libinput automatically disables the touchpad for a timeout after a key
press, a feature traditionally referred to as "disable while typing" and
previously available through the
�h j�  hhh8Nh:Nubh)��}�(h�6`syndaemon(1) <http://linux.die.net/man/1/syndaemon>`_�h]�h�syndaemon(1)�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]��name��syndaemon(1)�jf  �$http://linux.die.net/man/1/syndaemon�uh0hh j�  ubh^)��}�(h�' <http://linux.die.net/man/1/syndaemon>�h]�h!}�(h#]��syndaemon-1�ah%]�h']��syndaemon(1)�ah)]�h+]��refuri�j�  uh0h]ju  Kh j�  ubh�� command. libinput does
not require an external command and the feature is currently enabled for all
touchpads but will be reduced in the future to only apply to touchpads where
finger width or pressure data is unreliable.�����}�(h�� command. libinput does
not require an external command and the feature is currently enabled for all
touchpads but will be reduced in the future to only apply to touchpads where
finger width or pressure data is unreliable.�h j�  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h j�  hhubh�)��}�(h�=Notable behaviors of libinput's disable-while-typing feature:�h]�h�?Notable behaviors of libinput’s disable-while-typing feature:�����}�(hj  h j  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h j�  hhubh�)��}�(hhh]�(h�)��}�(h��Two different timeouts are used, after a single key press the timeout is
short to ensure responsiveness. After multiple key events, the timeout is
longer to avoid accidental pointer manipulation while typing.�h]�h�)��}�(h��Two different timeouts are used, after a single key press the timeout is
short to ensure responsiveness. After multiple key events, the timeout is
longer to avoid accidental pointer manipulation while typing.�h]�h��Two different timeouts are used, after a single key press the timeout is
short to ensure responsiveness. After multiple key events, the timeout is
longer to avoid accidental pointer manipulation while typing.�����}�(hj#  h j!  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h j  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j  hhh8hkh:Nubh�)��}�(h��Some keys do not trigger the timeout, specifically some modifier keys
(Ctrl, Alt, Shift, and Fn). Actions such as Ctrl + click thus stay
responsive.�h]�h�)��}�(h��Some keys do not trigger the timeout, specifically some modifier keys
(Ctrl, Alt, Shift, and Fn). Actions such as Ctrl + click thus stay
responsive.�h]�h��Some keys do not trigger the timeout, specifically some modifier keys
(Ctrl, Alt, Shift, and Fn). Actions such as Ctrl + click thus stay
responsive.�����}�(hj;  h j9  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h j5  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j  hhh8hkh:Nubh�)��}�(h��Touches started while typing do not control the cursor even after typing
has stopped, it is thus possible to rest the palm on the touchpad while
typing.�h]�h�)��}�(h��Touches started while typing do not control the cursor even after typing
has stopped, it is thus possible to rest the palm on the touchpad while
typing.�h]�h��Touches started while typing do not control the cursor even after typing
has stopped, it is thus possible to rest the palm on the touchpad while
typing.�����}�(hjS  h jQ  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h jM  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j  hhh8hkh:Nubh�)��}�(h�zPhysical buttons work even while the touchpad is disabled. This includes
:ref:`software-emulated buttons <t440_support>`.
�h]�h�)��}�(h�yPhysical buttons work even while the touchpad is disabled. This includes
:ref:`software-emulated buttons <t440_support>`.�h]�(h�IPhysical buttons work even while the touchpad is disabled. This includes
�����}�(h�IPhysical buttons work even while the touchpad is disabled. This includes
�h ji  ubh�)��}�(h�/:ref:`software-emulated buttons <t440_support>`�h]�h�)��}�(hjt  h]�h�software-emulated buttons�����}�(hhh jv  ubah!}�(h#]�h%]�(hیstd��std-ref�eh']�h)]�h+]�uh0h�h jr  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�j�  �refexplicit��h�t440_support�h�h�h�uh0h�h8hkh:K�h ji  ubh�.�����}�(hj�  h ji  ubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h je  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j  hhh8hkh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�j#  j$  uh0h�h8hkh:K�h j�  hhubh�)��}�(h�iDisable-while-typing can be enabled and disabled by calling
**libinput_device_config_dwt_set_enabled()**.�h]�(h�<Disable-while-typing can be enabled and disabled by calling
�����}�(h�<Disable-while-typing can be enabled and disabled by calling
�h j�  hhh8Nh:Nubh �strong���)��}�(h�,**libinput_device_config_dwt_set_enabled()**�h]�h�(libinput_device_config_dwt_set_enabled()�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j�  h j�  ubh�.�����}�(hj�  h j�  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h j�  hhubh^)��}�(h�.. _stylus-touch-arbitration:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�stylus-touch-arbitration�uh0h]h:K�h j�  hhh8hkubeh!}�(h#]�(j�  �id3�eh%]�h']��disable-while-typing�ah)]��disable-while-typing�ah+]�uh0hlh hnhhh8hkh:K�ju  Kj�  }�j�  j�  sj�  }�j�  j�  subhm)��}�(hhh]�(hr)��}�(h�Stylus-touch arbitration�h]�h�Stylus-touch arbitration�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh j�  hhh8hkh:K�ubh�)��}�(hX:  A special case of palm detection is touch arbitration on devices that
support styli. When interacting with a stylus on the screen, parts of the
hand may touch the surface and trigger touches. As the user is currently
interacting with the stylus, these touches would interfer with the correct
working of the stylus.�h]�hX:  A special case of palm detection is touch arbitration on devices that
support styli. When interacting with a stylus on the screen, parts of the
hand may touch the surface and trigger touches. As the user is currently
interacting with the stylus, these touches would interfer with the correct
working of the stylus.�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h j�  hhubh�)��}�(h�libinput employs a method similar to :ref:`disable-while-typing` to detect
these touches and disables the touchpad accordingly.�h]�(h�%libinput employs a method similar to �����}�(h�%libinput employs a method similar to �h j�  hhh8Nh:Nubh�)��}�(h�:ref:`disable-while-typing`�h]�h�)��}�(hj	  h]�h�disable-while-typing�����}�(hhh j  ubah!}�(h#]�h%]�(hیstd��std-ref�eh']�h)]�h+]�uh0h�h j  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�j  �refexplicit��h�disable-while-typing�h�h�h�uh0h�h8hkh:K�h j�  ubh�? to detect
these touches and disables the touchpad accordingly.�����}�(h�? to detect
these touches and disables the touchpad accordingly.�h j�  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h j�  hhubh^)��}�(h�.. _thumb-detection:�h]�h!}�(h#]�h%]�h']�h)]�h+]�hi�thumb-detection�uh0h]h:K�h j�  hhh8hkubeh!}�(h#]�(j�  �id4�eh%]�h']�(�stylus-touch arbitration��stylus-touch-arbitration�eh)]�h+]�uh0hlh hnhhh8hkh:K�j�  }�jA  j�  sj�  }�j�  j�  subhm)��}�(hhh]�(hr)��}�(h�Thumb detection�h]�h�Thumb detection�����}�(hjK  h jI  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0hqh jF  hhh8hkh:K�ubh�)��}�(hX]  Many users rest their thumb on the touchpad while using the index finger to
move the finger around. For clicks, often the thumb is used rather than the
finger. The thumb should otherwise be ignored as a touch, i.e. it should not
count towards :ref:`clickfinger` and it should not cause a single-finger
movement to trigger :ref:`twofinger_scrolling`.�h]�(h��Many users rest their thumb on the touchpad while using the index finger to
move the finger around. For clicks, often the thumb is used rather than the
finger. The thumb should otherwise be ignored as a touch, i.e. it should not
count towards �����}�(h��Many users rest their thumb on the touchpad while using the index finger to
move the finger around. For clicks, often the thumb is used rather than the
finger. The thumb should otherwise be ignored as a touch, i.e. it should not
count towards �h jW  hhh8Nh:Nubh�)��}�(h�:ref:`clickfinger`�h]�h�)��}�(hjb  h]�h�clickfinger�����}�(hhh jd  ubah!}�(h#]�h%]�(hیstd��std-ref�eh']�h)]�h+]�uh0h�h j`  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�jn  �refexplicit��h�clickfinger�h�h�h�uh0h�h8hkh:K�h jW  ubh�= and it should not cause a single-finger
movement to trigger �����}�(h�= and it should not cause a single-finger
movement to trigger �h jW  hhh8Nh:Nubh�)��}�(h�:ref:`twofinger_scrolling`�h]�h�)��}�(hj�  h]�h�twofinger_scrolling�����}�(hhh j�  ubah!}�(h#]�h%]�(hیstd��std-ref�eh']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��reftype��ref��	refdomain�j�  �refexplicit��h�twofinger_scrolling�h�h�h�uh0h�h8hkh:K�h jW  ubh�.�����}�(hj�  h jW  hhh8Nh:Nubeh!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h jF  hhubh�)��}�(h��libinput uses two triggers for thumb detection: pressure and
location. A touch exceeding a pressure threshold is considered a thumb if it
is within the thumb detection zone.�h]�h��libinput uses two triggers for thumb detection: pressure and
location. A touch exceeding a pressure threshold is considered a thumb if it
is within the thumb detection zone.�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h jF  hhubh �note���)��}�(h��"Pressure" on touchpads is synonymous with "contact area." A large touch
surface area has a higher pressure and thus hints at a thumb or palm
touching the surface.�h]�h�)��}�(h��"Pressure" on touchpads is synonymous with "contact area." A large touch
surface area has a higher pressure and thus hints at a thumb or palm
touching the surface.�h]�h��“Pressure” on touchpads is synonymous with “contact area.” A large touch
surface area has a higher pressure and thus hints at a thumb or palm
touching the surface.�����}�(hj�  h j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0j�  h jF  hhh8hkh:Nubh�)��}�(h��Pressure readings are unreliable at the far bottom of the touchpad as a
thumb hanging mostly off the touchpad will have a small surface area.
libinput has a definitive thumb zone where any touch is considered a resting
thumb.�h]�h��Pressure readings are unreliable at the far bottom of the touchpad as a
thumb hanging mostly off the touchpad will have a small surface area.
libinput has a definitive thumb zone where any touch is considered a resting
thumb.�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h jF  hhubj	  )��}�(hhh]�j  )��}�(h�3.. figure:: thumb-detection.svg
    :align: center
�h]�h!}�(h#]�h%]�h']�h)]�h+]��uri��thumb-detection.svg�j  }�j  j�  suh0j  h j�  h8hkh:Nubah!}�(h#]�h%]�h']�h)]�h+]�j$  �center�uh0j  h jF  hhh8hkh:Nubh�)��}�(hX  The picture above shows the two detection areas. In the larger (light red)
area, a touch is labelled as thumb when it exceeds a device-specific
pressure threshold. In the lower (dark red) area, a touch is labelled as
thumb if it remains in that area for a time without moving outside.�h]�hX  The picture above shows the two detection areas. In the larger (light red)
area, a touch is labelled as thumb when it exceeds a device-specific
pressure threshold. In the lower (dark red) area, a touch is labelled as
thumb if it remains in that area for a time without moving outside.�����}�(hj�  h j�  hhh8Nh:Nubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h8hkh:K�h jF  hhubeh!}�(h#]�(j:  �id5�eh%]�h']�(�thumb detection��thumb-detection�eh)]�h+]�uh0hlh hnhhh8hkh:K�j�  }�j  j0  sj�  }�j:  j0  subeh!}�(h#]�(hj�id1�eh%]�h']�(�palm detection��palm_detection�eh)]�h+]�uh0hlh hhhh8hkh:Kj�  }�j  h_sj�  }�hjh_subeh!}�(h#]�h%]�h']�h)]�h+]��source�hkuh0h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hqN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j?  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�hk�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�N�gettext_compact��ub�reporter�N�indirect_targets�]��substitution_defs�}�(h5hhTh;u�substitution_names�}�(�git_version�h5�git_version_full�hTu�refnames�}��refids�}�(hj]�h_aj=  ]�j3  aj�  ]�j�  aj�  ]�j�  ajs  ]�ji  aj0  ]�j&  aj�  ]�j�  aj�  ]�j�  aj:  ]�j0  au�nameids�}�(j  hjj  j  j�  j=  j�  j�  j   j�  j�  j�  jz  j�  jy  jv  j7  js  j6  j3  j�  j0  j�  j�  jq  jn  j�  j�  j�  j�  jA  j�  j@  j=  j  j:  j  j  u�	nametypes�}�(j  �j  Nj�  �j�  Nj   �j�  Njz  �jy  Nj7  �j6  Nj�  �j�  Njq  �j�  �j�  �jA  �j@  Nj  �j  Nuh#}�(hjhnj  hnj=  j>  j�  j>  j�  j�  j�  j�  j�  j  jv  j  js  j  j3  j  j0  j<  j�  j<  jn  jh  j�  j�  j�  j�  j�  j�  j�  j�  j=  j�  j:  jF  j  jF  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]�h �system_message���)��}�(hhh]�h�)��}�(h�7Duplicate implicit target name: "disable-while-typing".�h]�h�;Duplicate implicit target name: “disable-while-typing”.�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]�j�  a�level�K�type��INFO��source�hk�line�K�uh0j�  h j�  hhh8hkh:K�uba�transform_messages�]�(j�  )��}�(hhh]�h�)��}�(hhh]�h�4Hyperlink target "palm-detection" is not referenced.�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j�  �source�hk�line�Kuh0j�  ubj�  )��}�(hhh]�h�)��}�(hhh]�h�/Hyperlink target "palm-tool" is not referenced.�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j�  �source�hk�line�K(uh0j�  ubj�  )��}�(hhh]�h�)��}�(hhh]�h�3Hyperlink target "palm-pressure" is not referenced.�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j�  �source�hk�line�K3uh0j�  ubj�  )��}�(hhh]�h�)��}�(hhh]�h�5Hyperlink target "palm-touch-size" is not referenced.�����}�(hhh j  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j�  �source�hk�line�KCuh0j�  ubj�  )��}�(hhh]�h�)��}�(hhh]�h�:Hyperlink target "palm-exclusion-zones" is not referenced.�����}�(hhh j-  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j*  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j�  �source�hk�line�KQuh0j�  ubj�  )��}�(hhh]�h�)��}�(hhh]�h�:Hyperlink target "trackpoint-disabling" is not referenced.�����}�(hhh jG  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h jD  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j�  �source�hk�line�Kwuh0j�  ubj�  )��}�(hhh]�h�)��}�(hhh]�h�:Hyperlink target "disable-while-typing" is not referenced.�����}�(hhh ja  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j^  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j�  �source�hk�line�K�uh0j�  ubj�  )��}�(hhh]�h�)��}�(hhh]�h�>Hyperlink target "stylus-touch-arbitration" is not referenced.�����}�(hhh j{  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h jx  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j�  �source�hk�line�K�uh0j�  ubj�  )��}�(hhh]�h�)��}�(hhh]�h�5Hyperlink target "thumb-detection" is not referenced.�����}�(hhh j�  ubah!}�(h#]�h%]�h']�h)]�h+]�uh0h�h j�  ubah!}�(h#]�h%]�h']�h)]�h+]��level�K�type�j�  �source�hk�line�K�uh0j�  ube�transformer�N�
decoration�Nhhub.